var express = require("express");
var path = require('path');
const request = require('request');
var bodyParser = require("body-parser");
var cookieParser = require('cookie-parser');
var fs = require("fs");
var app = express();
var http = require('http');
var morgan = require('morgan');
var signUp = require('./model/signUp.js');
var config = require('./config.js');
var port     = process.env.PORT || config.port;
process.env.NODE_TLS_REJECT_UNAUTHORIZED=0;
var tournaments = require('./model/tournaments.js');
var dice = require('./model/dice.js');
var other = require('./model/other.js');
var common_model = require('./model/common_model.js');
//var server =http.createServer(app);
const server =require('http').createServer(app);
//const server =require('http').createServer(app);
const rateLimit = require("express-rate-limit");

server.listen(port,function(){
	 console.log(config.portMsg)
});


const limiter = rateLimit({
  windowMs: 10600 * 60 * 1000, // 15 minutes
  max: 105, // limit each IP to 100 requests per windowMs
  message:
    "Server Error"
});
const limiter2 = rateLimit({
  windowMs: 10600 * 60 * 1000, // 15 minutes
  max: 100, // limit each IP to 100 requests per windowMs
  message:
    "Server Error"
});
// app.use(morgan('dev'));
var logger = morgan('combined');
var tiny = morgan('tiny');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.set('view engine', 'ejs');


app.get('/',function(req,res){
	res.end('WELCOME TO NODE');
});
// http://15.206.99.151:3011/signUp
app.post("/signUp",limiter,function(req,res){
	var reqData = req.body;
	reqData['ip']=req.ip;
	// console.log(reqData);
	signUp.registration(reqData,function(response){
		// console.log(response);
		 res.send(response);
	});
});

app.post("/forgotPassword",limiter,function(req,res){
	var reqData = req.body;
	signUp.forgotPassword(reqData,function(response){
		res.send(response);
	});
});

app.post("/OtpVerify",limiter,function(req,res){
	var reqData = req.body;
	signUp.OtpVerifyFunction(reqData,function(response){
	// console.log(reqData);
		console.log(response," OtpVerifyFunction");
		res.send(response);
	});
});

app.post("/resendOtp",limiter,function(req,res){
	var reqData = req.body;
	signUp.ResendOtpFunction(reqData,function(response){
		res.send(response);
	});
});

app.post("/changePassword",limiter,function(req,res){
	var reqData =req.body;
	signUp.changePassword(reqData,function(response){
		res.send(response);
	});
});

app.post("/updateProfile",function(req,res){
	var reqData = req.body;
	signUp.profileUpdateFunction(reqData,function(response){
		res.send(response);
	});
});


app.post("/login",function(req,res){
	// console.log(req.ip)
	var reqData =req.body;
	signUp.loginAction(reqData,function(response){
		res.send(response);
	});
});

/*---------------------------- Get tournaments ------------------------------*/
app.post("/getTournaments",function(req,res){
	var reqData = req.body;
	tournaments.getTournaments(reqData,function(response){
		res.send(response);
	});
});
/*---------------------------- Get Bonus ------------------------------*/
app.post("/getBonus",function(req,res){
	 var reqData = req.body;
	 tournaments.getBonus(reqData,function(response){
		res.send(response);
	 });
});
/*---------------------------- Get Rooms ------------------------------*/
app.post("/getRoomDetails",function(req,res){
	 var reqData = req.body;
	 tournaments.getRoomDetails(reqData,function(response){
		res.send(response);
	 });
});
// get api for play time
app.post("/getIsPlayTime",function(req,res){
	var reqData =req.body;
	tournaments.getIsPlayTime(reqData,function(response){
		res.send(response);
	});
})
// get api for play time
app.post("/getdayWiseTimings",function(req,res){
	var reqData =req.body;
	tournaments.getdayWiseTimings(reqData,function(response){
		res.send(response);
	});
});
/*---------------------------- Update Device Id ------------------------------*/
app.post("/updateDevice",function(req,res){
	var reqData = req.body;
	// console.log(reqData);
	signUp.updateDeviceId(reqData,function(response){
		res.send(response);
	});
});

/*---------------------------- Update player progress ------------------------------*/
app.post("/updateplayerProgress",limiter2,function(req,res){
  	var reqData = req.body;
	tournaments.updateplayerProgress(reqData,function(response){
		res.send(response);
	});
});
/*---------------------------- Get player progress------------------------------*/
app.post("/getplayerProgress",function(req,res){
	 var reqData = req.body;
	 tournaments.getplayerProgress(reqData,function(response){
		res.send(response);
	 });
});
/*---------------------------- Get transaction list------------------------------*/
app.post("/getTransactionList",function(req,res){
	 var reqData = req.body;
	 tournaments.getTransactionListByCustId(reqData,function(response){
		res.send(response);
	 });
});
/*---------------------------- Get transaction list------------------------------*/
app.post("/purchaseDice",function(req,res){
		var reqData =req.body;
		dice.purchaseDice(reqData,function(response){
			res.send(response);
		});
});
//------------------ save support------------------------------------
app.post("/addSupport",function(req,res){
	var reqData =req.body;
	tournaments.addSupport(reqData,function(response){
		res.send(response);
	});
});
// get Support
app.post("/getSupport",function(req,res){
	var reqData =req.body;
	tournaments.getSupport(reqData,function(response){
		res.send(response);
	});
});
/*------------------- For get game version -------------------------*/
app.post("/getGameVersion",function(req,res){
	var reqData =req.body;
	dice.getGameVersion(reqData,function(response){
		res.send(response);
	});
});

/*------------------- get top 30 players record -------------------------*/
app.post("/topPlayersRecord",function(req,res){

	var reqData =req.body;
	other.topPlayersRecord(reqData,function(response){
		res.send(response);
	});
});

/*------------------- get items record -------------------------*/
app.post("/getItems",function(req,res){
	var reqData =req.body;
	dice.getItems(reqData,function(response){
		res.send(response);
	});
});


/*------------------- get admin Percent record -------------------------*/
app.post("/getadminPercent",function(req,res){
	var reqData =req.body;
	other.getadminPercent(reqData,function(response){
		res.send(response);
	});
});



/*------------------- get game history -------------------------*/
app.post("/getGameHistory",function(req,res){
    var reqData =req.body;
    other.getGameHistory(reqData,function(response){
        res.send(response);
    });
});

/*------------------- my Referr Record -------------------------*/
app.post("/myReferrRecord",function(req,res){
    var reqData =req.body;
    other.myReferrRecord(reqData,function(response){
        res.send(response);
    });
});

/*------------------- my Withdrawal History -------------------------*/
app.post("/myTransactionHistory",function(req,res){
    var reqData =req.body;
    other.myTransactionHistory(reqData,function(response){
        res.send(response);
    });
});

/*------------------- my Withdrawal History -------------------------*/
app.post("/myWithdrawalHistory",function(req,res){
    var reqData =req.body;
    other.myWithdrawalHistory(reqData,function(response){
        res.send(response);
    });
});

/*-------------------  Save Bonus By userID -------------------------*/
app.post("/setBonusByUserId",function(req,res){
    var reqData =req.body;
    other.setBonusByUserId(reqData,function(response){
        res.send(response);
    });
});

/*-------------------  get Bonus By userID -------------------------*/
app.post("/getBonusByUserId",function(req,res){
    var reqData =req.body;
    other.getBonusByUserId(reqData,function(response){
        res.send(response);
    });
});

/*-------------------  get Current Date&Time -------------------------*/
app.post("/getCurrentDateTime",function(req,res){
    var reqData =req.body;
    other.getCurrentDateTime(reqData,function(response){
        res.send(response);
    });
});

/*-------------------  get Current Date&Time -------------------------*/
app.post("/updateReferToBalance",function(req,res){
    var reqData =req.body;
    other.updateReferToBalance(reqData,function(response){
        res.send(response);
    });
});

/*-------------------  get Bonus By userID -------------------------*/
app.post("/getMaintainance",function(req,res){
    var reqData =req.body;
    other.getMaintainance(reqData,function(response){
        res.send(response);
    });
});


/*------------------- get addSpinWheel -------------------------*/
app.post("/addSpinWheel",function(req,res){
    var reqData =req.body;
    other.addSpinWheel(reqData,function(response){
        res.send(response);
    });
});

/*------------------- get getSpinWheel -------------------------*/
app.post("/getSpinWheel",function(req,res){
    var reqData =req.body;
    other.getSpinWheel(reqData,function(response){
        res.send(response);
    });
});

/*------------------- get getSpinPrices -------------------------*/
app.post("/getSpinPrices",function(req,res){
    var reqData =req.body;
    other.getSpinPrices(reqData,function(response){
        res.send(response);
    });
});

/*------------------- get getCoupons -------------------------*/
app.post("/getCoupons",function(req,res){
    var reqData =req.body;
    other.getCoupons(reqData,function(response){
        res.send(response);
    });
});

app.post("/getSettings",function(req,res){
    var reqData =req.body;
    other.getSettings(reqData,function(response){
        res.send(response);
    });
});

app.post("/updateVersion",function(req,res){
    var reqData =req.body;
    other.updateVersion(reqData,function(response){
        res.send(response);
    });
});

// var reqData={
// 	version:'9',
// 	userId:'68'
// }
// other.updateVersion(reqData,function(response){
//     console.log(response);
// });

