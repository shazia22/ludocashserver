<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CompletedRequest extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->library('image_lib');
		$this->load->model('CompletedRequest_model');
	} 

	public function index()
	{
		$getUsers = $this->Crud_model->GetData('user_details','','playerType="Real" and status="Active"');
		$data=array(
			'heading'=>"Completed Request",
			'bread'=>"Manage Completed Request",
			'getUsers'=>$getUsers,
			);
		$this->load->view('withdrawal/comp_list',$data);
	}


	public function ajax_manage_page()
	{
		$condition = "ua.type='Withdraw' and ua.status='Approved' and ua.paymentType in ('bank','Manually')";
		if(!empty($this->input->post('SearchData')) && !empty($this->input->post('SearchData1'))) {
			$condition .= " and date(ua.created) between '".date("Y-m-d",strtotime($this->input->post('SearchData')))."' and '".date("Y-m-d",strtotime($this->input->post('SearchData1')))."' ";
		}else if(!empty($this->input->post('SearchData'))) {
			$condition .= " and date(ua.created) >= '".date("Y-m-d",strtotime($this->input->post('SearchData')))."'";
		}else if(!empty($this->input->post('SearchData1'))) {
			$condition .= " and date(ua.created) <= '".date("Y-m-d",strtotime($this->input->post('SearchData1')))."'";
		}
		if(!empty($this->input->post('SearchData2'))) {
			$condition .= " and ua.paymentType = '".$this->input->post('SearchData2')."'";
		}

		$getPenWith = $this->CompletedRequest_model->get_datatables('user_account ua',$condition);
		// print_r($getPenWith);exit();

		if(empty($_POST['start']))
		{
			$no =0;   
		}else{
			 $no =$_POST['start'];
		}
		$data = array();
				  
		foreach ($getPenWith as $PendingWithd) 
		{
		
			$btn = '';

			$btn = ''.anchor(site_url(WITHDRAWALCOMPREQVIEW.'/'.base64_encode($PendingWithd->id)),'<span title="View" class="btn btn-primary btn-circle btn-xs"  data-placement="right" title="View">View</span>');



			if(!empty($PendingWithd->user_name)){ $user_name = $PendingWithd->user_name; }else{ $user_name = 'NA'; }
			if(!empty($PendingWithd->mobileNo)){ $mobileNo = $PendingWithd->mobileNo; }else{ $mobileNo = 'NA'; }

			if(!empty($PendingWithd->orderId)){ $orderId = $PendingWithd->orderId; }else{ $orderId = 'NA'; }

			if(!empty($PendingWithd->amount)){ $amount = $PendingWithd->amount; }else{ $amount = 'NA'; }

			if(!empty($PendingWithd->paymentType)){ $paymentType = $PendingWithd->paymentType; }else{ $paymentType = 'NA'; }

			if(!empty($PendingWithd->created) && $PendingWithd->created !="0000-00-00 00:00:00"){ $created = date('d M Y H:i A', strtotime($PendingWithd->created)); }else{ $created = 'NA'; }
			
			if(!empty($PendingWithd->modified) && $PendingWithd->modified !="0000-00-00 00:00:00"){ $modified = date('d M Y H:i A', strtotime($PendingWithd->modified)); }else{ $modified = 'NA'; }
		 
			$no++;
			$nestedData = array();
			$nestedData[] = $no;
			$nestedData[] = ucfirst($user_name);
			$nestedData[] = $mobileNo;
			$nestedData[] = $orderId;
			$nestedData[] = $amount;
			$nestedData[] = $paymentType;
			$nestedData[] = $created;
			$nestedData[] = $modified;
			$nestedData[] = $btn;
			
			$data[] = $nestedData;
		}

		$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->CompletedRequest_model->count_all('user_account ua',$condition),
					"recordsFiltered" => $this->CompletedRequest_model->count_filtered('user_account ua',$condition),
					"data" => $data,
					"csrfHash" => $this->security->get_csrf_hash(),
					"csrfName" => $this->security->get_csrf_token_name(),
				);
		echo json_encode($output);
	}


	public function viewRequest($id){
		$id = base64_decode($id);
		$this->db->select('d.*,u.user_name,u.user_id,u.user_name,u.email_id,u.balance,u.mobile,u.bank_account_holder_name,u.bank_account_no,u.bank_ifsc');
		$this->db->from("user_account d"); 
		$this->db->join("user_details u","u.id=d.user_detail_id","left");
		// $this->db->join("bank_details bd","bd.user_detail_id=d.user_detail_id","left");
		$this->db->order_by('d.id DESC'); 
		$this->db->where("d.id='".$id."' and d.type='Withdraw' and d.status='Approved'");
		$queryData = $this->db->get()->row();

		$data = array(
			"getData" => $queryData,
			"heading" => "Completed Request",
			"breadhead" => "Completed Request",
			"bread" => "View Completed request",
		);
		// print_r($data);exit;

		$this->load->view('withdrawal/view_comp_list',$data);
	}



	public function exportAction(){
		if(!empty($this->input->post('payment_type')) && $this->input->post('payment_type')!=''){
			$condition = "ua.type='Withdraw' and ua.status='Approved' and ua.paymentType in ('bank','Manually')";
			// $condition = "ua.type='Withdraw' and ua.status='Approved' and ua.paymentType in ('bank','paytm')";
			if(!empty($this->input->post('fromDate')) && !empty($this->input->post('toDate'))) {
				$condition .= " and date(ua.created) between '".date("Y-m-d",strtotime($this->input->post('fromDate')))."' and '".date("Y-m-d",strtotime($this->input->post('toDate')))."' ";
			}else if(!empty($this->input->post('fromDate'))) {
				$condition .= " and ua.created >= '".date("Y-m-d",strtotime($this->input->post('fromDate')))."'";
			}else if(!empty($this->input->post('toDate'))) {
				$condition .= " and ua.created <= '".date("Y-m-d",strtotime($this->input->post('toDate')))."'";
			}
			if(!empty($this->input->post('payment_type'))) {
				$condition .= " and ua.paymentType = '".$this->input->post('payment_type')."'";
			}
			$getUserData = $this->CompletedRequest_model->getExportData('user_account ua',$condition);
			// print_r($this->db->last_query());exit;
			if(!empty($getUserData)) {
				$filename = "CompleteWithdraw".date('d-m-Y H:i').".csv";
				$fp = fopen('php://output', 'w');
				$header = array("User Name",'Mobile','Order Id','Amount (Rs)','Payment Mod','Request On','Completed On');
				header('Content-type: application/csv');
				header('Content-Disposition: attachment; filename='.$filename);
				fputcsv($fp, $header);
				$sr=1;
				foreach ($getUserData as $report) {
					if(!empty($report->user_name)){ $user_name = $report->user_name; }else{ $user_name = 'NA'; }
					if(!empty($report->mobileNo)){ $mobileNo = $report->mobileNo; }else{ $mobileNo = 'NA'; }
					if(!empty($report->orderId)){ $orderId = $report->orderId; }else{ $orderId = 'NA'; }
					if(!empty($report->amount)){ $amount = $report->amount; }else{ $amount = 'NA'; }
					if(!empty($report->paymentType)){ $paymentType = $report->paymentType; }else{ $paymentType = 'NA'; }
					if(!empty($report->created)){ $created = $report->created; }else{ $created = 'NA'; }
					if(!empty($report->modified)){ $modified = $report->modified; }else{ $modified = 'NA'; }

					fputcsv($fp, array($user_name,$mobileNo,$orderId,$amount,$paymentType,$created,$modified));
					$sr++;
				}
			}else{
				$this->session->set_flashdata('message', 'Record not avaliable.');
				redirect(WITHDRAWALCOMPREQ);
			}
		}else{
			$this->session->set_flashdata('message', 'Please select Payment type.');
			redirect(WITHDRAWALCOMPREQ);
		}
		
	}

}