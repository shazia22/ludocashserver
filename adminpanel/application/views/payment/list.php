<?php $this->load->view('common/header'); ?>

<?php $this->load->view('common/left_panel.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?= $heading; ?></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box bShow">
            <div class="box-header col-md-12">
              <!-- <div align="center" class="box-header col-md-12">
                <select name="users" class="filter_search_data"> 
                  <option value="0">Search Users</option>
                  <?php if(!empty($getUsers)) { foreach ($getUsers as $users) { ?>
                  <option value="<?= $users->user_name; ?>"><?= $users->user_name; ?></option>
                <?php } } ?>
                </select>
              </div> -->
              <!-- <div class="col-md-4">
               </div> -->
               <form method="post" action="<?= site_url(PAYMENTTRANSACTIONEXPORT);?>">
                <div class="col-md-3">
                  <label>Payment Status</label>
                    <select class="form-control filter_search_data3" name="SearchData3" id="SearchData3">
                      <option value="">Select Status</option>
                      <option value="Approved">Approved</option>
                      <option value="Pending">Pending</option>
                      <option value="Rejected">Rejected</option>
                      <option value="Success">Success</option>
                      <option value="Failed">Failed</option>
                      <option value="Process">Process</option>
                    </select>
                </div>
                <div class="col-md-3">
                  <label>Payment Type</label>
                    <select class="form-control filter_search_data2" name="SearchData2" id="SearchData2">
                      <option value="">Select Type</option>
                      <option value="Withdraw">Withdraw</option>
                      <option value="Deposit">Deposit</option>
                    </select>
                </div>
                <div class="col-md-4">
                <div class="col-md-12">
                  <label>Date</label>
                </div>
                   <div class="col-md-6">
                     <input type="text" class="form-control datepicker filter_search_data1" name="toDate" id="toDate" placeholder="Select To Date" autocomplete="off">
                   </div>
                   <div class="col-md-6">
                     <input type="text" class="form-control datepicker filter_search_data" name="fromDate" id="fromDate" placeholder="Select From Date" autocomplete="off">
                   </div>
                </div>
               <!--  <div class="col-md-6">
                  <div class="col-md-3 pull-right paddRight">
                    <input type="text" class="form-control datepicker filter_search_data1" name="toDate" id="toDate" placeholder="Select To Date" autocomplete="off">
                  </div>
                  <div class="col-md-3 pull-right paddRight">
                    <input type="text" class="form-control datepicker filter_search_data" name="fromDate" id="fromDate" placeholder="Select From Date" autocomplete="off">
                  </div>
                  <div class="col-md-3 pull-left paddRight">
                    <label>Type</label>
                    <select class="form-control filter_search_data2" name="SearchData2" id="SearchData2">
                      <option value="">Select Type</option>
                      <option value="Withdraw">Withdraw</option>
                      <option value="Deposit">Deposit</option>
                    </select>
                  </div>
                  <div class="col-md-3 pull-left paddRight">
                    <label>Payment Status</label>
                    <select class="form-control filter_search_data3" name="SearchData3" id="SearchData3">
                      <option value="">Select Status</option>
                      <option value="Approved">Approved</option>
                      <option value="Pending">Pending</option>
                      <option value="Rejected">Rejected</option>
                      <option value="Success">Success</option>
                      <option value="Failed">Failed</option>
                      <option value="Process">Process</option>
                    </select>
                  </div>
                </div> -->
                <div class="col-md-12">
                <div>&nbsp;</div>
                <div class="col-md-4 pull-right">
                   <button type="reset" class="btn btn-warning resetBtn" name="reset" id="reset">Reset</button>&nbsp;
                   <button type="submit"  class="btn btn-success">Export</button>
                   <button type="button"  class="btn btn-info" onclick="checkPayment()">Check Payment Status</button>
                 </div>
                </div>
               
              </form>
              <input type="hidden" id="chk" class="filter_search_data5" value="">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-striped" id="example_datatable" style="width: 100%;">
                <thead>
                <tr>
                  
                  <th>#</th>
                  <th>OrderId</th>
                  <th>Username</th>
                  <th>Mobile</th>
                  <th>Txn Amt(<?= INR ?>)</th>
                  <th>Win Wallet(<?= INR ?>)</th>
                  <th>Main Wallet(<?= INR ?>)</th>
                  <th>Date</th>
                  <th>Type</th>
                  <th>Payment Mode</th>
                  <th>Status</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- Modal -->
<div class="modal fade" id="userImportModal" data-modal-color="teal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
      <form method="post" action="<?php echo site_url('Users/importUsers'); ?>" enctype="multipart/form-data">
        <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal" id="closeCpModal">&times;</button> -->
          <h4 class="modal-title">Import Users
          <a href="<?php echo base_url('assets/import/Import_users_format.xlsx'); ?>"><span class="btn btn-success btn-sm btn-small pull-right" style="margin-right: 5px;">Download Format</span></a>  
          </h4>
        </div>     
        <div class="modal-body">
          <div class="row">
            <label class="col-md-4 control-label" for="inputLabel">Please select file : </label>
            <div class="col-md-4">   
              <input id="import_user" type="file"  placeholder="Upload Users" type="file" name="import_user">
            </div>
            <div class="col-md-4">
              <span id="errorUser"></span>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" onclick="return User_Validation();">Upload</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </form>  
        </div>
    </div>
</div>

<!-- /.Modal -->
<script type="text/javascript">
   var url = '<?= site_url("Payment/ajaxList"); ?>';
   var actioncolumn=10;
   var pageLength='';
</script>

<!-- Load common footer -->
<?php $this->load->view('common/footer'); ?>

<script type="text/javascript">  
 
function User_Validation()
{ 
  var import_user = $("#import_user").val(); 
  if(import_user == "")
  {  
    $("#errorUser").html("<span style='color:red;'>Please upload excel</span>").fadeIn();
    setTimeout(function(){$("#errorUser").fadeOut()},3000);
    return false; 
  }

}


  function checkPaymentData(id) {
 
     var new_array=[];
   var chk = $("#chk").val();
   var check_box = $(".chk_"+id).is(':checked');
   // console.log(check_box);
   if(check_box==false){
     var res = chk.split(",");
     for (var i = 0; i < res.length; i++) {
     // console.log(res[i]);
       if(res[i]!=id){
        new_array.push(res[i]);
      }
     }
    
   }else{
    if(chk !=''){
      new_array.push(chk);
    }
      new_array.push(id);
    
   }
  
   $("#chk").val(new_array);
  }
  function checkPayment(id) {
   var chk = $("#chk").val();
   if(chk==''){
      alert('Please select at list one check box');
      return false;
    }
    $(".preloader").show();
    $.ajax({
      type:'post',
      data:{ids:chk},
      url:'<?= site_url('Payment/check_status')?>',
      success:function(returndata){
        $("#chk").val('');
         var res = chk.split(",");
       for (var i = 0; i < res.length; i++) {
        console.log("chk_"+res[i]);
          $(".chk_"+res[i]).removeAttr('checked');
          $(".chk_"+res[i]).attr('disabled');
       }
        // $("input:checkbox").prop('checked', $(this).prop("checked"));
        // $(".chk").val('');
        setTimeout(function(){

        $(".resetBtn").click();
        $(".preloader").hide();
      },500);
        console.log(returndata);
      }
    })
  }
</script>
