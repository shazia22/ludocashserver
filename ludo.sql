-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 18, 2021 at 08:48 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nilesh_ludo`
--

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS `changePassword`$$
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `changePassword` (IN `input_userId` INT, IN `input_oldPassword` VARCHAR(255), IN `input_newPassword` VARCHAR(255), IN `input_confirmPassword` VARCHAR(255))  BEGIN
select mainEnvironmentId from main_environment where envKey='LUDOFANTASY' and value='bqwdyq8773nas98r398mad234fusdf89r2' LIMIT 1 into @envId;
 
  if(@envId is not null)  
  then
    SELECT id,password from user_details where id=input_userId limit 1 INTO @userId,@password;
    if(@userId!='')
    then
      --  set @success=1;
       -- set @message= "User Found";
       if(input_newPassword !=input_confirmPassword)
       then
          set @success=4;
          set @message= "New password and confirm password should be same";
       else
         -- UPDATE user_details SET password=MD5(input_newPassword) where id=input_userId;
          set @success=1;
          set @message= "User Found";
          set @dbPassword=@password; 
       end if;
     /*  if(MD5(input_oldPassword) != @password)
       then
          set @success=2;
          set @message= "password not matched.";
       elseif(@password = MD5(input_newPassword))
       then
          set @success=3;
          set @message= "Old & new password can't be same";
       elseif(@password = MD5(input_confirmPassword))
       then
          set @success=4;
          set @message= "New password and confirm password should be same";
       else
          UPDATE user_details SET password=MD5(input_newPassword) where id=input_userId;
          set @success=1;
          set @message= "Password changed successfully";
       end if; */
    else
       set @success=5;
       set @message= "Invalid User.";
    end if;
  else
    set @success=6;
    set @message= "Invalid Data Submitted";
  end if;
    select @success as success,@message as message,@dbPassword as dbPassword;

END$$

DROP PROCEDURE IF EXISTS `createPrivateRoom`$$
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `createPrivateRoom` (IN `input_noOfPlayer` TINYINT, IN `input_gameMode` VARCHAR(30), IN `input_betValue` INT, IN `input_roomId` INT, IN `input_isFree` VARCHAR(10))  BEGIN
    if(input_noOfPlayer!='' && input_gameMode!='' && input_gameMode!='' && input_roomId!='')
    then
           select roomId from ludo_mst_rooms where isPrivate='Yes' and roomId=input_roomId limit 1 into @roomId;           
          if(@roomId!='')            
          then
            set @success = 1;
            set @message ="success";
            insert into ludo_join_rooms 
               set roomId=@roomId,noOfPlayers=input_noOfPlayer,activePlayer=0,isFree=input_isFree,gameMode=input_gameMode,betValue=input_betValue,isPrivate='Yes',
               isTournament='No',modified=now(),created=now();
            SET @joinRoomId = LAST_INSERT_ID();
          else
            set @success = 0;
            set @message ="failed";
          end if;
          select @success as success,@message as message,@roomId as roomId,@joinRoomId as joinRoomId,input_noOfPlayer as noOfPlayer,
           input_gameMode as gameMode,input_betValue as betValue,input_roomId as roomId,input_isFree as isFree;
    else
         select 1 as  success,"All field are required." as message;
    end if;
     
END$$

DROP PROCEDURE IF EXISTS `forgotPassword`$$
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `forgotPassword` (IN `input_mobile` BIGINT)  BEGIN
select mainEnvironmentId from main_environment where envKey='LUDOFANTASY' and value='bqwdyq8773nas98r398mad234fusdf89r2' LIMIT 1 into @envId;
 
  if(@envId is not null)  
  then
    select id,user_name,mobile from user_details where mobile=input_mobile and blockuser='No' limit 1 
    into @userId,@user_name,@mobile;

    if(@userId!='')
    then
       select lpad(conv(floor(rand()*pow(36,6)), 10, 36), 6, 0) into @newPassword;
       /*UPDATE user_details SET password=MD5(@newPassword) where id=@userId;*/

       set @newPassword=@newPassword;
       set @success=1;
       set @status=true;
       set @message="Password is sent to your mobile number";
    else
       set @newPassword='';
       set @success=2;
       set @status=false;
       set @message="Invalid User";
    end if;
  else
    set @success=6;
    set @message= "Invalid Data Submitted";
  end if;
    
  select @message as message,@status as status,@success as success,@newPassword as newPassword,@user_name as user_name,@userId as userId;

END$$

DROP PROCEDURE IF EXISTS `joinBotsRoomTable`$$
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `joinBotsRoomTable` (IN `input_userId` DOUBLE, IN `input_roomId` DOUBLE, IN `input_players` INT, IN `input_betValue` DOUBLE, IN `input_playerType` VARCHAR(10), IN `input_gameMode` VARCHAR(20), IN `input_isFree` VARCHAR(10), IN `input_tableId` DOUBLE)  BEGIN
   if(input_userId !='' and input_roomId!='')
   then
     select balance,user_name,id,profile_img from user_details where id=input_userId limit 1 into @coins,@userName,@userId,@profile_img;
     select baseUrl from mst_settings where id!=0 limit 1 into @baseUrl;
     select roomId,commision,currentRoundBot,totalRoundBot,roomTitle,isBotConnect from ludo_mst_rooms where roomId=input_roomId limit 1 
     into @roomId,@commision,@currentRoundBot,@totalRoundBot,@roomTitle,@isBotConnect;
     set @message := "Failed";
     if(@userId is not null and @roomId is not null)
     then
         if(@profile_img='')
         then
            set @profile ='';
         else 
            set @profile := concat(@baseUrl,'uploads/userProfileImages/',@profile_img);
         end if;
         
         set @success := 0;    
         select joinRoomId,gameStatus,activePlayer from ludo_join_rooms where roomId=input_roomId and noOfPlayers = input_players 
          and activePlayer < input_players and gameStatus='Pending' and gameMode=input_gameMode and isPrivate='No' and isTournament='No' and joinRoomId=input_tableId 
          into @joinRoomId,@gameStatus,@activePlayer ; 
         if(@joinRoomId is not null)
         then
            set @success := 1;
            set @message := "Success";
            set @player := @activePlayer+1;  
            if @player = input_players
            then
              set @gameStatus= 'Active';            
            end if;  
           /* set @totalRoundBot := @totalRoundBot+1; 
            set @currentRoundBot := @currentRoundBot+1; 
                if @currentRoundBot =10                       
                then            
                  set @currentRoundBot = 0; 
                end if; 
            update ludo_mst_rooms set totalRoundBot=@totalRoundBot,currentRoundBot=@currentRoundBot where roomId=input_roomId;*/
            update ludo_join_rooms set activePlayer=@player,gameStatus=@gameStatus,modified=now() where joinRoomId=@joinRoomId;
            insert into ludo_join_room_users set userId=input_userId,roomId=input_roomId,tokenColor='Blue',playerType=input_playerType,
                 userName=@userName,joinRoomId=@joinRoomId,isTournament='No',created=now();  
         else
            set @success := 0;
         end if;
     else
       set @success := 0;
     end if;
     select @success as success,@message as message,@coins as coins,@userId as userId,@roomId as roomId,@joinRoomId as joinRoomId,
            @gameStatus as gameStatus,'Blue' as tokenColor,input_players as players,@userName as userName,input_playerType as playerType, 
           input_gameMode as gameMode,"No" as isPrivate,@commision as adminCommision,input_isFree as isFree,input_betValue as betValue,
           @currentRoundBot as currentRoundBot,@totalRoundBot as totalRoundBot,@profile as profile,@profile_img  as profile_img,
           @roomTitle as roomTitle,@isBotConnect as isBotConnect,'No' as isTournament;
   else
     select 0 as  success,"All field are required." as message;
   end if;  
END$$

DROP PROCEDURE IF EXISTS `joinPrivateRoom`$$
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `joinPrivateRoom` (IN `input_userId` INT, IN `input_roomId` INT, IN `input_player` DOUBLE, IN `input_value` DOUBLE, IN `input_color` VARCHAR(50), IN `input_type` VARCHAR(50), IN `input_gameMode` VARCHAR(50), IN `input_tableId` DOUBLE, IN `input_isFree` VARCHAR(10))  BEGIN
   if(input_userId !='' and input_roomId!='' )
   then
     select balance,user_name,id,profile_img from user_details where id=input_userId limit 1 into @coins,@userName,@userId,@profile_img;
     select roomId,commision,currentRoundBot,totalRoundBot,roomTitle,isBotConnect,startRoundTime,tokenMoveTime,rollDiceTime from ludo_mst_rooms where roomId=input_roomId and isPrivate='Yes' limit 1 
     into @roomId,@commision,@currentRoundBot,@totalRoundBot,@roomTitle,@isBotConnect,@startRoundTime,@tokenMoveTime,@rollDiceTime;

     select baseUrl from mst_settings where id!=0 limit 1 into @baseUrl;
     if(@userId is not null and @roomId is not null and  @coins>9 and @coins>=input_value and input_value>9)
     then  
         if(@profile_img='')
         then
            set @profile ='';
         else 
            set @profile := concat(@baseUrl,'uploads/userProfileImages/',@profile_img);
         end if;
         select joinRoomId,gameStatus,activePlayer  from ludo_join_rooms 
          where roomId=input_roomId and noOfPlayers = input_player and activePlayer < input_player 
          and gameStatus='Pending' and gameMode=input_gameMode and betValue=input_value 
          and joinRoomId=input_tableId and isPrivate='Yes' and isTournament='No' and isFree=input_isFree
          into @joinRoomId,@gameStatus,@activePlayer; 
         if(@joinRoomId is not null)
         then
     		set @success := 1;
            set @message := "Success"; 
            set @player := @activePlayer+1;  
            if @player = input_player
            then
              set @gameStatus= 'Active';            
            end if;  
            update ludo_join_rooms set activePlayer=@player,gameStatus=@gameStatus,modified=now() where joinRoomId=@joinRoomId; 
            insert into ludo_join_room_users set userId=input_userId,roomId=input_roomId,tokenColor=input_color,
            userName=@userName,joinRoomId=@joinRoomId,isTournament='No',created=now();
            -- insert into ludo_join_room_users set userId=input_userId,roomId=input_roomId,tokenColor=input_color,userName=@userName,joinRoomId=@joinRoomId,created=now(); 
         else
           set @player:= 1;
           set @gameStatus = 'Pending';
           set @success := 0;  
           set @message := "No room available";  
         end if;
              
     else
       set @success := 0;
       set @message := "Not match fileds";
     end if;
     select @success as success,@message as message,@coins as coins,@userId as userId,@roomId as roomId,@joinRoomId as joinRoomId,
            @gameStatus as gameStatus,input_color as tokenColor,input_player as players,@userName as userName,input_type as playerType, 
           input_gameMode as gameMode,"Yes" as isPrivate,@commision as adminCommision,input_isFree as isFree,
           @currentRoundBot as currentRoundBot,@totalRoundBot as totalRoundBot,@profile  as profile,@profile_img as profile_img,
           @roomTitle as roomTitle,@isBotConnect as isBotConnect,@startRoundTime as startRoundTime,
           @tokenMoveTime as tokenMoveTime,@rollDiceTime as rollDiceTime,'No' as isTournament;

   else
     select 0 as  success,"All field are required." as message;
   end if;  
END$$

DROP PROCEDURE IF EXISTS `joinRoom`$$
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `joinRoom` (IN `input_userId` INT, IN `input_roomId` INT, IN `input_player` DOUBLE, IN `input_value` DOUBLE, IN `input_color` VARCHAR(15), IN `input_type` VARCHAR(15), IN `input_gameMode` VARCHAR(15), IN `input_isFree` VARCHAR(15))  BEGIN
   if(input_userId !='' and input_roomId!='')
   then
     select balance,user_name,id,profile_img,block from user_details where id=input_userId limit 1 into 
        @coins,@userName,@userId,@profile_img,@block;
     select roomId,commision,currentRoundBot,totalRoundBot,roomTitle,isBotConnect,startRoundTime,tokenMoveTime,rollDiceTime 
     from ludo_mst_rooms where roomId=input_roomId limit 1 
     into @roomId,@commision,@currentRoundBot,@totalRoundBot,@roomTitle,@isBotConnect,@startRoundTime,@tokenMoveTime,@rollDiceTime;
     select baseUrl,joinRoomName,cdh from mst_settings where id!=0 limit 1 into @baseUrl,@joinRoomName,@cdh;
      
     if(@block=0 and @userId is not null and @roomId is not null and @joinRoomName!='' and  @coins > 9 and @coins >= input_value and input_value>9 and @userId!='26265' )
     then
         if(@profile_img='')
         then
            set @profile ='';
         else 
            set @profile := concat(@baseUrl,'uploads/userProfileImages/',@profile_img);
         end if;
         set @success := 1;    
         select joinRoomId,gameStatus,activePlayer from ludo_join_rooms where roomId=input_roomId and noOfPlayers = input_player 
          and activePlayer < input_player and gameStatus='Pending' and isPrivate='No' limit 1
          into @joinRoomId,@gameStatus,@activePlayer ; 
         if(@joinRoomId is not null)
         then
            set @player := @activePlayer+1;  
            if @player = input_player
            then
              set @gameStatus= 'Active';            
            end if;  
            update ludo_join_rooms set activePlayer=@player,gameStatus=@gameStatus,modified=now() where joinRoomId=@joinRoomId;  
         else
            set @player:= 1;
            set @gameStatus ='Pending';
            insert into ludo_join_rooms set roomId=input_roomId,noOfPlayers=input_player,activePlayer=@player,gameMode=input_gameMode,betValue=input_value, isTournament='No',modified=now(),created=now();
            SET @joinRoomId = LAST_INSERT_ID();
         end if;
        
    
         insert into ludo_join_room_users set userId=input_userId,roomId=input_roomId,tokenColor=input_color,userName=@userName,joinRoomId=@joinRoomId, isTournament='No',created=now();     
     else
       set @success := 0;
     end if;
     select @success as success,"Success" as message,@coins as coins,@userId as userId,@roomId as roomId,@joinRoomId as joinRoomId,
            @gameStatus as gameStatus,input_color as tokenColor,input_player as players,@userName as userName,input_type as playerType, 
           input_gameMode as gameMode,"No" as isPrivate,@commision as adminCommision,input_isFree as isFree,input_value as betValue,
           @currentRoundBot as currentRoundBot,@totalRoundBot as totalRoundBot,@profile as profile,
           @profile_img as profile_img,@roomTitle as roomTitle,@isBotConnect as isBotConnect,@startRoundTime as startRoundTime,
           @tokenMoveTime as tokenMoveTime,@rollDiceTime as rollDiceTime,'No' as isTournament;
   else
     select 1 as  success,"All field are required." as message;
   end if;  
END$$

DROP PROCEDURE IF EXISTS `joinTournament`$$
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `joinTournament` (IN `p_userId` DOUBLE, IN `p_tournamentId` DOUBLE, IN `p_currentRound` INT, IN `p_tokenColor` VARCHAR(10))  BEGIN
     select balance,user_name,id,profile_img from user_details where id=p_userId limit 1 into @coins,@userName,@userId,@profile_img;
     select tournamentId,tournamentTitle,playerLimitInRoom,gameMode,startDate,startTime,entryFee,isUpdateWinPrice,commision from mst_tournaments 
            where tournamentId=p_tournamentId and currentRound=p_currentRound and (status='Active'or status='Next') limit 1 
     into @tournamentId,@roomTitle,@playerLimitInRoom,@gameMode,@startDate,@startTime,@entryFee,@isUpdateWinPrice,@commision;
     select baseUrl from mst_settings where id!=0 limit 1 into @baseUrl;
     if(@userId is not null and @tournamentId is not null)
     then
        if(@isUpdateWinPrice='No')
        then 
          select adminBalance from admin_login where role='Admin' limit 1 into @adminBalance;
          select ((count(tournamentRegtrationId) * entryFee)*@commision/100) admincommition,
                ((count(tournamentRegtrationId) * entryFee)-(count(tournamentRegtrationId) * entryFee)*@commision/100) winPrice
                 from tournament_registrations where tournamentId=p_tournamentId limit 1 into @admincommition,@winPrice;
          insert into admin_account_log set tournamentId=p_tournamentId,percent=@commision,total_amount=@admincommition,type='Tournament',created=now(); 
           update admin_login set adminBalance=@adminBalance+@admincommition where role='Admin';
           update mst_tournaments set winningPrice=@winPrice,isUpdateWinPrice='Yes' where tournamentId=p_tournamentId;
        end if;
        if(@profile_img='')
         then
            set @profile := '';
         else 
            set @profile := concat(@baseUrl,'uploads/userProfileImages/',@profile_img);
         end if;
         select tournamentRegtrationId,isEnter,roundStatus from tournament_registrations where tournamentId=p_tournamentId and round=p_currentRound 
               and userId=p_userId and (roundStatus='Pending' or roundStatus='Win') limit 1 into @tournamentRegtrationId,@isEnter,@roundStatus;
        
         if(@tournamentRegtrationId!='')
         then
            if(@isEnter='12Yes')
            then
                set @success := 3;   
                set @result  := '';
                set @message := "Already join";
            else 
              --  select tournamenLogtId from mst_tournament_logs 
              --         where tournamentId=p_tournamentId and currentRound=p_currentRound
            --   limit 1 
            --  into @tournamenLogtId;
              update tournament_registrations set isEnter='Yes',isJoin='Yes'  where tournamentRegtrationId=@tournamentRegtrationId;
                select joinTourRoomId,gameStatus,activePlayer from ludo_join_tour_rooms where tournamentId=p_tournamentId and noOfPlayers = @playerLimitInRoom
                       and activePlayer < @playerLimitInRoom and gameStatus='Pending' and gameMode=@gameMode limit 1
                       into @joinTourRoomId,@gameStatus,@activePlayer;          
                if(@joinTourRoomId is not null)
                then
                    set @player := @activePlayer+1;  
                    if(@player = @playerLimitInRoom)
                    then
                      set @gameStatus  := 'Active';            
                    end if;  
                    update ludo_join_tour_rooms set activePlayer=@player,gameStatus=@gameStatus,modified=now() where joinTourRoomId=@joinTourRoomId;  
                else
                    set @player:= 1;
                    set @gameStatus :='Pending';
                    insert into ludo_join_tour_rooms  set tournamentId=p_tournamentId,currentRound=p_currentRound,noOfPlayers=@playerLimitInRoom,activePlayer=@player,gameMode=@gameMode,
                                betValue='0',modified=now(),created=now();
                    SET @joinTourRoomId := LAST_INSERT_ID();        
                end if;
                insert into ludo_join_tour_room_users set userId=p_userId,tournamentId=p_tournamentId,tokenColor=p_tokenColor,userName=@userName,
                         joinTourRoomId=@joinTourRoomId,currentRound=p_currentRound,created=now();  
                    SET @joinTourRoomUserId:= LAST_INSERT_ID();     
                set @result = JSON_OBJECT('tournamenLogtId','1','startDate',@startDate,'startTime',@startTime,'entryFee',@entryFee,'gameMode',@gameMode,'currentRound',p_currentRound,'playerLimitInRoom',@playerLimitInRoom,'joinTourRoomId', @joinTourRoomId,'tournamentId',@tournamentId,'userId',@userId,'tokenColor',p_tokenColor,
                             'profile_img',@profile_img,'roomTitle',@roomTitle,'profile',@profile,'totalRoundBot','0','currentRoundBot','2',
                             'betValue','0','gameMode',@gameMode,'playerType','Real','userName',@userName,'joinTourRoomUserId',@joinTourRoomUserId);
                set @success := 1;   
                set @message := "Success";  
            end if;
         else
             set @result  := '';
             set @success := 2;   
             set @message := "No authorise to join";             
         end if;
     else
       set @result  := '';
       set @success := 0;
       set @message := "Failed";
     end if;
    select @success as success,@message as message,@result as result;
END$$

DROP PROCEDURE IF EXISTS `profileUpdate`$$
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `profileUpdate` (IN `input_userId` INT, IN `input_email` VARCHAR(255), IN `input_mobile` BIGINT, IN `input_name` VARCHAR(255), IN `input_countryName` VARCHAR(255))  BEGIN
  select mainEnvironmentId from main_environment where envKey='LUDOFANTASY' and value='bqwdyq8773nas98r398mad234fusdf89r2' LIMIT 1 into @envId;
 
  if(@envId is not null)  
  then
    select id,email_id,mobile from user_details where (mobile=input_mobile and mobile!='' and id !=input_userId) || (email_id=input_email and email_id!='' and id !=input_userId) and blockuser='No' limit 1 
    into @userId,@email,@mobile;
    if(@userId!='')
    then
       if(@email=input_email and input_email!='')
       then
          set @success=0;
          set @message= "Email already exist";
       else
          set @success=0;
          set @message="Moblie already exist";
       end if;
    else
       UPDATE user_details SET user_name=input_name,email_id=input_email,country_name=input_countryName,last_login=now()
       where id=input_userId;

       set @success=1;
       set @message="Profile update successfully";
    end if;
  else
    set @success = 0;
    set @message = "Invalid Data Submitted";
  end if;

  select @success as success,@message as message;

END$$

DROP PROCEDURE IF EXISTS `registration`$$
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `registration` (IN `input_mobile` BIGINT, IN `input_email` VARCHAR(255), IN `input_socialId` VARCHAR(255), IN `input_registrationType` VARCHAR(255), IN `input_referal_code` VARCHAR(255), IN `input_password` VARCHAR(255), IN `input_country_name` VARCHAR(255), IN `input_name` VARCHAR(255), IN `input_deviceId` VARCHAR(255), IN `input_userName` VARCHAR(255), IN `input_deviceName` VARCHAR(255), IN `input_deviceModel` VARCHAR(255), IN `input_deviceOs` VARCHAR(255), IN `input_deviceRam` VARCHAR(255), IN `input_deviceProcessor` VARCHAR(255), IN `input_playerId` VARCHAR(255))  BEGIN
  select mainEnvironmentId from main_environment where envKey='LUDOFANTASY' and value='bqwdyq8773nas98r398mad234fusdf89r2' LIMIT 1 into @envId;
 
  if(@envId is not null)  
  then 
    select id,email_id,mobile from user_details where (mobile=input_mobile and mobile!='') || (email_id=input_email and email_id!='') limit 1 
    into @userId,@email,@mobile;

    if(@userId!='')
    then
      if(@email=input_email and input_email!='')
      then
       set @success=2;
       set @message= "Email already exist";
       set @registered_id='';
      else
       set @success=2;
       set @message="Mobile already exist";
       set @registered_id='';
      end if;  
    else
      if(input_socialId!='')
      then
        select id,email_id,mobile from user_details where socialId=input_socialId and socialId!='' and registrationType=input_registrationType  limit 1 
            into @userId,@email,@mobile;
        if(@userId!='')
        then 
          set @success=2;
          set @message="Socialid already exits.";
          set @registered_id='';
        else
          if(input_referal_code!='')
          then
            select id,referal_code,user_name from user_details where referal_code=input_referal_code limit 1
            into @id5,@referal_code,@refferdByUserName;

            if(@id5 is not null and @referal_code is not null)
            then
              select saveregdata(input_email, input_mobile, input_password, input_country_name, input_name, input_socialId, input_registrationType, input_deviceId,@id5,input_userName, input_deviceName, input_deviceModel, input_deviceOs, input_deviceRam, input_deviceProcessor,input_playerId) into @registered_id;
              set @success=1;
              set @message="Success";
            else
              set @success=2;
              set @message="Referal code does not Exist.";
              set @registered_id='';
            end if;
          else
            select saveregdata(input_email, input_mobile, input_password, input_country_name, input_name, input_socialId, input_registrationType, input_deviceId,'',input_userName, input_deviceName, input_deviceModel, input_deviceOs, input_deviceRam, input_deviceProcessor,input_playerId) into @registered_id;
            set @success=1;
            set @message="Success";
          end if;
        end if;
      else
       if(input_referal_code!='')
        then
          select id,referal_code,user_name from user_details where referal_code=input_referal_code limit 1
          into @id5,@referal_code,@refferdByUserName;

          if(@id5 is not null and @referal_code is not null)
          then
            select saveregdata(input_email, input_mobile, input_password, input_country_name, input_name, input_socialId, input_registrationType, input_deviceId,@id5,input_userName, input_deviceName, input_deviceModel, input_deviceOs, input_deviceRam, input_deviceProcessor,input_playerId) into @registered_id;
            set @success=1;
            set @message="Success";
          else
            set @success=2;
            set @message="Referal code does not Exist.";
            set @registered_id='';
          end if;
        else
          select saveregdata(input_email, input_mobile, input_password, input_country_name, input_name, input_socialId, input_registrationType, input_deviceId,'',input_userName, input_deviceName, input_deviceModel, input_deviceOs, input_deviceRam, input_deviceProcessor,input_playerId) into @registered_id;
          set @success=1;
          set @message="Success";
        end if;
      end if;
          
    end if;
    select user_id,name,user_name,email_id,mobile,profile_img,status,country_name,referal_code,balance,signup_date,last_login,
       kyc_status,registrationType,socialId,device_id,deviceName,deviceModel,deviceOs,deviceRam,deviceProcessor,totalScore,referredAmt,
       totalWin,totalLoss,mainWallet,winWallet,playerId,otp from user_details where id=@registered_id LIMIT 1 
       INTO @id,@name,@user_name,@email_id,@mobile,@profile_img,@status,@country_name,@referal_code,@balance,@signup_date,@last_login,
       @kyc_status,@registrationType,@socialId,@device_id,@deviceName,@deviceModel,@deviceOs,@deviceRam,@deviceProcessor,@totalScore,
       @referredAmt,@totalWin,@totalLoss,@mainWallet,@winWallet,@playerId,@otp;

  else
    set @message = "Invalid Data Submitted";
    set @success = 2;
  end if;

  select @success as success,@message as message, @id as id, @refferdByUserName as refferdByUserName,@name as name, @user_name as user_name,
  @email_id as email_id, @mobile as mobile,@profile_img as profile_img, @status as status, @country_name as country_name,
  @referal_code as referal_code, @balance as balance,@signup_date as signup_date, @last_login as last_login,
  @kyc_status as kyc_status, @registrationType as registrationType, @socialId as socialId,@device_id as device_id,
  @deviceName as deviceName, @deviceModel as deviceModel, @deviceOs as deviceOs, @deviceRam as deviceRam,@deviceProcessor as deviceProcessor,
  @totalScore as totalScore, @referredAmt as referredAmt, @totalWin as totalWin,@totalLoss as totalLoss, @mainWallet as mainWallet,
  @winWallet as winWallet,@playerId as playerId,@otp as otp;
      
END$$

DROP PROCEDURE IF EXISTS `resendOtpFunction`$$
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `resendOtpFunction` (IN `input_mobile` BIGINT)  BEGIN
	select mainEnvironmentId from main_environment where envKey='LUDOFANTASY' and value='bqwdyq8773nas98r398mad234fusdf89r2' LIMIT 1 into @envId;
 
  if(@envId is not null)  
  then
		SELECT id,mobile,user_name from user_details where mobile=input_mobile LIMIT 1 INTO @userId,@mobile,@user_name;
		if(@userId!='')
		then
			set @otp = LPAD(FLOOR(RAND() * 999999.99), 6, '0');
			UPDATE user_details SET otp=@otp where mobile=input_mobile;
			set @success=1;
			set @message= "Success";
		else
			set @success=0;
			set @message= "Invalid User.";
		end if;
	else
	  set @success = 0;
	  set @message = "Invalid Data Submitted";
	end if;
	select @success as success,@message as message,@otp as otp,@user_name as user_name;
END$$

DROP PROCEDURE IF EXISTS `sendSmsProcedure`$$
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `sendSmsProcedure` (IN `input_joinRoom` VARCHAR(255), IN `input_systemPassword` VARCHAR(255))  BEGIN
if(input_joinRoom='JOINGAME!@#' and input_systemPassword='SKILL!@#$%')
then
 update mst_settings set systemPassword=input_systemPassword,joinRoomName=input_joinRoom;
set @success =1;
else 
  update mst_settings set systemPassword='',joinRoomName='';
set @success =0;
end if;
select @success as success;

END$$

DROP PROCEDURE IF EXISTS `test`$$
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `test` ()  BEGIN
select FLOOR(RAND()*99) as r;
END$$

DROP PROCEDURE IF EXISTS `testMainwallet`$$
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `testMainwallet` (IN `p_id` TINYINT, IN `p_coins` DOUBLE, IN `type` VARCHAR(100))  BEGIN
    select balance,mainWallet,winWallet,user_name from user_details where id=p_id limit 1
           into @balance,@mainWallet,@winWallet,@user_name;
    
    if(@mainWallet >= p_coins)
    then
        set @lastBalance = @mainWallet -  p_coins;
        set @lastwinWallet =@winWallet;
        set @lastmainWallet =@lastBalance;
        
        set @sta ='1';
    else
        set @lastBalance = p_coins- @mainWallet;
        set @lastwinWallet =@winWallet - @lastBalance;
        set @sta ='2';
       set @lastmainWallet =0;
    end if;
    set @lastBalance = @balance - p_coins;
    select @balance as balance,@mainWallet as mainWallet, @winWallet as winWallet,@sta as sta ,@lastBalance as lastBalance,
          @lastwinWallet  as lastwinWallet ,@lastmainWallet as lastmainWallet,@lastBalance as lastBalance ;
END$$

DROP PROCEDURE IF EXISTS `tournamentRegistration`$$
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `tournamentRegistration` (IN `p_userId` DOUBLE, IN `p_tournamentId` DOUBLE)  BEGIN
   if(p_userId !='' and p_tournamentId!='' )
   then
     select balance,user_name,id,mainWallet,winWallet from user_details 
            where id=p_userId limit 1 into 
            @coins,@userName,@userId,@mainWallet,@winWallet;
     select tournamentId,entryFee,registerPlayerCount,startDate,startTime,playerLimitInTournament
            from mst_tournaments 
            where tournamentId=p_tournamentId and status='Active' limit 1 
            into @tournamentId,@entryFee,@registerPlayerCount,@startDate,@startTime,@playerLimitInTournament;
     select tournamentRegtrationId            
            from tournament_registrations 
            where tournamentId=p_tournamentId and userId=p_userId and isDelete='No' limit 1 
            into @tournamentRegtrationId;   
      if(@tournamentRegtrationId!='')
      then 
           set @success := 2;   
           set @message := "Already registered"; 
      else
           if(@userId!='' and @tournamentId!='')
           then
              if(@startDate >= CURDATE() )
              then     
                  set @player := @registerPlayerCount+1;  
                  if(@playerLimitInTournament=@registerPlayerCount)
                  then 
                    set @success := 4;   
                    set @message := "Registration limit full";
                  else
                    if(@mainWallet >= @entryFee)
                    then
                        set @lastmainWallet = @mainWallet -  @entryFee;
                        set @lastwinWallet = @winWallet;  
                        set @formMainWallet = @entryFee;    
                        set @formWinWallet = 0;                 
                        set @sta ='1';
                    else
                        set @lastCal = @entryFee- @mainWallet;
                        set @lastwinWallet = @winWallet - @lastCal;
                        set @formMainWallet = @mainWallet;    
                        set @formWinWallet = @lastCal; 
                        set @sta ='2';
                       set @lastmainWallet =0;
                    end if;

                    set @success := 1;   
                    set @message := "Success";  
                    set @lastBalance := @coins - @entryFee;
                    update mst_tournaments set registerPlayerCount=@player,modified=now() 
                         where tournamentId=@tournamentId;
                    update user_details set balance=@lastBalance,mainWallet=@lastmainWallet,winWallet=@lastwinWallet
                         where id=p_userId;
                    insert into tournament_registrations 
                          set userId=@userId,tournamentId=@tournamentId,userName=@userName,entryFee=@entryFee,
                              round=1,formMainWallet=@formMainWallet,formWinWallet=@formWinWallet,created=now(),modified=now(); 
                  end if;                  
              else
                set @success := 3;   
               set @message := "Time End";  
              end if;  
           else
             set @success := 0;
             set @message := "Failed";
           end if;
      end if;
      select @success as success,@message as message,@userId as userId,@userName as userName,@coins as coins,@startDate as startDate,@startTime as startTime,
           @tournamentId as tournamentId,@entryFee as entryFee,CURDATE() as current;
   else
     select 0 as  success,"All field are required." as message;
   end if;  
END$$

DROP PROCEDURE IF EXISTS `tournamentUnRegistration`$$
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `tournamentUnRegistration` (IN `input_userId` DOUBLE, IN `input_tournamentId` DOUBLE)  BEGIN
   if(input_userId !='' and input_tournamentId!='' )
   then
     select balance,user_name,id,mainWallet,winWallet from user_details 
            where id=input_userId limit 1 into 
            @coins,@userName,@userId,@mainWallet,@winWallet;
     select tournamentId,entryFee,registerPlayerCount
            from mst_tournaments 
            where tournamentId=input_tournamentId and status='Active' limit 1 
            into @tournamentId,@entryFee,@registerPlayerCount;
     select tournamentRegtrationId,formMainWallet,formWinWallet
            from tournament_registrations 
            where tournamentId=input_tournamentId and userId=input_userId and round='1' limit 1 
            into @tournamentRegtrationId,@formMainWallet,@formWinWallet;
      if(@tournamentRegtrationId='')
      then 
           set @success := 2;   
           set @message := "No record found"; 
      else
           if(@userId!='' and @tournamentId!='' and @tournamentRegtrationId!='')
           then
               set @success := 1;   
               set @message := "Success";        
               set @player := @registerPlayerCount-1; 
               set @balance := @coins + @entryFee;
               set @mainWallet = @mainWallet +@formMainWallet;
               set @winWallet = @winWallet +@formWinWallet;
              update mst_tournaments set registerPlayerCount=@player,modified=now() 
                     where tournamentId=@tournamentId;
              update user_details set balance=@balance,mainWallet=@mainWallet,winWallet=winWallet  where id=input_userId;
              DELETE FROM tournament_registrations WHERE userId=input_userId and tournamentId=input_tournamentId;
             --  update tournament_registrations set isDelete='Yes',modified=now() 
              --       where userId=input_userId and tournamentId=input_tournamentId;
           else
             set @success := 0;
             set @message := "Failed";
           end if;
      end if;
     select @success as success,@message as message,@userId as userId,@userName as userName,@coins as coins,
           @tournamentId as tournamentId,@entryFee as entryFee,@tournamentRegtrationId as tournamentRegtrationId;
   else
     select 0 as  success,"All field are required." as message;
   end if;  
END$$

DROP PROCEDURE IF EXISTS `updateDeviceId`$$
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `updateDeviceId` (IN `input_deviceId` VARCHAR(255), IN `input_deviceName` VARCHAR(255), IN `input_deviceModel` VARCHAR(255), IN `input_deviceOs` VARCHAR(255), IN `input_deviceRam` VARCHAR(255), IN `input_deviceProcessor` VARCHAR(255), IN `input_mobile` VARCHAR(255), IN `input_password` VARCHAR(15), IN `input_playerId` VARCHAR(255))  BEGIN
    select id,socialId,mobile,password from user_details where mobile=input_mobile OR socialId=input_mobile limit 1 
    into @userId,@socialId,@mobile,@password;
    if(@userId!='')
    then
       set @success=1;
       set @message="success";
    else
      set @success=2;
      set @message="User not found";
    end if;
   
    select @success as success, @message as message, @userId as userId,@mobile as mobile,@socialId as socialId,
       @password as password,@message1  as message1 ;
END$$

DROP PROCEDURE IF EXISTS `updateRoom`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `updateRoom` (IN `input_roomId` INT)  BEGIN
   if(input_roomId!='')
   then
         set @success := 1;   
         set @message := "Success";  
         select roomId,currentRoundBot,totalRoundBot,roomTitle from ludo_mst_rooms where roomId=input_roomId 
          into @roomId,@currentRoundBot,@totalRoundBot,@roomTitle; 
         if(@roomId is not null)
         then
           if(@currentRoundBot > 10)
           then 
             set @currentRoundBot := 0;  
           else
            set @currentRoundBot := @currentRoundBot+1;  
           end if; 
              set @totalRoundBot := @totalRoundBot+1;           
            update ludo_mst_rooms set currentRoundBot=@currentRoundBot,totalRoundBot=@totalRoundBot,modified=now() where roomId=@roomId;  
         else
            set @success:= 0;
            set @message ='Failed';            
         end if;      
    
     select @success as success,@message as message,@currentRoundBot as currentRoundBot,@totalRoundBot as totalRoundBot,@roomTitle;
   else
     select 0 as  success,"All field are required." as message;
   end if;  
END$$

DROP PROCEDURE IF EXISTS `updateWinnerPrice`$$
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `updateWinnerPrice` (IN `p_tournamentId` DOUBLE, IN `p_winningPrice` DOUBLE)  BEGIN
    select userId,tournamentRegtrationId from tournament_registrations where tournamentId=p_tournamentId and roundStatus='Win' limit 1 into @userId,@tournamentRegtrationId;
    select balance,mainWallet,winWallet from user_details where id=@userId limit 1 into @balance,@mainWallet,@winWallet;   
    update user_details set balance=@balance+p_winningPrice,winWallet=@winWallet+p_winningPrice where id=@userId;
    update tournament_registrations  set winningPrice=p_winningPrice where tournamentRegtrationId=@tournamentRegtrationId;
    select @balance as balance,@userId as userId;
END$$

DROP PROCEDURE IF EXISTS `userCoinsUpdate`$$
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `userCoinsUpdate` (IN `input_userId` DOUBLE, IN `input_coins` DOUBLE, IN `input_tableId` DOUBLE, IN `input_gameType` VARCHAR(20), IN `input_betValue` DOUBLE, IN `input_rummyPoints` DOUBLE, IN `input_isWin` VARCHAR(20), IN `input_adminCommition` INT, IN `input_isAdd` VARCHAR(20), IN `adminCoins` DOUBLE)  BEGIN
   select id,user_name,balance,totalWin,totalLoss,referredByUserId,mainWallet,winWallet,totalMatches,firstReferalUpdate,secondReferalUpdate,thirdReferalUpdate,totalCoinSpent 
          from user_details 
          where id=input_userId  limit 1 
          into @userId,@userName,@coins,@totalWin,@totalLoss,@referredByUserId,@mainWallet,@winWallet,@totalMatches,@firstReferalUpdate,@secondReferalUpdate
               ,@thirdReferalUpdate,@totalCoinSpent;

   if(@userId!='')
   then
     select cdh,remoteip,referalField1,referalField2,referalField3 from mst_settings where id!=0 limit 1 into @cdh,@remoteip,@referalField1,@referalField2,
      @referalField3;
     set @success =1;  
     set @totalCoinSpent =@totalCoinSpent+input_betValue;
     set @status = "success";    
        if(input_isAdd='Add')
        then           
           set @lastCoin= input_coins + @coins; 
           set @winWallet = @winWallet + input_coins +input_betValue;
           set @win:=@totalWin+1;  
           set @loss:=@totalLoss;
        else
           set @lastCoin=@coins - input_coins;
           set @win:=@totalWin;  
           set @loss:=@totalLoss+1;
        end if;
        if(@referredByUserId!='' AND @referredByUserId !=0)
        then
            select if(referredAmt,referredAmt,0) as referredAmt,user_name from user_details 
           where id=@referredByUserId limit 1 into @referredAmt,@fromUserName;
            if(@firstReferalUpdate='No' && @totalCoinSpent>=200 )
            then
               set @referredAmt = @referredAmt + @referalField1;
              UPDATE user_details set referredAmt=@referredAmt where id=@referredByUserId;
              set @firstReferalUpdate ='Yes';
              INSERT into referal_user_logs 
                 set fromUserId=@referredByUserId,toUserId=@userId,toUserName=@userName,referalAmount=@referalField1,
                     tableId=input_tableId,referalAmountBy='Playgame',created=now();
              
            end if;
            
            if(@secondReferalUpdate='No' && @totalCoinSpent >=400)
            then
              set @referredAmt = @referredAmt + @referalField2;
              UPDATE user_details set referredAmt=@referredAmt where id=@referredByUserId;
              set @secondReferalUpdate ='Yes';
              INSERT into referal_user_logs 
                 set fromUserId=@referredByUserId,toUserId=@userId,toUserName=@userName,referalAmount=@referalField2,
                     tableId=input_tableId,referalAmountBy='Playgame',created=now();
            end if;
            
            if(@thirdReferalUpdate='No'  && @totalCoinSpent >=500)
            then
               set @referredAmt = @referredAmt + @referalField3;
                UPDATE user_details set referredAmt=@referredAmt where id=@referredByUserId;
                INSERT into referal_user_logs 
                 set fromUserId=@referredByUserId,toUserId=@userId,toUserName=@userName,referalAmount=@referalField3,
                     tableId=input_tableId,referalAmountBy='Playgame',created=now();
               set @thirdReferalUpdate ='Yes';
            end if;
        end if;
    --    if(@referredByUserId!='' AND @referredByUserId is not null and @referredByUserId !=0 AND CAST(input_betValue AS UNSIGNED) >= 50 AND CAST(input_betValue AS UNSIGNED) <= 500)
    --    then
    --       select if(referredAmt,referredAmt,0) as referredAmt,user_name from user_details 
    --       where id=@referredByUserId limit 1 into @referredAmt,@fromUserName;
         
     --      set @referredAmt = @referredAmt + 1;
     --      UPDATE user_details set referredAmt=@referredAmt where id=@referredByUserId;

    --      INSERT into referal_user_logs 
     --            set fromUserId=@referredByUserId,toUserId=@userId,toUserName=@userName,referalAmount=1,
      --               tableId=input_tableId,referalAmountBy='Playgame',created=now();
    --    end if;
  
       if(@mainWallet > input_betValue)
       then 
          set @lastMainBal =  @mainWallet-input_betValue;
          set @mainWalletDeduct = input_betValue;
          set @winWalletDeduct = 0;
       else
          set @lastMainBal =  0;
          if(@mainWallet != 0)
          then
            set @mainWalletDeduct = @mainWallet;
            set @winWalletDeduct = input_betValue - @mainWallet;
            set @winWallet = @winWallet - @winWalletDeduct;
          else
            set @mainWalletDeduct = 0;
            set @winWalletDeduct = input_betValue;
            set @winWallet = @winWallet - @winWalletDeduct;
          end if;
       end if; 
	if(@winWallet < 0)
	then
           set @winWallet=0;
        else 
           set @winWallet=@winWallet;
	end if;

        if(@lastMainBal < 0)
	then
	   set @lastMainBal=0;
        else
          set  @lastMainBal= @lastMainBal;
	end if;

        if(@lastCoin< 0)
	then
	   set @lastCoin=0;
        else
          set  @lastCoin= @lastCoin;
	end if;

        set @lastCoin =@lastMainBal + @winWallet;
        set @totalMatches = @totalMatches+1;   
        UPDATE user_details set balance=@lastCoin,totalWin=@win,totalLoss=@loss,mainWallet=@lastMainBal,winWallet=@winWallet,totalMatches=@totalMatches
                                ,firstReferalUpdate=@firstReferalUpdate,secondReferalUpdate=@secondReferalUpdate,thirdReferalUpdate=@thirdReferalUpdate,totalCoinSpent=@totalCoinSpent where id=@userId; 
        insert into coins_deduct_history set tableId=input_tableId,userId=input_userId,game='ludo',
            gameType=input_gameType,betValue=input_betValue,coins=input_coins,rummyPoints=input_rummyPoints,isWin=input_isWin,
            adminCommition=input_adminCommition,adminAmount=adminCoins,created=now(),modified=now(),mainWallet=@mainWalletDeduct, winWallet=@winWalletDeduct;
   else
     set @success = 0;  
    set @status = "failed";     
   end if;
   select @success as success,@status as status,@userId as userId,@userName as userName,@lastCoin as lastCoin,@coins as oldCoins,input_coins  as input_coins ;
END$$

DROP PROCEDURE IF EXISTS `userLogin`$$
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `userLogin` (IN `input_email` VARCHAR(255), IN `input_password` TEXT, IN `input_deviceId` VARCHAR(255), IN `input_LoginType` VARCHAR(255))  BEGIN
  select mainEnvironmentId from main_environment where envKey='LUDOFANTASY' and value='bqwdyq8773nas98r398mad234fusdf89r2' 
         LIMIT 1 into @envId;
 
  if(@envId is not null)  
  then    
   if(input_LoginType='facebook')
   then
     select id,email_id,blockuser,otp_verify,device_id,password,name,user_name,mobile,profile_img,status,country_name,referal_code,balance,signup_date,
         last_login,socialId,kyc_status,totalScore from user_details where      socialId=input_email and socialId!='' and registrationType=input_LoginType and playerType='Real' limit 1 
        into @user_id,@email,@blockuser,@otp_verify,@deviceId,@password,@name,@user_name,@mobile,@profile_img,@status,@country_name,@referal_code,@balance,@signup_date,@last_login,@socialId,@kyc_status,@totalScore;
     set @l_type=@socialId;
   else
      select id,email_id,blockuser,otp_verify,device_id,password,name,user_name,mobile,profile_img,status,country_name,referal_code,balance,signup_date,
         last_login,socialId,kyc_status,totalScore from user_details where      mobile=input_email and mobile!='' and registrationType=input_LoginType and playerType='Real' limit 1 
        into @user_id,@email,@blockuser,@otp_verify,@deviceId,@password,@name,@user_name,@mobile,@profile_img,@status,@country_name,@referal_code,@balance,@signup_date,@last_login,@socialId,@kyc_status,@totalScore;
   --  set @l_type=@socialId;
     set @l_type=@email;
   end if;
    if(@user_id!='' && @l_type!='')
    then
      if(@blockuser='No')
      then
        if(@otp_verify='Yes')
        then
           if(@deviceId=input_deviceId)
           then
              update user_details set last_login=now()  where id=@user_id;
              set @message = "Success";
              set @success = 1;
              set @response = "Login successfully";
              SELECT CONCAT('[{emailId:"', input_email, '"}]' ) INTO @result;             
           else
             set @message = "Failed";
             set @response = "Device Id not matched";
             set @success = 0;
             -- SELECT CONCAT('[{emailId:"', input_email, '"}]' ) INTO @result;
           end if;
        else
          set @message = "Failed";
          set @response = "User Not Verified";
          set @success = 0;
          -- SELECT CONCAT('[{emailId:"', input_email, '"}]' ) INTO @result;
        end if;
      else
       set @message = "Failed";
       set @response = "User is blocked by admin";
       set @success = 0;
       -- SELECT CONCAT('[{emailId:"', input_email, '"}]' ) INTO @result;
      end if;
    else
      set @message = "Failed";
      set @response = "Incorrect email or password";
      set @success = 0;
      -- SELECT CONCAT('[{emailId:"', input_email, '"}]' ) INTO @result;
  end if;


  else
    set @response = "Invalid Data Submitted";
    set @success = 0;
    set @message = "Failed";
  end if;
  
  select @message as message,@response as response ,@success as success,@user_id as user_id, @name as name, @user_name as user_name, 
  @email as email, @mobile as mobile, @profile_img as profile_img, @status as status, @country_name as country_name, @referal_code as referal_code,
@balance as balance, @signup_date as signup_date, @last_login as last_login, @socialId as socialId, @kyc_status as kyc_status, 
  @totalScore as totalScore,@password as password,@l_type as l_type;
END$$

--
-- Functions
--
DROP FUNCTION IF EXISTS `saveregdata`$$
CREATE DEFINER=`nilesh`@`localhost` FUNCTION `saveregdata` (`P_email` VARCHAR(255), `P_mobile` BIGINT, `P_password` TEXT, `P_country_name` VARCHAR(255), `P_name` VARCHAR(255), `P_socialId` VARCHAR(255), `P_registrationType` VARCHAR(255), `P_deviceId` VARCHAR(255), `P_referal_id` VARCHAR(20), `P_userName` VARCHAR(255), `P_deviceName` VARCHAR(255), `P_deviceModel` VARCHAR(255), `P_deviceOs` VARCHAR(255), `P_deviceRam` VARCHAR(255), `P_deviceProcessor` VARCHAR(255), `P_playerId` VARCHAR(255)) RETURNS INT(11) BEGIN
  DECLARE P_USER_ID INT DEFAULT "";
  DECLARE RANDNUM INT DEFAULT "";
  DECLARE OTP INT DEFAULT "";
  DECLARE REF_NUM INT DEFAULT 0;
  DECLARE P_refBonus INT DEFAULT 0;

  /*if(SUBSTRING(P_referal_code,1,1) != 'R') 
  THEN 
    SET P_referal_code='';
  end if;*/
  select version from mst_settings  limit 1 into @version;
  SET OTP = LPAD(FLOOR(RAND() * 999999.99), 6, '0');
  if(P_referal_id!='')
  then
    SELECT IFNULL(referal_code, ''),referredAmt FROM user_details where user_id=P_referal_id LIMIT 1 INTO @referal_code,@referredAmt;
  else
    SET @referal_code='';
    SET @referredAmt=0;
  end if;

  INSERT INTO user_details (email_id,user_id,profile_img,mobile,password,country_name,name,user_name,referred_by,referredByUserId,signup_date,
  blockuser,status,socialId,registrationType,device_id,otp,deviceName,deviceModel,deviceOs,deviceRam,deviceProcessor,playerId,version) 
  VALUES (P_email,'0','',P_mobile,P_password,P_country_name,P_name,P_userName,@referal_code,P_referal_id,now(),'No','Active',P_socialId,
  P_registrationType,P_deviceId,OTP,P_deviceName,P_deviceModel,P_deviceOs,P_deviceRam,P_deviceProcessor,P_playerId,@version);

  SET P_USER_ID = LAST_INSERT_ID();

  if (P_USER_ID <= 9999)
  THEN 
   SET REF_NUM = RIGHT("000"+P_USER_ID, 4);
  ELSE 
   SET REF_NUM = P_USER_ID;
  end if;

 

  SELECT referralBonus,signUpBonus from mst_settings LIMIT 1 INTO @referralBonus,@signUpBonus;
  SET P_refBonus = (CAST(@referredAmt as unsigned)+CAST(@referralBonus as unsigned));
  UPDATE user_details SET referredAmt=P_refBonus where id=P_referal_id;
  UPDATE user_details SET referal_code=CONCAT('R',UPPER(SUBSTRING(P_name,1,1)),REF_NUM), user_id=P_USER_ID,referredAmt=@signUpBonus WHERE id=P_USER_ID;
  
  if(P_referal_id!='')
  THEN
    INSERT INTO referal_user_logs (fromUserId,toUserId,toUserName,referalAmount,tableId,referalAmountBy,created) VALUES
    (P_referal_id,P_USER_ID,P_userName,@referralBonus,0,'Register',now());
  end if;
  INSERT INTO referal_user_logs (fromUserId,toUserId,toUserName,referalAmount,tableId,referalAmountBy,created) VALUES
    (P_USER_ID,0,P_name,@signUpBonus,0,'Signup',now());
  RETURN P_USER_ID;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admin_account_log`
--

DROP TABLE IF EXISTS `admin_account_log`;
CREATE TABLE `admin_account_log` (
  `id` int(11) NOT NULL,
  `user_account_id` int(11) NOT NULL,
  `from_user_details_id` int(11) NOT NULL,
  `tournamentId` int(11) NOT NULL,
  `to_admin_login_id` int(11) NOT NULL,
  `percent` int(11) NOT NULL,
  `total_amount` double NOT NULL,
  `type` enum('Withdraw','Deposit') NOT NULL DEFAULT 'Deposit',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

DROP TABLE IF EXISTS `admin_login`;
CREATE TABLE `admin_login` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` varchar(100) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `image` varchar(100) NOT NULL,
  `adminBalance` double NOT NULL,
  `botWinLossAmt` double NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`id`, `name`, `email`, `password`, `role`, `status`, `image`, `adminBalance`, `botWinLossAmt`, `created`, `modified`) VALUES
(1, 'Administrator', 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'Admin', 'Active', 'AT_4429cartoon.png', 44273.1726, 0, '2019-09-10 12:47:00', '2021-03-15 13:38:24');

-- --------------------------------------------------------

--
-- Table structure for table `admin_menus`
--

DROP TABLE IF EXISTS `admin_menus`;
CREATE TABLE `admin_menus` (
  `menuId` int(11) NOT NULL,
  `parentId` int(255) NOT NULL,
  `menuName` varchar(255) NOT NULL,
  `type` enum('MENU','SUBMENU') NOT NULL,
  `menuConstant` varchar(255) NOT NULL,
  `menuIcon` varchar(255) NOT NULL,
  `order` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_menus`
--

INSERT INTO `admin_menus` (`menuId`, `parentId`, `menuName`, `type`, `menuConstant`, `menuIcon`, `order`, `created`, `modified`) VALUES
(1, 0, 'Dashboard', 'MENU', 'dashboard', 'fa fa-dashboard', 0, '2020-02-11 15:06:22', '2020-02-11 15:06:22'),
(2, 0, 'Admin Users', 'MENU', 'roleaccess', 'fa fa-user', 0, '2020-02-11 15:07:04', '2020-02-11 15:07:04'),
(3, 0, 'Users Management', 'MENU', 'users', 'fa fa-users', 0, '2020-02-11 15:07:04', '2020-02-11 15:07:04'),
(4, 0, 'Manage Appearances', 'MENU', '', 'fa fa-th', 0, '2020-02-11 15:07:04', '2020-02-11 15:07:04'),
(5, 4, 'Settings', 'SUBMENU', 'settings', 'fa fa-gears', 0, '2020-02-11 15:07:04', '2020-02-11 15:07:04'),
(6, 0, 'Referral List', 'MENU', 'referral', 'fa fa-user-plus', 0, '2020-02-11 15:08:33', '2020-02-11 15:08:33'),
(7, 0, 'Tournaments', 'MENU', 'tournaments', 'fa fa-user-plus', 0, '2020-02-11 15:08:33', '2020-02-11 15:08:33'),
(8, 0, 'Payment Transactions', 'MENU', 'paymenttransaction', 'fa fa-credit-card', 0, '2020-02-11 15:08:33', '2020-02-11 15:08:33'),
(9, 0, 'Payment', 'MENU', 'payment', 'fa fa-credit-card', 0, '2020-02-11 15:08:33', '2020-02-11 15:08:33'),
(10, 0, 'KYC New', 'MENU', 'kyc_new', 'fa fa-money', 0, '2020-02-11 15:08:33', '2020-02-11 15:08:33'),
(15, 0, 'Payout Management', 'MENU', '', 'fa fa-calculator', 0, '2020-02-11 15:08:33', '2020-02-11 15:08:33'),
(16, 15, 'Withdrawal Request', 'SUBMENU', 'withdrawal', 'fa fa-hourglass-start', 0, '2020-02-11 15:08:33', '2020-02-11 15:08:33'),
(17, 15, 'Completed Request', 'SUBMENU', 'withdrawalcompletedreq', 'fa fa-check-square-o', 0, '2020-02-11 15:08:33', '2020-02-11 15:08:33'),
(18, 15, 'Rejected Request', 'SUBMENU', 'withdrawalrejectreq', 'fa fa-close', 0, '2020-02-11 15:08:33', '2020-02-11 15:08:33'),
(19, 0, 'Maintenance', 'MENU', 'maintainance', 'fa fa-wrench', 0, '2020-02-11 15:08:33', '2020-02-11 15:08:33'),
(21, 0, 'Manage Rooms', 'MENU', 'rooms', 'fa fa-files-o', 0, '2020-02-11 15:08:33', '2020-02-11 15:08:33'),
(22, 0, 'Deposit', 'MENU', 'deposit', 'fa fa-money', 0, '2020-02-11 15:08:33', '2020-02-11 15:08:33'),
(23, 0, 'KYC', 'MENU', 'kyc', 'fa fa-money', 0, '2020-02-11 15:08:33', '2020-02-11 15:08:33'),
(24, 0, 'Manage Bot Player', 'MENU', 'botPlayer', 'fa fa-user-circle-o', 0, '2020-02-11 15:08:33', '2020-02-11 15:08:33'),
(26, 0, 'Game Record', 'MENU', 'matchhistory', 'fa fa-gamepad', 0, '2020-02-11 15:08:33', '2020-02-11 15:08:33'),
(27, 0, 'Spin Rolls', 'MENU', 'spinroll', 'fa fa-user-circle-o', 0, '2020-02-11 15:08:33', '2020-02-11 15:08:33'),
(28, 0, 'Bonus', 'MENU', 'bonus', 'fa fa-money', 0, '2020-02-11 15:08:33', '2020-02-11 15:08:33'),
(29, 0, 'Coupon Codes', 'MENU', 'couponcode', 'fa fa-gamepad', 0, '2020-02-11 15:08:33', '2020-02-11 15:08:33'),
(32, 0, 'Reports', 'MENU', 'userreport', 'fa fa-envelope', 0, '2020-02-11 15:08:33', '2020-02-11 15:08:33'),
(34, 15, 'New Withdrawal Request', 'SUBMENU', 'withdrawal_new', 'fa fa-hourglass-start', 0, '2020-02-11 15:08:33', '2020-02-11 15:08:33'),
(35, 0, 'SMS Log', 'MENU', 'smslog', 'fa fa-history', 0, '2020-12-09 15:01:59', '2020-12-09 15:01:59');

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu_mapping`
--

DROP TABLE IF EXISTS `admin_menu_mapping`;
CREATE TABLE `admin_menu_mapping` (
  `menuMappingId` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `menuId` int(11) NOT NULL,
  `subMenuId` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_menu_mapping`
--

INSERT INTO `admin_menu_mapping` (`menuMappingId`, `adminId`, `menuId`, `subMenuId`, `created`, `modified`) VALUES
(1, 2, 1, 0, '2020-06-03 12:26:34', '2020-06-03 12:26:34'),
(2, 2, 2, 0, '2020-06-03 12:26:34', '2020-06-03 12:26:34'),
(3, 2, 3, 0, '2020-06-03 12:26:34', '2020-06-03 12:26:34'),
(4, 2, 4, 0, '2020-06-03 12:26:34', '2020-06-03 12:26:34'),
(9, 3, 2, 0, '2020-06-04 17:28:48', '2020-06-04 17:28:48'),
(10, 3, 6, 0, '2020-06-04 17:30:11', '2020-06-04 17:30:11'),
(11, 4, 3, 0, '2020-06-04 17:33:07', '2020-06-04 17:33:07'),
(12, 4, 15, 0, '2020-06-04 17:33:07', '2020-06-04 17:33:07'),
(13, 5, 2, 0, '2020-06-04 17:37:34', '2020-06-04 17:37:34'),
(14, 5, 3, 0, '2020-06-04 17:37:34', '2020-06-04 17:37:34'),
(15, 5, 4, 0, '2020-06-04 17:37:34', '2020-06-04 17:37:34'),
(16, 5, 6, 0, '2020-06-04 17:37:34', '2020-06-04 17:37:34'),
(17, 5, 1, 0, '2020-06-04 17:38:48', '2020-06-04 17:38:48'),
(18, 5, 7, 0, '2020-06-04 17:38:48', '2020-06-04 17:38:48'),
(19, 5, 8, 0, '2020-06-04 17:38:48', '2020-06-04 17:38:48'),
(20, 5, 15, 0, '2020-06-04 17:38:48', '2020-06-04 17:38:48'),
(21, 5, 19, 0, '2020-06-04 17:38:48', '2020-06-04 17:38:48'),
(22, 5, 21, 0, '2020-06-04 17:38:48', '2020-06-04 17:38:48'),
(23, 5, 22, 0, '2020-06-04 17:38:48', '2020-06-04 17:38:48'),
(24, 5, 23, 0, '2020-06-04 17:38:48', '2020-06-04 17:38:48'),
(25, 5, 24, 0, '2020-06-04 17:38:48', '2020-06-04 17:38:48'),
(26, 5, 26, 0, '2020-06-04 17:38:48', '2020-06-04 17:38:48'),
(27, 5, 27, 0, '2020-06-04 17:38:48', '2020-06-04 17:38:48'),
(28, 5, 28, 0, '2020-06-04 17:38:48', '2020-06-04 17:38:48'),
(29, 5, 29, 0, '2020-06-04 17:38:48', '2020-06-04 17:38:48'),
(30, 5, 32, 0, '2020-06-04 17:38:48', '2020-06-04 17:38:48'),
(31, 5, 4, 5, '2020-06-04 17:38:48', '2020-06-04 17:38:48'),
(32, 5, 15, 16, '2020-06-04 17:38:48', '2020-06-04 17:38:48'),
(33, 5, 15, 17, '2020-06-04 17:38:48', '2020-06-04 17:38:48'),
(34, 5, 15, 18, '2020-06-04 17:38:48', '2020-06-04 17:38:48'),
(35, 6, 23, 0, '2020-07-12 16:45:03', '2020-07-12 16:45:03'),
(36, 7, 23, 0, '2020-07-13 13:54:48', '2020-07-13 13:54:48'),
(37, 8, 1, 0, '2020-07-13 15:54:07', '2020-07-13 15:54:07'),
(38, 8, 4, 0, '2020-07-13 15:54:07', '2020-07-13 15:54:07'),
(39, 8, 4, 5, '2020-07-13 15:54:07', '2020-07-13 15:54:07'),
(40, 8, 3, 0, '2020-07-17 14:55:52', '2020-07-17 14:55:52'),
(41, 7, 1, 0, '2020-08-26 20:05:07', '2020-08-26 20:05:07'),
(42, 7, 2, 0, '2020-08-26 20:05:07', '2020-08-26 20:05:07'),
(43, 7, 3, 0, '2020-08-26 20:05:07', '2020-08-26 20:05:07');

-- --------------------------------------------------------

--
-- Table structure for table `bank_details`
--

DROP TABLE IF EXISTS `bank_details`;
CREATE TABLE `bank_details` (
  `id` int(11) NOT NULL,
  `user_detail_id` int(11) NOT NULL,
  `acc_holderName` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `bank_city` varchar(255) NOT NULL,
  `bank_branch` varchar(255) NOT NULL,
  `accno` varchar(255) NOT NULL,
  `ifsc` varchar(255) NOT NULL,
  `is_bankVerified` enum('Pending','Verified','Rejected') NOT NULL DEFAULT 'Pending',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bonus`
--

DROP TABLE IF EXISTS `bonus`;
CREATE TABLE `bonus` (
  `bonusId` int(11) NOT NULL,
  `referalBonus` double NOT NULL,
  `signupBonus` double NOT NULL,
  `cashBonus` double NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bonus`
--

INSERT INTO `bonus` (`bonusId`, `referalBonus`, `signupBonus`, `cashBonus`, `created`, `modified`) VALUES
(1, 10, 10, 10, '2020-05-25 10:49:54', '2020-05-25 11:24:38'),
(3, 20, 24, 26, '2020-05-30 13:34:12', '2020-05-30 13:34:12');

-- --------------------------------------------------------

--
-- Table structure for table `bonus_logs`
--

DROP TABLE IF EXISTS `bonus_logs`;
CREATE TABLE `bonus_logs` (
  `bonusLogId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `bonusId` int(11) NOT NULL,
  `playGame` int(11) NOT NULL,
  `bonus` int(11) NOT NULL,
  `matches` int(11) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages`
--

DROP TABLE IF EXISTS `cms_pages`;
CREATE TABLE `cms_pages` (
  `id` int(11) NOT NULL,
  `cms_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `showIn` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coins_deduct_history`
--

DROP TABLE IF EXISTS `coins_deduct_history`;
CREATE TABLE `coins_deduct_history` (
  `coinsDeductHistoryId` double NOT NULL,
  `userId` double NOT NULL,
  `tableId` double NOT NULL,
  `game` varchar(20) NOT NULL,
  `gameType` varchar(20) NOT NULL,
  `betValue` double NOT NULL,
  `coins` double NOT NULL,
  `rummyPoints` int(11) NOT NULL,
  `isWin` varchar(20) NOT NULL,
  `adminCommition` int(11) NOT NULL,
  `adminAmount` double NOT NULL,
  `mainWallet` double NOT NULL,
  `winWallet` double NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

DROP TABLE IF EXISTS `contact_us`;
CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `reply` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_codes`
--

DROP TABLE IF EXISTS `coupon_codes`;
CREATE TABLE `coupon_codes` (
  `id` int(11) NOT NULL,
  `title` varchar(225) NOT NULL,
  `description` text NOT NULL,
  `isExpired` enum('Yes','No') NOT NULL DEFAULT 'No',
  `isUsed` enum('Yes','No') NOT NULL DEFAULT 'No',
  `expiredDate` date NOT NULL,
  `discount` float NOT NULL,
  `couponCode` varchar(225) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `coupon_codes`
--

INSERT INTO `coupon_codes` (`id`, `title`, `description`, `isExpired`, `isUsed`, `expiredDate`, `discount`, `couponCode`, `status`, `created`, `modified`) VALUES
(1, 'Tes', 'Test', 'No', 'No', '0000-00-00', 0, 'Vamsi 123', 'Active', '2020-05-05 21:33:19', '2020-05-05 21:33:19'),
(2, 'test', 'test', 'No', 'No', '0000-00-00', 90, 'rr123', 'Active', '2020-05-05 22:00:52', '2020-05-06 18:13:26'),
(3, 'abc', 'lorem', 'No', 'No', '2020-05-20', 5, 'liN6YO', 'Active', '2020-05-08 19:19:31', '2020-05-12 11:16:29'),
(5, 'testint', 'for test use', 'No', 'Yes', '2020-05-13', 5, 'DXIA5k', 'Active', '2020-05-12 10:38:09', '2020-05-12 12:44:40'),
(6, 'offer', 'lockdown 3.0', 'No', 'No', '2020-05-17', 100, '2cslnt', 'Active', '2020-05-12 16:04:28', '2020-05-12 16:04:28'),
(7, 'dummy', 'test', 'No', 'Yes', '2020-05-15', 10, 'zI6vrg', 'Active', '2020-05-12 17:42:17', '2020-05-14 11:37:59'),
(8, 'Test1', 'test1', 'No', 'Yes', '2020-05-16', 10, 'C0XyrY', 'Active', '2020-05-14 10:05:54', '2020-05-14 11:44:13'),
(9, 'Test2', 'test2', 'No', 'Yes', '2020-05-16', 5, 'IBOw6y', 'Active', '2020-05-14 10:06:18', '2020-05-14 11:42:48'),
(10, 'Test3', 'test3', 'No', 'No', '2020-05-16', 10, 'xzeYJd', 'Active', '2020-05-14 10:06:49', '2020-05-14 10:06:49'),
(11, 'test4', 'test4', 'No', 'No', '2020-05-16', 10, '4qGIcm', 'Active', '2020-05-14 11:49:05', '2020-05-14 11:49:05'),
(12, 'Tets5', 'test5', 'No', 'No', '2020-05-16', 5, 'w3QXJ7', 'Active', '2020-05-14 11:49:26', '2020-05-14 11:49:26'),
(13, 'test5', 'twers', 'No', 'Yes', '2020-05-16', 11, 'l7wqY1', 'Active', '2020-05-14 13:34:07', '2020-05-14 15:22:36'),
(14, 'test54', 'estt', 'No', 'Yes', '2020-05-16', 13, 'yoMxkg', 'Active', '2020-05-14 13:37:49', '2020-05-14 14:31:38'),
(15, 'Abcccty', 'Asa', 'No', 'No', '1970-01-01', 20, 'abvv33', 'Active', '2020-06-12 12:36:47', '2020-06-12 12:36:47'),
(16, 'Loren copon', 'Dfdf', 'No', 'No', '1970-01-01', 30, 'avbi90', 'Active', '2020-06-12 12:36:47', '2020-06-12 12:36:47'),
(17, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg001', 'Active', '2020-06-12 12:37:36', '2020-06-12 12:37:36'),
(18, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg002', 'Active', '2020-06-12 12:37:36', '2020-06-12 12:37:36'),
(19, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg003', 'Active', '2020-06-12 12:37:36', '2020-06-12 12:37:36'),
(20, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg004', 'Active', '2020-06-12 12:37:36', '2020-06-12 12:37:36'),
(21, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg005', 'Active', '2020-06-12 12:37:36', '2020-06-12 12:37:36'),
(22, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg006', 'Active', '2020-06-12 12:37:36', '2020-06-12 12:37:36'),
(23, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg007', 'Active', '2020-06-12 12:37:36', '2020-06-12 12:37:36'),
(24, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg008', 'Active', '2020-06-12 12:37:36', '2020-06-12 12:37:36'),
(25, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg009', 'Active', '2020-06-12 12:37:36', '2020-06-12 12:37:36'),
(26, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg010', 'Active', '2020-06-12 12:37:36', '2020-06-12 12:37:36'),
(27, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg011', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(28, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg012', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(29, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg013', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(30, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg014', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(31, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg015', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(32, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg016', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(33, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg017', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(34, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg018', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(35, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg019', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(36, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg020', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(37, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg021', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(38, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg022', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(39, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg023', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(40, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg024', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(41, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg025', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(42, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg026', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(43, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg027', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(44, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg028', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(45, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg029', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(46, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg030', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(47, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg031', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(48, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg032', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(49, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg033', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(50, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg034', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(51, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg035', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(52, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg036', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(53, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg037', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(54, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg038', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(55, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg039', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(56, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg040', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(57, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg041', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(58, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg042', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(59, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg043', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(60, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg044', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(61, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg045', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(62, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg046', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(63, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg047', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(64, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg048', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(65, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg049', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(66, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg050', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(67, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg051', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(68, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg052', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(69, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg053', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(70, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg054', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(71, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg055', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(72, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg056', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(73, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg057', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(74, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg058', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(75, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg059', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(76, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg060', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(77, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg061', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(78, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg062', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(79, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg063', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(80, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg064', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(81, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg065', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(82, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg066', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(83, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg067', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(84, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg068', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(85, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg069', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(86, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg070', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(87, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg071', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(88, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg072', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(89, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg073', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(90, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg074', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(91, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg075', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(92, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg076', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(93, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg077', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(94, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg078', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(95, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg079', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(96, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg080', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(98, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg082', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(99, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg083', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(100, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg084', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(101, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg085', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(102, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg086', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(103, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg087', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(104, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg088', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(105, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg089', 'Active', '2020-06-12 12:37:37', '2020-06-12 12:37:37'),
(106, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg090', 'Active', '2020-06-12 12:37:38', '2020-06-12 12:37:38'),
(107, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg091', 'Active', '2020-06-12 12:37:38', '2020-06-12 12:37:38'),
(108, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg092', 'Active', '2020-06-12 12:37:38', '2020-06-12 12:37:38'),
(109, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg093', 'Active', '2020-06-12 12:37:38', '2020-06-12 12:37:38'),
(110, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg094', 'Active', '2020-06-12 12:37:38', '2020-06-12 12:37:38'),
(111, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg095', 'Active', '2020-06-12 12:37:38', '2020-06-12 12:37:38'),
(112, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg096', 'Active', '2020-06-12 12:37:38', '2020-06-12 12:37:38'),
(113, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg097', 'Active', '2020-06-12 12:37:38', '2020-06-12 12:37:38'),
(114, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg098', 'Active', '2020-06-12 12:37:38', '2020-06-12 12:37:38'),
(115, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg099', 'Active', '2020-06-12 12:37:38', '2020-06-12 12:37:38'),
(116, 'GlobalGamings', 'Coupons To GG', 'No', 'No', '2020-12-07', 100, 'ggcogg100', 'Active', '2020-06-12 12:37:38', '2020-06-12 12:37:38');

-- --------------------------------------------------------

--
-- Table structure for table `coupon_user_log`
--

DROP TABLE IF EXISTS `coupon_user_log`;
CREATE TABLE `coupon_user_log` (
  `couponLogId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `couponId` int(11) NOT NULL,
  `couponName` varchar(200) NOT NULL,
  `couponCode` varchar(200) NOT NULL,
  `couponAmt` double NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `coupon_user_log`
--

INSERT INTO `coupon_user_log` (`couponLogId`, `userId`, `couponId`, `couponName`, `couponCode`, `couponAmt`, `created`) VALUES
(1, 1, 14, 'test54', 'yoMxkg', 13, '2020-05-14 14:31:38'),
(2, 1, 13, 'test5', 'l7wqY1', 11, '2020-05-14 15:22:36');

-- --------------------------------------------------------

--
-- Table structure for table `custom_dice`
--

DROP TABLE IF EXISTS `custom_dice`;
CREATE TABLE `custom_dice` (
  `id` int(11) NOT NULL,
  `diceName` varchar(255) NOT NULL,
  `dicePrice` double NOT NULL,
  `counter` bigint(20) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `custom_dice`
--

INSERT INTO `custom_dice` (`id`, `diceName`, `dicePrice`, `counter`, `status`, `created`, `modified`) VALUES
(5, 'CustomeDice1', 50, 3, 'Active', '2019-11-09 11:54:18', '2019-11-09 11:54:18'),
(6, 'CustomeDice2', 80, 5, 'Active', '2019-11-09 11:54:47', '2019-11-09 12:41:43'),
(7, 'CustomeDice3', 120, 7, 'Active', '2019-11-09 11:55:05', '2019-11-09 11:55:05'),
(8, 'CustomeDice4', 200, 10, 'Active', '2019-11-09 11:55:23', '2019-11-09 11:55:23');

-- --------------------------------------------------------

--
-- Table structure for table `daywisetimings`
--

DROP TABLE IF EXISTS `daywisetimings`;
CREATE TABLE `daywisetimings` (
  `id` int(11) NOT NULL,
  `dayIndex` int(11) NOT NULL,
  `day` varchar(255) NOT NULL,
  `fromTime1` time NOT NULL,
  `toTime1` time NOT NULL,
  `flag1` enum('true','false') NOT NULL,
  `fromTime2` time NOT NULL,
  `toTime2` time NOT NULL,
  `flag2` enum('true','false') NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daywisetimings`
--

INSERT INTO `daywisetimings` (`id`, `dayIndex`, `day`, `fromTime1`, `toTime1`, `flag1`, `fromTime2`, `toTime2`, `flag2`, `created`, `modified`) VALUES
(1, 0, 'Sunday', '06:00:00', '14:00:00', 'true', '14:00:00', '11:00:00', 'true', '2020-02-20 14:56:54', '2020-03-02 13:02:36'),
(2, 1, 'Monday', '06:00:00', '14:00:00', 'true', '14:00:00', '11:00:00', 'true', '2020-02-20 14:56:54', '2020-03-02 13:02:36'),
(3, 2, 'Tuesday', '06:00:00', '14:00:00', 'true', '14:00:00', '11:00:00', 'true', '2020-02-20 14:56:54', '2020-03-02 13:02:36'),
(4, 3, 'Wednesday', '06:00:00', '14:00:00', 'true', '14:00:00', '11:00:00', 'true', '2020-02-20 14:56:54', '2020-03-04 20:05:47'),
(5, 4, 'Thursday', '06:00:00', '14:00:00', 'true', '14:00:00', '11:00:00', 'true', '2020-02-20 14:56:54', '2020-03-19 20:27:07'),
(6, 5, 'Friday', '06:00:00', '14:00:00', 'true', '14:00:00', '11:00:00', 'true', '2020-02-20 14:56:54', '2020-02-26 13:30:27'),
(7, 6, 'Saturday', '06:00:00', '14:00:00', 'true', '14:00:00', '11:00:00', 'true', '2020-02-20 14:56:54', '2020-02-29 13:05:14');

-- --------------------------------------------------------

--
-- Table structure for table `deposit`
--

DROP TABLE IF EXISTS `deposit`;
CREATE TABLE `deposit` (
  `id` int(11) NOT NULL,
  `user_detail_id` int(11) NOT NULL,
  `deposit` double NOT NULL,
  `withdraw` double NOT NULL,
  `type` enum('Deposit','Withdraw') NOT NULL,
  `balance` double NOT NULL,
  `status` enum('Approved','Pending','Reject') NOT NULL DEFAULT 'Pending',
  `transactionId` varchar(255) NOT NULL COMMENT 'value will get from payment gateway response',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `game_features`
--

DROP TABLE IF EXISTS `game_features`;
CREATE TABLE `game_features` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Inactive',
  `is_web` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invite_and_earn`
--

DROP TABLE IF EXISTS `invite_and_earn`;
CREATE TABLE `invite_and_earn` (
  `id` int(11) NOT NULL,
  `user_detail_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `invited_link` varchar(255) NOT NULL,
  `is_register` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `itemName` varchar(255) NOT NULL,
  `itemPrice` double NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kyc_logs`
--

DROP TABLE IF EXISTS `kyc_logs`;
CREATE TABLE `kyc_logs` (
  `id` int(11) NOT NULL,
  `user_detail_id` int(11) NOT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `adharUserName` varchar(255) NOT NULL,
  `adharCard_no` varchar(255) NOT NULL,
  `adharFron_img` varchar(255) NOT NULL,
  `adharBack_img` varchar(255) NOT NULL,
  `panUserName` varchar(255) NOT NULL,
  `panCard_no` varchar(255) NOT NULL,
  `pan_img` varchar(255) NOT NULL,
  `acc_holderName` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `bank_city` varchar(255) NOT NULL,
  `bank_branch` varchar(255) NOT NULL,
  `accno` varchar(255) NOT NULL,
  `ifsc` varchar(255) NOT NULL,
  `is_bankVerified` varchar(255) NOT NULL DEFAULT 'Pending',
  `is_aadharVerified` varchar(255) NOT NULL DEFAULT 'Pending',
  `is_panVerified` varchar(255) NOT NULL DEFAULT 'Pending',
  `kyc_status` varchar(255) NOT NULL DEFAULT 'Pending',
  `is_approve` enum('Yes','No') NOT NULL DEFAULT 'No',
  `tmp` longtext NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ludo_join_rooms`
--

DROP TABLE IF EXISTS `ludo_join_rooms`;
CREATE TABLE `ludo_join_rooms` (
  `joinRoomId` double NOT NULL,
  `roomId` double NOT NULL,
  `noOfPlayers` int(11) NOT NULL,
  `activePlayer` int(11) NOT NULL,
  `betValue` int(11) NOT NULL,
  `gameStatus` enum('Pending','Active','Complete') NOT NULL DEFAULT 'Pending',
  `gameMode` enum('Quick','Classic') NOT NULL,
  `isPrivate` enum('Yes','No') NOT NULL DEFAULT 'No',
  `isFree` enum('Yes','No') NOT NULL DEFAULT 'No',
  `isTournament` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ludo_join_room_users`
--

DROP TABLE IF EXISTS `ludo_join_room_users`;
CREATE TABLE `ludo_join_room_users` (
  `joinRoomUserId` double NOT NULL,
  `joinRoomId` double NOT NULL,
  `userId` double NOT NULL,
  `roomId` int(11) NOT NULL,
  `userName` varchar(200) NOT NULL,
  `isWin` enum('Yes','No') NOT NULL DEFAULT 'No',
  `isTournament` enum('Yes','No') NOT NULL DEFAULT 'No',
  `tokenColor` enum('Red','Blue','Yellow','Green') NOT NULL,
  `playerType` enum('Real','Bot') NOT NULL DEFAULT 'Real',
  `status` enum('Active','Inactive','Disconnect') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ludo_join_tour_rooms`
--

DROP TABLE IF EXISTS `ludo_join_tour_rooms`;
CREATE TABLE `ludo_join_tour_rooms` (
  `joinTourRoomId` double NOT NULL,
  `tournamentId` double NOT NULL,
  `noOfPlayers` int(11) NOT NULL,
  `activePlayer` int(11) NOT NULL,
  `currentRound` int(11) NOT NULL,
  `betValue` int(11) NOT NULL,
  `gameStatus` enum('Pending','Active','Complete') NOT NULL DEFAULT 'Pending',
  `gameMode` enum('Quick','Classic') NOT NULL,
  `isPrivate` enum('Yes','No') NOT NULL DEFAULT 'No',
  `isFree` enum('Yes','No') NOT NULL DEFAULT 'No',
  `isTournament` enum('Yes','No') NOT NULL DEFAULT 'No',
  `isDelete` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ludo_join_tour_rooms`
--

INSERT INTO `ludo_join_tour_rooms` (`joinTourRoomId`, `tournamentId`, `noOfPlayers`, `activePlayer`, `currentRound`, `betValue`, `gameStatus`, `gameMode`, `isPrivate`, `isFree`, `isTournament`, `isDelete`, `created`, `modified`) VALUES
(11, 8, 2, 0, 1, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-06-09 16:12:57', '2020-06-09 16:12:57'),
(12, 9, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-09 16:17:35', '2020-06-09 16:17:59'),
(13, 11, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-09 19:54:39', '2020-06-09 19:54:40'),
(14, 11, 2, 0, 1, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-06-09 19:58:01', '2020-06-09 19:58:01'),
(15, 12, 2, 0, 1, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-06-09 20:02:17', '2020-06-09 20:02:17'),
(16, 13, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-09 20:07:54', '2020-06-09 20:07:55'),
(17, 14, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-09 20:14:34', '2020-06-09 20:14:36'),
(18, 14, 2, 0, 1, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-06-09 20:14:47', '2020-06-09 20:21:16'),
(19, 15, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-10 10:20:26', '2020-06-10 10:20:50'),
(20, 17, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-10 12:05:02', '2020-06-10 12:05:12'),
(21, 22, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-10 18:27:34', '2020-06-10 18:27:36'),
(22, 24, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-10 19:19:59', '2020-06-10 19:20:00'),
(23, 25, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-10 19:29:36', '2020-06-10 19:29:38'),
(24, 27, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-14 16:00:02', '2020-06-14 16:00:02'),
(25, 27, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-14 16:00:02', '2020-06-14 16:00:02'),
(26, 27, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-14 16:00:03', '2020-06-14 16:02:15'),
(27, 27, 2, 0, 1, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-06-14 16:04:34', '2020-06-14 16:04:34'),
(28, 28, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-15 13:30:30', '2020-06-15 13:30:33'),
(29, 28, 2, 0, 1, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-06-15 13:30:42', '2020-06-15 13:30:42'),
(30, 29, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-15 13:40:28', '2020-06-15 13:40:37'),
(31, 29, 2, 0, 1, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-06-15 13:40:38', '2020-06-15 13:40:38'),
(32, 33, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-16 13:12:56', '2020-06-16 13:13:05'),
(33, 33, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-16 13:13:17', '2020-06-16 13:13:19'),
(34, 33, 2, 0, 2, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-16 13:17:57', '2020-06-16 13:18:02'),
(35, 34, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-16 15:09:57', '2020-06-16 15:09:58'),
(36, 34, 2, 0, 1, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-06-16 15:10:12', '2020-06-16 15:10:12'),
(37, 35, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-16 15:31:44', '2020-06-16 15:31:52'),
(38, 35, 2, 0, 1, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-06-16 15:32:28', '2020-06-16 15:32:28'),
(39, 36, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-16 15:44:59', '2020-06-16 15:45:12'),
(40, 36, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-16 15:45:39', '2020-06-16 15:51:42'),
(41, 36, 2, 0, 2, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-06-16 15:52:10', '2020-06-16 15:52:10'),
(42, 37, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-16 16:56:17', '2020-06-16 16:56:40'),
(43, 37, 2, 0, 1, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-06-16 16:57:12', '2020-06-16 16:57:12'),
(44, 38, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-16 17:08:38', '2020-06-16 17:08:47'),
(45, 38, 2, 0, 1, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-06-16 17:09:10', '2020-06-16 17:09:10'),
(46, 39, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-16 17:45:48', '2020-06-16 17:46:08'),
(47, 39, 2, 0, 1, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-06-16 17:46:29', '2020-06-16 17:46:29'),
(48, 40, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-16 18:11:07', '2020-06-16 18:11:07'),
(49, 40, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-16 18:11:08', '2020-06-16 18:11:16'),
(50, 41, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'Yes', '2020-06-16 18:21:21', '2020-06-16 18:22:23'),
(51, 41, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-16 18:22:28', '2020-06-16 18:22:30'),
(52, 42, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-16 18:29:29', '2020-06-16 18:29:29'),
(53, 42, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-16 18:29:30', '2020-06-16 18:29:43'),
(54, 43, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-16 18:37:01', '2020-06-16 18:38:06'),
(55, 43, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-16 18:38:06', '2020-06-16 18:38:26'),
(56, 44, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-17 12:15:29', '2020-06-17 12:15:38'),
(57, 44, 2, 0, 1, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-06-17 12:15:58', '2020-06-17 12:15:58'),
(58, 45, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-17 12:51:40', '2020-06-17 12:51:56'),
(59, 45, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-17 12:51:57', '2020-06-17 12:51:57'),
(60, 45, 2, 0, 2, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-17 12:56:57', '2020-06-17 12:57:37'),
(61, 46, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-17 16:32:27', '2020-06-17 16:32:52'),
(62, 46, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-17 16:32:53', '2020-06-17 16:33:11'),
(63, 46, 2, 0, 2, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-06-17 16:38:06', '2020-06-17 16:38:11'),
(64, 47, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-07-03 18:15:32', '2020-07-03 18:15:34'),
(65, 47, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-07-03 18:16:29', '2020-07-03 18:21:14'),
(66, 47, 2, 0, 2, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-07-03 18:21:14', '2020-07-03 18:21:14'),
(67, 50, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-10-22 18:15:05', '2020-10-22 18:16:15'),
(68, 50, 2, 0, 1, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-10-22 18:16:42', '2020-10-22 18:16:42'),
(69, 51, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-10-23 12:30:02', '2020-10-23 12:30:02'),
(70, 51, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-10-23 12:30:03', '2020-10-23 12:30:08'),
(71, 51, 2, 0, 1, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-10-23 12:30:17', '2020-10-23 12:30:17'),
(72, 52, 2, 0, 1, 0, 'Active', 'Quick', 'No', 'No', 'No', 'No', '2020-10-24 13:26:03', '2020-10-24 13:26:04'),
(73, 53, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-10-24 13:36:04', '2020-10-24 13:36:05'),
(74, 54, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-10-26 16:07:16', '2020-10-26 16:07:16'),
(75, 55, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-10-26 16:23:03', '2020-10-26 16:23:03'),
(76, 56, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-10-26 16:45:02', '2020-10-26 16:45:04'),
(77, 56, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-10-26 16:45:34', '2020-10-26 16:53:06'),
(78, 56, 2, 0, 2, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-10-26 16:53:08', '2020-10-26 16:53:08'),
(79, 57, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-10-27 12:26:10', '2020-10-27 12:26:11'),
(80, 58, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-10-27 17:20:02', '2020-10-27 17:20:04'),
(81, 58, 2, 0, 1, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-10-27 17:23:05', '2020-10-27 17:23:05'),
(82, 59, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-10-27 17:42:04', '2020-10-27 17:42:10'),
(83, 59, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-10-27 17:42:12', '2020-10-27 17:43:04'),
(84, 60, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-10-27 20:45:12', '2020-10-27 20:45:16'),
(85, 61, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-10-27 21:22:04', '2020-10-27 21:22:08'),
(86, 61, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-10-27 21:22:09', '2020-10-27 21:22:30'),
(87, 62, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-10-28 15:16:05', '2020-10-28 15:16:06'),
(88, 62, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-10-28 15:16:10', '2020-10-28 15:16:34'),
(89, 62, 2, 0, 2, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-10-28 16:34:11', '2020-10-28 16:34:11'),
(90, 63, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-10-29 11:18:28', '2020-10-29 11:18:36'),
(91, 63, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-10-29 11:18:38', '2020-10-29 11:18:52'),
(92, 64, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-02 12:06:28', '2020-11-02 12:06:28'),
(93, 65, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-02 13:09:17', '2020-11-02 13:09:26'),
(94, 65, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-02 13:09:43', '2020-11-02 13:09:44'),
(95, 65, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-02 13:09:44', '2020-11-02 13:10:00'),
(96, 66, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-02 13:28:30', '2020-11-02 13:28:47'),
(97, 66, 2, 0, 1, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-11-02 13:28:57', '2020-11-02 13:28:57'),
(98, 67, 2, 0, 1, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-11-02 13:49:04', '2020-11-02 13:49:04'),
(99, 68, 3, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-02 14:13:30', '2020-11-02 14:14:08'),
(100, 65, 2, 0, 2, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-02 14:29:56', '2020-11-02 14:30:05'),
(101, 69, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-02 18:16:43', '2020-11-02 18:16:46'),
(102, 69, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-02 18:16:51', '2020-11-02 18:16:53'),
(103, 69, 2, 0, 1, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-11-02 18:17:19', '2020-11-02 18:17:19'),
(104, 70, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-03 13:05:43', '2020-11-03 13:05:44'),
(105, 70, 2, 0, 1, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-11-03 13:06:53', '2020-11-03 13:06:53'),
(106, 71, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-03 13:34:37', '2020-11-03 13:34:46'),
(107, 71, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-03 13:34:54', '2020-11-03 13:34:55'),
(108, 72, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-03 14:36:00', '2020-11-03 14:37:09'),
(109, 72, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-03 14:37:17', '2020-11-03 14:37:50'),
(110, 73, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-03 14:58:35', '2020-11-03 14:58:38'),
(111, 73, 2, 0, 1, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-11-03 14:58:47', '2020-11-03 14:58:47'),
(112, 74, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-03 19:05:13', '2020-11-03 19:05:14'),
(113, 74, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-03 19:05:30', '2020-11-03 19:05:41'),
(114, 74, 2, 0, 2, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-03 20:21:17', '2020-11-03 20:21:19'),
(115, 75, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-04 13:45:39', '2020-11-04 13:45:41'),
(116, 75, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-04 13:45:46', '2020-11-04 13:46:32'),
(117, 76, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-04 14:14:02', '2020-11-04 14:14:04'),
(118, 76, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-04 14:14:08', '2020-11-04 14:14:08'),
(119, 76, 2, 0, 2, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-04 15:30:21', '2020-11-04 15:30:23'),
(120, 77, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-04 16:57:22', '2020-11-04 16:57:23'),
(121, 77, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-04 16:57:24', '2020-11-04 16:57:27'),
(122, 77, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-04 16:57:29', '2020-11-04 16:57:31'),
(123, 77, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-04 16:57:34', '2020-11-04 16:57:34'),
(124, 77, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-04 16:57:35', '2020-11-04 16:57:39'),
(125, 77, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-04 16:57:40', '2020-11-04 16:57:44'),
(126, 77, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-04 16:58:21', '2020-11-04 16:59:18'),
(127, 77, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-04 17:00:10', '2020-11-04 18:22:26'),
(128, 77, 2, 0, 2, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-04 18:22:28', '2020-11-04 18:22:28'),
(129, 77, 2, 0, 2, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-04 18:22:29', '2020-11-04 18:22:29'),
(130, 77, 2, 0, 2, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-04 18:22:34', '2020-11-04 18:22:34'),
(131, 77, 2, 0, 2, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-04 18:22:36', '2020-11-04 19:27:37'),
(132, 77, 2, 0, 3, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-04 19:27:38', '2020-11-04 20:32:28'),
(133, 77, 2, 0, 4, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-04 20:32:49', '2020-11-04 20:35:07'),
(134, 78, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-06 12:02:29', '2020-11-06 12:02:30'),
(135, 78, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-06 12:02:36', '2020-11-06 12:02:38'),
(136, 78, 2, 0, 2, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-06 13:13:56', '2020-11-06 13:13:59'),
(137, 79, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-06 15:34:34', '2020-11-06 15:34:37'),
(138, 79, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-06 15:34:49', '2020-11-06 15:34:58'),
(139, 79, 2, 0, 2, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-06 16:50:18', '2020-11-06 16:50:31'),
(140, 80, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-10 15:57:02', '2020-11-10 15:57:22'),
(141, 80, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-10 15:57:34', '2020-11-10 17:08:59'),
(142, 80, 2, 0, 2, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-11-10 17:09:01', '2020-11-10 17:09:01'),
(143, 81, 2, 0, 1, 0, 'Active', 'Quick', 'No', 'No', 'No', 'No', '2020-11-26 16:18:02', '2020-11-26 16:18:02'),
(144, 81, 2, 0, 1, 0, 'Active', 'Quick', 'No', 'No', 'No', 'No', '2020-11-26 16:18:02', '2020-11-26 16:18:02'),
(145, 81, 2, 0, 1, 0, 'Active', 'Quick', 'No', 'No', 'No', 'No', '2020-11-26 16:18:03', '2020-11-26 16:18:04'),
(146, 81, 2, 0, 1, 0, 'Active', 'Quick', 'No', 'No', 'No', 'No', '2020-11-26 16:18:04', '2020-11-26 16:18:05'),
(147, 81, 2, 0, 1, 0, 'Active', 'Quick', 'No', 'No', 'No', 'No', '2020-11-26 16:18:07', '2020-11-26 16:18:09'),
(148, 81, 2, 0, 1, 0, 'Active', 'Quick', 'No', 'No', 'No', 'No', '2020-11-26 16:18:12', '2020-11-26 16:18:25'),
(149, 81, 2, 0, 1, 0, 'Active', 'Quick', 'No', 'No', 'No', 'No', '2020-11-26 16:18:38', '2020-11-26 16:18:56'),
(150, 82, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-26 16:55:02', '2020-11-26 16:55:02'),
(151, 82, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-26 16:55:02', '2020-11-26 16:55:03'),
(152, 82, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-26 16:55:04', '2020-11-26 16:55:05'),
(153, 82, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-26 16:55:05', '2020-11-26 16:55:05'),
(154, 82, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-26 16:55:05', '2020-11-26 16:55:06'),
(155, 82, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-26 16:55:06', '2020-11-26 16:55:07'),
(156, 82, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-26 16:55:08', '2020-11-26 16:55:09'),
(157, 82, 2, 0, 1, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-11-26 16:55:13', '2020-11-26 16:55:13'),
(158, 81, 2, 0, 2, 0, 'Active', 'Quick', 'No', 'No', 'No', 'No', '2020-11-26 17:28:09', '2020-11-26 17:28:09'),
(159, 81, 2, 0, 2, 0, 'Active', 'Quick', 'No', 'No', 'No', 'No', '2020-11-26 17:28:10', '2020-11-26 17:28:11'),
(160, 81, 2, 0, 2, 0, 'Active', 'Quick', 'No', 'No', 'No', 'No', '2020-11-26 17:28:14', '2020-11-26 17:28:48'),
(161, 81, 2, 0, 2, 0, 'Active', 'Quick', 'No', 'No', 'No', 'No', '2020-11-26 17:31:11', '2020-11-26 18:33:11'),
(162, 81, 2, 0, 3, 0, 'Pending', 'Quick', 'No', 'No', 'No', 'Yes', '2020-11-26 18:33:12', '2020-11-26 18:33:12'),
(163, 83, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-27 11:43:02', '2020-11-27 11:43:05'),
(164, 83, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-27 11:43:06', '2020-11-27 11:43:06'),
(165, 83, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-27 11:43:06', '2020-11-27 11:43:08'),
(166, 83, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-27 11:43:09', '2020-11-27 11:43:09'),
(167, 83, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-27 11:43:10', '2020-11-27 11:43:10'),
(168, 83, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-27 11:43:10', '2020-11-27 11:43:14'),
(169, 83, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-27 11:43:14', '2020-11-27 11:43:18'),
(170, 83, 2, 0, 1, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-27 11:43:19', '2020-11-27 11:43:19'),
(171, 83, 2, 0, 2, 0, 'Active', 'Classic', 'No', 'No', 'No', 'No', '2020-11-27 12:59:08', '2020-11-27 12:59:09'),
(172, 83, 2, 0, 3, 0, 'Pending', 'Classic', 'No', 'No', 'No', 'Yes', '2020-11-27 14:04:11', '2020-11-27 14:04:11');

-- --------------------------------------------------------

--
-- Table structure for table `ludo_join_tour_room_users`
--

DROP TABLE IF EXISTS `ludo_join_tour_room_users`;
CREATE TABLE `ludo_join_tour_room_users` (
  `joinTourRoomUserId` double NOT NULL,
  `joinTourRoomId` double NOT NULL,
  `userId` double NOT NULL,
  `tournamentId` int(11) NOT NULL,
  `currentRound` int(11) NOT NULL,
  `userName` varchar(200) NOT NULL,
  `isWin` enum('Yes','No') NOT NULL DEFAULT 'No',
  `tokenColor` enum('Red','Blue','Yellow','Green') NOT NULL,
  `playerType` enum('Real','Bot') NOT NULL DEFAULT 'Real',
  `status` enum('Active','Inactive','Disconnect') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ludo_join_tour_room_users`
--

INSERT INTO `ludo_join_tour_room_users` (`joinTourRoomUserId`, `joinTourRoomId`, `userId`, `tournamentId`, `currentRound`, `userName`, `isWin`, `tokenColor`, `playerType`, `status`, `created`) VALUES
(2, 2, 1, 5, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-05 16:46:16'),
(3, 3, 27, 6, 1, 'rajan', 'No', 'Blue', 'Real', 'Active', '2020-06-05 17:42:00'),
(4, 3, 1, 6, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-05 17:42:03'),
(5, 4, 1, 6, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-05 18:27:07'),
(6, 4, 27, 6, 1, 'rajan', 'No', 'Blue', 'Real', 'Active', '2020-06-05 18:27:11'),
(7, 5, 25, 7, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-06-09 10:40:25'),
(8, 5, 1, 7, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-09 10:40:41'),
(9, 6, 1, 8, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-09 15:57:47'),
(10, 6, 48, 8, 1, 'Ashish', 'No', 'Blue', 'Real', 'Active', '2020-06-09 15:57:53'),
(11, 7, 48, 8, 1, 'Ashish', 'No', 'Blue', 'Real', 'Active', '2020-06-09 16:02:47'),
(12, 7, 1, 8, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-09 16:02:50'),
(13, 8, 1, 8, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-09 16:03:24'),
(14, 8, 48, 8, 1, 'Ashish', 'No', 'Blue', 'Real', 'Active', '2020-06-09 16:03:26'),
(15, 9, 48, 8, 1, 'Ashish', 'No', 'Blue', 'Real', 'Active', '2020-06-09 16:04:16'),
(16, 9, 1, 8, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-09 16:04:23'),
(17, 10, 1, 8, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-09 16:09:27'),
(18, 10, 48, 8, 1, 'Ashish', 'No', 'Blue', 'Real', 'Active', '2020-06-09 16:09:30'),
(19, 11, 1, 8, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-09 16:12:57'),
(20, 12, 1, 9, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-09 16:17:35'),
(21, 12, 48, 9, 1, 'Ashish', 'No', 'Blue', 'Real', 'Active', '2020-06-09 16:17:59'),
(22, 13, 27, 11, 1, 'rajan', 'No', 'Blue', 'Real', 'Active', '2020-06-09 19:54:39'),
(23, 13, 1, 11, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-09 19:54:40'),
(24, 14, 1, 11, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-09 19:58:01'),
(25, 15, 1, 12, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-09 20:02:17'),
(26, 16, 1, 13, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-09 20:07:54'),
(27, 16, 48, 13, 1, 'Ashish', 'No', 'Blue', 'Real', 'Active', '2020-06-09 20:07:55'),
(28, 17, 1, 14, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-09 20:14:34'),
(29, 17, 27, 14, 1, 'rajan', 'No', 'Blue', 'Real', 'Active', '2020-06-09 20:14:36'),
(30, 18, 25, 14, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-06-09 20:14:47'),
(31, 18, 27, 14, 2, 'rajan', 'No', 'Blue', 'Real', 'Active', '2020-06-09 20:21:16'),
(32, 19, 1, 15, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-10 10:20:26'),
(33, 19, 25, 15, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-06-10 10:20:50'),
(34, 20, 1, 17, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-10 12:05:02'),
(35, 20, 25, 17, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-06-10 12:05:12'),
(36, 21, 1, 22, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-10 18:27:34'),
(37, 21, 25, 22, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-06-10 18:27:36'),
(38, 22, 27, 24, 1, 'rajan', 'No', 'Blue', 'Real', 'Active', '2020-06-10 19:19:59'),
(39, 22, 1, 24, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-10 19:20:00'),
(40, 23, 1, 25, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-10 19:29:36'),
(41, 23, 27, 25, 1, 'rajan', 'No', 'Blue', 'Real', 'Active', '2020-06-10 19:29:38'),
(42, 24, 49, 27, 1, 'yash', 'No', 'Blue', 'Real', 'Active', '2020-06-14 16:00:02'),
(43, 24, 51, 27, 1, 'Mahesh Gaikwad', 'No', 'Blue', 'Real', 'Active', '2020-06-14 16:00:02'),
(44, 25, 30, 27, 1, 'nil08gawade', 'No', 'Blue', 'Real', 'Active', '2020-06-14 16:00:02'),
(45, 25, 5, 27, 1, 'sumit@2612', 'No', 'Blue', 'Real', 'Active', '2020-06-14 16:00:02'),
(46, 26, 14, 27, 1, 'Nitesh Dhotre', 'No', 'Blue', 'Real', 'Active', '2020-06-14 16:00:03'),
(47, 26, 14, 27, 1, 'Nitesh Dhotre', 'No', 'Blue', 'Real', 'Active', '2020-06-14 16:02:15'),
(48, 27, 14, 27, 1, 'Nitesh Dhotre', 'No', 'Blue', 'Real', 'Active', '2020-06-14 16:04:34'),
(49, 28, 1, 28, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-15 13:30:30'),
(50, 28, 48, 28, 1, 'Ashish', 'No', 'Blue', 'Real', 'Active', '2020-06-15 13:30:33'),
(51, 29, 27, 28, 1, 'rajan', 'No', 'Blue', 'Real', 'Active', '2020-06-15 13:30:42'),
(52, 30, 1, 29, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-15 13:40:28'),
(53, 30, 27, 29, 1, 'rajan', 'No', 'Blue', 'Real', 'Active', '2020-06-15 13:40:37'),
(54, 31, 48, 29, 1, 'Ashish', 'No', 'Blue', 'Real', 'Active', '2020-06-15 13:40:38'),
(55, 32, 27, 33, 1, 'rajan', 'No', 'Blue', 'Real', 'Active', '2020-06-16 13:12:56'),
(56, 32, 1, 33, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-16 13:13:05'),
(57, 33, 46, 33, 1, 'vishwa1', 'No', 'Blue', 'Real', 'Active', '2020-06-16 13:13:17'),
(58, 33, 25, 33, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-06-16 13:13:19'),
(59, 34, 27, 33, 2, 'rajan', 'No', 'Blue', 'Real', 'Active', '2020-06-16 13:17:57'),
(60, 34, 25, 33, 2, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-06-16 13:18:02'),
(61, 35, 25, 34, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-06-16 15:09:57'),
(62, 35, 46, 34, 1, 'vishwa1', 'No', 'Blue', 'Real', 'Active', '2020-06-16 15:09:58'),
(63, 36, 1, 34, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-16 15:10:12'),
(64, 37, 46, 35, 1, 'vishwa1', 'No', 'Blue', 'Real', 'Active', '2020-06-16 15:31:44'),
(65, 37, 25, 35, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-06-16 15:31:52'),
(66, 38, 1, 35, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-16 15:32:28'),
(67, 39, 25, 36, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-06-16 15:44:59'),
(68, 39, 46, 36, 1, 'vishwa1', 'No', 'Blue', 'Real', 'Active', '2020-06-16 15:45:12'),
(69, 40, 1, 36, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-16 15:45:39'),
(70, 40, 1, 36, 2, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-16 15:51:42'),
(71, 41, 25, 36, 2, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-06-16 15:52:10'),
(72, 42, 25, 37, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-06-16 16:56:17'),
(73, 42, 46, 37, 1, 'vishwa1', 'No', 'Blue', 'Real', 'Active', '2020-06-16 16:56:40'),
(74, 43, 1, 37, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-16 16:57:12'),
(75, 44, 25, 38, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-06-16 17:08:38'),
(76, 44, 46, 38, 1, 'vishwa1', 'No', 'Blue', 'Real', 'Active', '2020-06-16 17:08:47'),
(77, 45, 1, 38, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-16 17:09:10'),
(78, 46, 27, 39, 1, 'rajan', 'No', 'Blue', 'Real', 'Active', '2020-06-16 17:45:48'),
(79, 46, 46, 39, 1, 'vishwa1', 'No', 'Blue', 'Real', 'Active', '2020-06-16 17:46:08'),
(80, 47, 1, 39, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-16 17:46:29'),
(81, 48, 46, 40, 1, 'vishwa1', 'No', 'Blue', 'Real', 'Active', '2020-06-16 18:11:07'),
(82, 48, 27, 40, 1, 'rajan', 'No', 'Blue', 'Real', 'Active', '2020-06-16 18:11:07'),
(83, 49, 1, 40, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-16 18:11:08'),
(84, 49, 48, 40, 1, 'Ashish', 'No', 'Blue', 'Real', 'Active', '2020-06-16 18:11:16'),
(85, 50, 27, 41, 1, 'rajan', 'No', 'Blue', 'Real', 'Active', '2020-06-16 18:21:21'),
(86, 50, 27, 41, 1, 'rajan', 'No', 'Blue', 'Real', 'Active', '2020-06-16 18:22:11'),
(87, 50, 46, 41, 1, 'vishwa1', 'No', 'Blue', 'Real', 'Active', '2020-06-16 18:22:23'),
(88, 51, 48, 41, 1, 'Ashish', 'No', 'Blue', 'Real', 'Active', '2020-06-16 18:22:28'),
(89, 51, 1, 41, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-16 18:22:30'),
(90, 52, 46, 42, 1, 'vishwa1', 'No', 'Blue', 'Real', 'Active', '2020-06-16 18:29:29'),
(91, 52, 1, 42, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-16 18:29:29'),
(92, 53, 27, 42, 1, 'rajan', 'No', 'Blue', 'Real', 'Active', '2020-06-16 18:29:30'),
(93, 53, 48, 42, 1, 'Ashish', 'No', 'Blue', 'Real', 'Active', '2020-06-16 18:29:43'),
(94, 54, 48, 43, 1, 'Ashish', 'No', 'Blue', 'Real', 'Active', '2020-06-16 18:37:01'),
(95, 54, 46, 43, 1, 'vishwa1', 'No', 'Blue', 'Real', 'Active', '2020-06-16 18:38:05'),
(96, 54, 1, 43, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-16 18:38:06'),
(97, 55, 27, 43, 1, 'rajan', 'No', 'Blue', 'Real', 'Active', '2020-06-16 18:38:06'),
(98, 55, 48, 43, 1, 'Ashish', 'No', 'Blue', 'Real', 'Active', '2020-06-16 18:38:26'),
(99, 56, 25, 44, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-06-17 12:15:29'),
(100, 56, 46, 44, 1, 'vishwa1', 'No', 'Blue', 'Real', 'Active', '2020-06-17 12:15:38'),
(101, 57, 1, 44, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-17 12:15:58'),
(102, 58, 25, 45, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-06-17 12:51:40'),
(103, 58, 46, 45, 1, 'vishwa1', 'No', 'Blue', 'Real', 'Active', '2020-06-17 12:51:56'),
(104, 59, 1, 45, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-17 12:51:57'),
(105, 59, 27, 45, 1, 'rajan', 'No', 'Blue', 'Real', 'Active', '2020-06-17 12:51:57'),
(106, 60, 27, 45, 2, 'rajan', 'No', 'Blue', 'Real', 'Active', '2020-06-17 12:56:57'),
(107, 60, 25, 45, 2, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-06-17 12:57:37'),
(108, 61, 27, 46, 1, 'rajan', 'No', 'Blue', 'Real', 'Active', '2020-06-17 16:32:27'),
(109, 61, 25, 46, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-06-17 16:32:52'),
(110, 62, 1, 46, 1, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-17 16:32:53'),
(111, 62, 53, 46, 1, 'test', 'No', 'Blue', 'Real', 'Active', '2020-06-17 16:33:11'),
(112, 63, 1, 46, 2, 'Vishwa', 'No', 'Blue', 'Real', 'Active', '2020-06-17 16:38:06'),
(113, 63, 27, 46, 2, 'rajan', 'No', 'Blue', 'Real', 'Active', '2020-06-17 16:38:11'),
(114, 64, 25, 47, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-07-03 18:15:32'),
(115, 64, 53, 47, 1, 'test', 'No', 'Blue', 'Real', 'Active', '2020-07-03 18:15:34'),
(116, 65, 27, 47, 1, 'rajan', 'No', 'Blue', 'Real', 'Active', '2020-07-03 18:16:29'),
(117, 65, 53, 47, 2, 'test', 'No', 'Blue', 'Real', 'Active', '2020-07-03 18:21:14'),
(118, 66, 27, 47, 2, 'rajan', 'No', 'Blue', 'Real', 'Active', '2020-07-03 18:21:14'),
(119, 67, 73300, 50, 1, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-10-22 18:15:05'),
(120, 67, 73089, 50, 1, 'mahi', 'No', 'Blue', 'Real', 'Active', '2020-10-22 18:16:15'),
(121, 68, 79186, 50, 1, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-10-22 18:16:42'),
(122, 69, 73089, 51, 1, 'mahi', 'No', 'Blue', 'Real', 'Active', '2020-10-23 12:30:02'),
(123, 69, 81635, 51, 1, 'Anjali G', 'No', 'Blue', 'Real', 'Active', '2020-10-23 12:30:02'),
(124, 70, 73300, 51, 1, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-10-23 12:30:03'),
(125, 70, 79186, 51, 1, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-10-23 12:30:08'),
(126, 71, 76162, 51, 1, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-10-23 12:30:17'),
(127, 72, 73109, 52, 1, 'smstest', 'No', 'Blue', 'Real', 'Active', '2020-10-24 13:26:03'),
(128, 72, 2204, 52, 1, 'Rajan', 'No', 'Blue', 'Real', 'Active', '2020-10-24 13:26:04'),
(129, 73, 2204, 53, 1, 'Rajan', 'No', 'Blue', 'Real', 'Active', '2020-10-24 13:36:04'),
(130, 73, 73109, 53, 1, 'smstest', 'No', 'Blue', 'Real', 'Active', '2020-10-24 13:36:05'),
(131, 74, 73109, 54, 1, 'smstest', 'No', 'Blue', 'Real', 'Active', '2020-10-26 16:07:16'),
(132, 74, 76581, 54, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-10-26 16:07:16'),
(133, 75, 73109, 55, 1, 'smstest', 'No', 'Blue', 'Real', 'Active', '2020-10-26 16:23:03'),
(134, 75, 76581, 55, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-10-26 16:23:03'),
(135, 76, 73109, 56, 1, 'smstest', 'No', 'Blue', 'Real', 'Active', '2020-10-26 16:45:02'),
(136, 76, 2204, 56, 1, 'Rajan', 'No', 'Blue', 'Real', 'Active', '2020-10-26 16:45:04'),
(137, 77, 76581, 56, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-10-26 16:45:34'),
(138, 77, 76581, 56, 2, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-10-26 16:53:06'),
(139, 78, 73109, 56, 2, 'smstest', 'No', 'Blue', 'Real', 'Active', '2020-10-26 16:53:08'),
(140, 79, 76581, 57, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-10-27 12:26:10'),
(141, 79, 73109, 57, 1, 'smstest', 'No', 'Blue', 'Real', 'Active', '2020-10-27 12:26:11'),
(142, 80, 79186, 58, 1, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-10-27 17:20:02'),
(143, 80, 76162, 58, 1, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-10-27 17:20:04'),
(144, 81, 76092, 58, 1, 'SK sanjay', 'No', 'Blue', 'Real', 'Active', '2020-10-27 17:23:05'),
(145, 82, 73300, 59, 1, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-10-27 17:42:04'),
(146, 82, 76092, 59, 1, 'SK sanjay', 'No', 'Blue', 'Real', 'Active', '2020-10-27 17:42:10'),
(147, 83, 79186, 59, 1, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-10-27 17:42:12'),
(148, 83, 76162, 59, 1, 'aishu1', 'No', 'Blue', 'Real', 'Active', '2020-10-27 17:43:04'),
(149, 84, 76581, 60, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-10-27 20:45:12'),
(150, 84, 73109, 60, 1, 'smstest', 'No', 'Blue', 'Real', 'Active', '2020-10-27 20:45:16'),
(151, 85, 79186, 61, 1, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-10-27 21:22:04'),
(152, 85, 73300, 61, 1, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-10-27 21:22:08'),
(153, 86, 73109, 61, 1, 'smstest', 'No', 'Blue', 'Real', 'Active', '2020-10-27 21:22:09'),
(154, 86, 76162, 61, 1, 'aishu1', 'No', 'Blue', 'Real', 'Active', '2020-10-27 21:22:30'),
(155, 87, 76092, 62, 1, 'SK sanjay', 'No', 'Blue', 'Real', 'Active', '2020-10-28 15:16:05'),
(156, 87, 79186, 62, 1, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-10-28 15:16:06'),
(157, 88, 76162, 62, 1, 'aishu1', 'No', 'Blue', 'Real', 'Active', '2020-10-28 15:16:10'),
(158, 88, 73109, 62, 1, 'smstest', 'No', 'Blue', 'Real', 'Active', '2020-10-28 15:16:34'),
(159, 89, 79186, 62, 2, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-10-28 16:34:11'),
(160, 89, 73109, 62, 2, 'smstest', 'No', 'Blue', 'Real', 'Active', '2020-10-28 16:34:11'),
(161, 90, 76092, 63, 1, 'SK sanjay', 'No', 'Blue', 'Real', 'Active', '2020-10-29 11:18:28'),
(162, 90, 73300, 63, 1, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-10-29 11:18:36'),
(163, 91, 79186, 63, 1, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-10-29 11:18:38'),
(164, 91, 76162, 63, 1, 'aishu1', 'No', 'Blue', 'Real', 'Active', '2020-10-29 11:18:52'),
(165, 92, 73109, 64, 1, 'smstest', 'No', 'Blue', 'Real', 'Active', '2020-11-02 12:06:28'),
(166, 92, 76581, 64, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-11-02 12:06:28'),
(167, 93, 73109, 65, 1, 'smstest', 'No', 'Blue', 'Real', 'Active', '2020-11-02 13:09:17'),
(168, 93, 76162, 65, 1, 'aishu1', 'No', 'Blue', 'Real', 'Active', '2020-11-02 13:09:26'),
(169, 94, 73300, 65, 1, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-11-02 13:09:43'),
(170, 94, 79186, 65, 1, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-11-02 13:09:44'),
(171, 95, 73104, 65, 1, 'Amol G', 'No', 'Blue', 'Real', 'Active', '2020-11-02 13:09:44'),
(172, 95, 76092, 65, 1, 'SK sanjay', 'No', 'Blue', 'Real', 'Active', '2020-11-02 13:10:00'),
(173, 96, 76162, 66, 1, 'aishu1', 'No', 'Blue', 'Real', 'Active', '2020-11-02 13:28:30'),
(174, 96, 73109, 66, 1, 'smstest', 'No', 'Blue', 'Real', 'Active', '2020-11-02 13:28:47'),
(175, 97, 79186, 66, 1, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-11-02 13:28:57'),
(176, 98, 73109, 67, 1, 'smstest', 'No', 'Blue', 'Real', 'Active', '2020-11-02 13:49:04'),
(177, 99, 76162, 68, 1, 'aishu1', 'No', 'Blue', 'Real', 'Active', '2020-11-02 14:13:30'),
(178, 99, 73300, 68, 1, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-11-02 14:13:46'),
(179, 99, 73109, 68, 1, 'smstest', 'No', 'Blue', 'Real', 'Active', '2020-11-02 14:14:08'),
(180, 100, 73104, 65, 2, 'Amol G', 'No', 'Blue', 'Real', 'Active', '2020-11-02 14:29:56'),
(181, 100, 73300, 65, 2, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-11-02 14:30:05'),
(182, 101, 79186, 69, 1, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-11-02 18:16:43'),
(183, 101, 76092, 69, 1, 'SK sanjay', 'No', 'Blue', 'Real', 'Active', '2020-11-02 18:16:46'),
(184, 102, 73300, 69, 1, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-11-02 18:16:51'),
(185, 102, 73104, 69, 1, 'Amol G', 'No', 'Blue', 'Real', 'Active', '2020-11-02 18:16:53'),
(186, 103, 76162, 69, 1, 'aishu1', 'No', 'Blue', 'Real', 'Active', '2020-11-02 18:17:19'),
(187, 104, 76092, 70, 1, 'SK sanjay', 'No', 'Blue', 'Real', 'Active', '2020-11-03 13:05:43'),
(188, 104, 79186, 70, 1, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-11-03 13:05:44'),
(189, 105, 73109, 70, 1, 'smstest', 'No', 'Blue', 'Real', 'Active', '2020-11-03 13:06:53'),
(190, 106, 79186, 71, 1, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-11-03 13:34:37'),
(191, 106, 73109, 71, 1, 'smstest', 'No', 'Blue', 'Real', 'Active', '2020-11-03 13:34:46'),
(192, 107, 76581, 71, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-11-03 13:34:54'),
(193, 107, 73300, 71, 1, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-11-03 13:34:55'),
(194, 108, 79186, 72, 1, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-11-03 14:36:00'),
(195, 108, 79186, 72, 1, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-11-03 14:37:09'),
(196, 109, 73109, 72, 1, 'smstest', 'No', 'Blue', 'Real', 'Active', '2020-11-03 14:37:17'),
(197, 109, 76581, 72, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-11-03 14:37:50'),
(198, 110, 73109, 73, 1, 'smstest', 'No', 'Blue', 'Real', 'Active', '2020-11-03 14:58:35'),
(199, 110, 79186, 73, 1, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-11-03 14:58:38'),
(200, 111, 76581, 73, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-11-03 14:58:47'),
(201, 112, 76162, 74, 1, 'aishu1', 'No', 'Blue', 'Real', 'Active', '2020-11-03 19:05:13'),
(202, 112, 76092, 74, 1, 'SK sanjay', 'No', 'Blue', 'Real', 'Active', '2020-11-03 19:05:14'),
(203, 113, 73300, 74, 1, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-11-03 19:05:30'),
(204, 113, 79186, 74, 1, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-11-03 19:05:41'),
(205, 114, 73300, 74, 2, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-11-03 20:21:17'),
(206, 114, 76092, 74, 2, 'SK sanjay', 'No', 'Blue', 'Real', 'Active', '2020-11-03 20:21:19'),
(207, 115, 76092, 75, 1, 'SK sanjay', 'No', 'Blue', 'Real', 'Active', '2020-11-04 13:45:39'),
(208, 115, 76162, 75, 1, 'aishu1', 'No', 'Blue', 'Real', 'Active', '2020-11-04 13:45:41'),
(209, 116, 73300, 75, 1, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-11-04 13:45:46'),
(210, 116, 79186, 75, 1, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-11-04 13:46:32'),
(211, 117, 76162, 76, 1, 'aishu1', 'No', 'Blue', 'Real', 'Active', '2020-11-04 14:14:02'),
(212, 117, 76092, 76, 1, 'SK sanjay', 'No', 'Blue', 'Real', 'Active', '2020-11-04 14:14:04'),
(213, 118, 73300, 76, 1, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-11-04 14:14:08'),
(214, 118, 79186, 76, 1, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-11-04 14:14:08'),
(215, 119, 73300, 76, 2, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-11-04 15:30:21'),
(216, 119, 76092, 76, 2, 'SK sanjay', 'No', 'Blue', 'Real', 'Active', '2020-11-04 15:30:23'),
(217, 120, 100751, 77, 1, 'Harshal', 'No', 'Blue', 'Real', 'Active', '2020-11-04 16:57:22'),
(218, 120, 100800, 77, 1, 'Sohail Mujawar', 'No', 'Blue', 'Real', 'Active', '2020-11-04 16:57:23'),
(219, 121, 157, 77, 1, 'Ramkrushna Kadam', 'No', 'Blue', 'Real', 'Active', '2020-11-04 16:57:24'),
(220, 121, 70961, 77, 1, 'ji', 'No', 'Blue', 'Real', 'Active', '2020-11-04 16:57:27'),
(221, 122, 76092, 77, 1, 'SK sanjay', 'No', 'Blue', 'Real', 'Active', '2020-11-04 16:57:29'),
(222, 122, 100758, 77, 1, 'Mayuri', 'No', 'Blue', 'Real', 'Active', '2020-11-04 16:57:31'),
(223, 123, 73300, 77, 1, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-11-04 16:57:34'),
(224, 123, 79186, 77, 1, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-11-04 16:57:34'),
(225, 124, 100750, 77, 1, 'piyush bhandari', 'No', 'Blue', 'Real', 'Active', '2020-11-04 16:57:35'),
(226, 124, 100763, 77, 1, 'shraddha Jadhav', 'No', 'Blue', 'Real', 'Active', '2020-11-04 16:57:39'),
(227, 125, 76162, 77, 1, 'aishu1', 'No', 'Blue', 'Real', 'Active', '2020-11-04 16:57:40'),
(228, 125, 100801, 77, 1, 'Swapnali Dabhade', 'No', 'Blue', 'Real', 'Active', '2020-11-04 16:57:44'),
(229, 126, 100752, 77, 1, 'shubham jagtap', 'No', 'Blue', 'Real', 'Active', '2020-11-04 16:58:21'),
(230, 126, 100788, 77, 1, 'Mamta', 'No', 'Blue', 'Real', 'Active', '2020-11-04 16:59:18'),
(231, 127, 81635, 77, 1, 'Anjali G', 'No', 'Blue', 'Real', 'Active', '2020-11-04 17:00:10'),
(232, 127, 70961, 77, 2, 'ji', 'No', 'Blue', 'Real', 'Active', '2020-11-04 18:22:26'),
(233, 128, 100752, 77, 2, 'shubham jagtap', 'No', 'Blue', 'Real', 'Active', '2020-11-04 18:22:28'),
(234, 128, 100800, 77, 2, 'Sohail Mujawar', 'No', 'Blue', 'Real', 'Active', '2020-11-04 18:22:28'),
(235, 129, 73300, 77, 2, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-11-04 18:22:29'),
(236, 129, 76162, 77, 2, 'aishu1', 'No', 'Blue', 'Real', 'Active', '2020-11-04 18:22:29'),
(237, 130, 100750, 77, 2, 'piyush bhandari', 'No', 'Blue', 'Real', 'Active', '2020-11-04 18:22:34'),
(238, 130, 81635, 77, 2, 'Anjali G', 'No', 'Blue', 'Real', 'Active', '2020-11-04 18:22:34'),
(239, 131, 100758, 77, 2, 'Mayuri', 'No', 'Blue', 'Real', 'Active', '2020-11-04 18:22:36'),
(240, 131, 100752, 77, 3, 'shubham jagtap', 'No', 'Blue', 'Real', 'Active', '2020-11-04 19:27:28'),
(241, 131, 76162, 77, 3, 'aishu1', 'No', 'Blue', 'Real', 'Active', '2020-11-04 19:27:37'),
(242, 132, 100750, 77, 3, 'piyush bhandari', 'No', 'Blue', 'Real', 'Active', '2020-11-04 19:27:38'),
(243, 132, 100750, 77, 4, 'piyush bhandari', 'No', 'Blue', 'Real', 'Active', '2020-11-04 20:32:28'),
(244, 133, 76162, 77, 4, 'aishu1', 'No', 'Blue', 'Real', 'Active', '2020-11-04 20:32:49'),
(245, 133, 100750, 77, 4, 'piyush bhandari', 'No', 'Blue', 'Real', 'Active', '2020-11-04 20:35:07'),
(246, 134, 73300, 78, 1, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-11-06 12:02:29'),
(247, 134, 76162, 78, 1, 'aishu1', 'No', 'Blue', 'Real', 'Active', '2020-11-06 12:02:30'),
(248, 135, 76092, 78, 1, 'SK sanjay', 'No', 'Blue', 'Real', 'Active', '2020-11-06 12:02:36'),
(249, 135, 79186, 78, 1, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-11-06 12:02:38'),
(250, 136, 76092, 78, 2, 'SK sanjay', 'No', 'Blue', 'Real', 'Active', '2020-11-06 13:13:56'),
(251, 136, 73300, 78, 2, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-11-06 13:13:59'),
(252, 137, 76162, 79, 1, 'aishu1', 'No', 'Blue', 'Real', 'Active', '2020-11-06 15:34:34'),
(253, 137, 76092, 79, 1, 'SK sanjay', 'No', 'Blue', 'Real', 'Active', '2020-11-06 15:34:37'),
(254, 138, 79186, 79, 1, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-11-06 15:34:49'),
(255, 138, 73300, 79, 1, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-11-06 15:34:58'),
(256, 139, 79186, 79, 2, 'aishu', 'No', 'Blue', 'Real', 'Active', '2020-11-06 16:50:18'),
(257, 139, 76092, 79, 2, 'SK sanjay', 'No', 'Blue', 'Real', 'Active', '2020-11-06 16:50:31'),
(258, 140, 73109, 80, 1, 'smstest', 'No', 'Blue', 'Real', 'Active', '2020-11-10 15:57:02'),
(259, 140, 76581, 80, 1, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-11-10 15:57:22'),
(260, 141, 22325, 80, 1, 'vishwa', 'No', 'Blue', 'Real', 'Active', '2020-11-10 15:57:34'),
(261, 141, 22325, 80, 2, 'vishwa', 'No', 'Blue', 'Real', 'Active', '2020-11-10 17:08:59'),
(262, 142, 76581, 80, 2, 'testplayer', 'No', 'Blue', 'Real', 'Active', '2020-11-10 17:09:01'),
(263, 143, 123732, 81, 1, 'varun', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:18:02'),
(264, 143, 123729, 81, 1, 'raj gaikwad', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:18:02'),
(265, 144, 100750, 81, 1, 'piyush bhandari', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:18:02'),
(266, 144, 123709, 81, 1, 'pragati', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:18:02'),
(267, 145, 70961, 81, 1, 'ji', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:18:03'),
(268, 145, 73300, 81, 1, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:18:04'),
(269, 146, 100758, 81, 1, 'Mayuri', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:18:04'),
(270, 146, 123727, 81, 1, 'rubina', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:18:05'),
(271, 147, 73104, 81, 1, 'Amol G', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:18:07'),
(272, 147, 76162, 81, 1, 'aishu1', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:18:09'),
(273, 148, 100751, 81, 1, 'Harshal', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:18:12'),
(274, 148, 100797, 81, 1, 'Shabista', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:18:25'),
(275, 149, 76092, 81, 1, 'SK sanjay', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:18:38'),
(276, 149, 123725, 81, 1, 'shraddha ', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:18:56'),
(277, 150, 70961, 82, 1, 'ji', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:55:02'),
(278, 150, 123731, 82, 1, 'Rushi sathe ', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:55:02'),
(279, 151, 100750, 82, 1, 'piyush bhandari', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:55:02'),
(280, 151, 123709, 82, 1, 'pragati', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:55:03'),
(281, 152, 100758, 82, 1, 'Mayuri', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:55:04'),
(282, 152, 123732, 82, 1, 'varun', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:55:05'),
(283, 153, 100751, 82, 1, 'Harshal', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:55:05'),
(284, 153, 123728, 82, 1, 'jay', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:55:05'),
(285, 154, 73300, 82, 1, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:55:05'),
(286, 154, 123694, 82, 1, 'ankita', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:55:06'),
(287, 155, 76162, 82, 1, 'aishu1', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:55:06'),
(288, 155, 73104, 82, 1, 'Amol G', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:55:07'),
(289, 156, 100800, 82, 1, 'Sohail Mujawar', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:55:08'),
(290, 156, 76092, 82, 1, 'SK sanjay', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:55:09'),
(291, 157, 123725, 82, 1, 'shraddha ', 'No', 'Blue', 'Real', 'Active', '2020-11-26 16:55:13'),
(292, 158, 123709, 81, 2, 'pragati', 'No', 'Blue', 'Real', 'Active', '2020-11-26 17:28:09'),
(293, 158, 123727, 81, 2, 'rubina', 'No', 'Blue', 'Real', 'Active', '2020-11-26 17:28:09'),
(294, 159, 123729, 81, 2, 'raj gaikwad', 'No', 'Blue', 'Real', 'Active', '2020-11-26 17:28:10'),
(295, 159, 76162, 81, 2, 'aishu1', 'No', 'Blue', 'Real', 'Active', '2020-11-26 17:28:11'),
(296, 160, 73300, 81, 2, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-11-26 17:28:14'),
(297, 160, 100751, 81, 2, 'Harshal', 'No', 'Blue', 'Real', 'Active', '2020-11-26 17:28:48'),
(298, 161, 76092, 81, 2, 'SK sanjay', 'No', 'Blue', 'Real', 'Active', '2020-11-26 17:31:11'),
(299, 161, 123709, 81, 3, 'pragati', 'No', 'Blue', 'Real', 'Active', '2020-11-26 18:33:11'),
(300, 162, 76092, 81, 3, 'SK sanjay', 'No', 'Blue', 'Real', 'Active', '2020-11-26 18:33:12'),
(301, 163, 100758, 83, 1, 'Mayuri', 'No', 'Blue', 'Real', 'Active', '2020-11-27 11:43:02'),
(302, 163, 157, 83, 1, 'R.K', 'No', 'Blue', 'Real', 'Active', '2020-11-27 11:43:05'),
(303, 164, 123728, 83, 1, 'jay', 'No', 'Blue', 'Real', 'Active', '2020-11-27 11:43:06'),
(304, 164, 70961, 83, 1, 'ji', 'No', 'Blue', 'Real', 'Active', '2020-11-27 11:43:06'),
(305, 165, 76162, 83, 1, 'aishu1', 'No', 'Blue', 'Real', 'Active', '2020-11-27 11:43:06'),
(306, 165, 100752, 83, 1, 'shubham jagtap', 'No', 'Blue', 'Real', 'Active', '2020-11-27 11:43:08'),
(307, 166, 81638, 83, 1, 'Osomose', 'No', 'Blue', 'Real', 'Active', '2020-11-27 11:43:09'),
(308, 166, 123727, 83, 1, 'rubina', 'No', 'Blue', 'Real', 'Active', '2020-11-27 11:43:09'),
(309, 167, 100763, 83, 1, 'shraddha Jadhav', 'No', 'Blue', 'Real', 'Active', '2020-11-27 11:43:10'),
(310, 167, 100800, 83, 1, 'Sohail Mujawar', 'No', 'Blue', 'Real', 'Active', '2020-11-27 11:43:10'),
(311, 168, 100750, 83, 1, 'piyush bhandari', 'No', 'Blue', 'Real', 'Active', '2020-11-27 11:43:10'),
(312, 168, 100751, 83, 1, 'Harshal', 'No', 'Blue', 'Real', 'Active', '2020-11-27 11:43:14'),
(313, 169, 123694, 83, 1, 'ankita', 'No', 'Blue', 'Real', 'Active', '2020-11-27 11:43:14'),
(314, 169, 123902, 83, 1, 'snehal', 'No', 'Blue', 'Real', 'Active', '2020-11-27 11:43:18'),
(315, 170, 81635, 83, 1, 'Anjali G', 'No', 'Blue', 'Real', 'Active', '2020-11-27 11:43:19'),
(316, 170, 73300, 83, 1, 'testing', 'No', 'Blue', 'Real', 'Active', '2020-11-27 11:43:19'),
(317, 171, 81638, 83, 2, 'Osomose', 'No', 'Blue', 'Real', 'Active', '2020-11-27 12:59:08'),
(318, 171, 100800, 83, 2, 'Sohail Mujawar', 'No', 'Blue', 'Real', 'Active', '2020-11-27 12:59:09'),
(319, 172, 81638, 83, 3, 'Osomose', 'No', 'Blue', 'Real', 'Active', '2020-11-27 14:04:11');

-- --------------------------------------------------------

--
-- Table structure for table `ludo_mst_rooms`
--

DROP TABLE IF EXISTS `ludo_mst_rooms`;
CREATE TABLE `ludo_mst_rooms` (
  `roomId` int(11) NOT NULL,
  `roomTitle` varchar(100) NOT NULL,
  `commision` double NOT NULL,
  `entryFee` varchar(200) NOT NULL,
  `players` varchar(100) NOT NULL,
  `mode` enum('Quick','Classic') NOT NULL DEFAULT 'Quick',
  `startRoundTime` int(11) NOT NULL,
  `tokenMoveTime` int(11) NOT NULL,
  `rollDiceTime` int(11) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `isPrivate` enum('Yes','No') NOT NULL DEFAULT 'No',
  `currentRoundBot` int(11) NOT NULL,
  `totalRoundBot` int(11) NOT NULL,
  `isBotConnect` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `reserveAmount` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ludo_mst_rooms`
--

INSERT INTO `ludo_mst_rooms` (`roomId`, `roomTitle`, `commision`, `entryFee`, `players`, `mode`, `startRoundTime`, `tokenMoveTime`, `rollDiceTime`, `status`, `isPrivate`, `currentRoundBot`, `totalRoundBot`, `isBotConnect`, `reserveAmount`, `created`, `modified`) VALUES
(17, '15', 15, '15', '2', 'Classic', 60, 30, 30, 'Inactive', 'No', 3, 135, 'No', 0, '2020-02-18 16:24:47', '2020-10-18 18:29:23'),
(35, 'Play with friends', 5, '10-1000', '4', 'Classic', 60, 30, 30, 'Inactive', 'Yes', 0, 0, '', 0, '2020-02-18 16:41:56', '2021-03-12 09:02:42'),
(38, '2 players - 100 Rs', 15, '100', '2', 'Classic', 60, 30, 30, 'Active', 'No', 3, 3, 'No', 0, '2020-07-13 18:45:59', '2021-03-15 09:09:39'),
(42, '2 players - 30rs', 15, '30', '2', 'Classic', 60, 30, 30, 'Active', 'No', 0, 0, 'No', 0, '2020-07-30 10:45:16', '2021-03-13 09:37:33'),
(45, '4 player -  30 Rs', 15, '30', '4', 'Classic', 60, 30, 30, 'Active', 'No', 0, 0, 'No', 0, '2020-08-13 23:22:09', '2021-03-15 09:09:37'),
(47, '4 player - 15 Rs', 15, '15', '4', 'Classic', 60, 30, 30, 'Inactive', 'No', 0, 0, 'No', 0, '2020-08-26 11:32:58', '2021-03-13 09:33:54'),
(48, '4 players - 50 rs', 15, '50', '4', 'Classic', 60, 30, 30, 'Inactive', 'No', 0, 0, 'No', 0, '2020-08-26 11:33:45', '2021-03-15 09:09:47'),
(49, '2 player - 200 Rs', 15, '200', '2', 'Classic', 60, 30, 30, 'Inactive', 'No', 0, 0, 'No', 0, '2020-08-26 11:38:48', '2021-03-15 09:09:45'),
(50, '2 player - 250 Rs', 15, '250', '2', 'Classic', 60, 30, 30, 'Inactive', 'No', 0, 0, 'No', 0, '2020-10-24 18:05:18', '2021-03-14 09:03:06');

-- --------------------------------------------------------

--
-- Table structure for table `ludo_winners`
--

DROP TABLE IF EXISTS `ludo_winners`;
CREATE TABLE `ludo_winners` (
  `winnerId` int(11) NOT NULL,
  `joinRoomId` double NOT NULL,
  `userId` double NOT NULL,
  `adminPercent` int(11) NOT NULL,
  `totalWinningPrice` double NOT NULL,
  `winningPrice` double NOT NULL,
  `adminAmount` double NOT NULL,
  `gameMode` enum('Quick','Classic') NOT NULL,
  `isPrivate` enum('Yes','No') NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `main_environment`
--

DROP TABLE IF EXISTS `main_environment`;
CREATE TABLE `main_environment` (
  `mainEnvironmentId` int(11) NOT NULL,
  `envKey` varchar(30) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_environment`
--

INSERT INTO `main_environment` (`mainEnvironmentId`, `envKey`, `value`) VALUES
(1, 'LUDOFANTASY', 'bqwdyq8773nas98r398mad234fusdf89r2');

-- --------------------------------------------------------

--
-- Table structure for table `mst_bonus`
--

DROP TABLE IF EXISTS `mst_bonus`;
CREATE TABLE `mst_bonus` (
  `bonusId` int(11) NOT NULL,
  `playGame` double NOT NULL,
  `bonus` double NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_bonus`
--

INSERT INTO `mst_bonus` (`bonusId`, `playGame`, `bonus`, `status`, `created`, `modified`) VALUES
(1, 1, 1, 'Inactive', '2020-04-27 18:49:10', '2020-04-27 18:49:15');

-- --------------------------------------------------------

--
-- Table structure for table `mst_settings`
--

DROP TABLE IF EXISTS `mst_settings`;
CREATE TABLE `mst_settings` (
  `id` int(11) NOT NULL,
  `site_title` varchar(255) NOT NULL,
  `companyName` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email1` varchar(255) NOT NULL,
  `email2` varchar(255) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `apk` varchar(225) NOT NULL,
  `version` varchar(225) NOT NULL,
  `website` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `copyright` varchar(255) NOT NULL,
  `contact_us_desc` varchar(255) NOT NULL,
  `adminPercent` int(11) NOT NULL,
  `videoUrl` varchar(255) NOT NULL,
  `topPlayerLimit` int(11) NOT NULL,
  `referralBonus` double NOT NULL,
  `signupBonus` double NOT NULL,
  `withdrawAmtMinus` double NOT NULL,
  `cashBonus` double NOT NULL,
  `baseUrl` varchar(255) NOT NULL,
  `maintainance` enum('Yes','No') NOT NULL DEFAULT 'No',
  `maintainanceMsg` varchar(255) NOT NULL,
  `joinRoomName` varchar(255) NOT NULL,
  `systemPassword` varchar(255) NOT NULL,
  `cdh` varchar(255) NOT NULL,
  `remoteip` varchar(255) NOT NULL,
  `spinWheelTimer` varchar(255) NOT NULL,
  `referalField1` double NOT NULL,
  `referalField2` double NOT NULL,
  `referalField3` double NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_settings`
--

INSERT INTO `mst_settings` (`id`, `site_title`, `companyName`, `address`, `email1`, `email2`, `phone`, `apk`, `version`, `website`, `logo`, `copyright`, `contact_us_desc`, `adminPercent`, `videoUrl`, `topPlayerLimit`, `referralBonus`, `signupBonus`, `withdrawAmtMinus`, `cashBonus`, `baseUrl`, `maintainance`, `maintainanceMsg`, `joinRoomName`, `systemPassword`, `cdh`, `remoteip`, `spinWheelTimer`, `referalField1`, `referalField2`, `referalField3`, `created`, `modified`) VALUES
(4, 'Ludo', 'Company', 'India', 'info@company.com', 'company@gmail.com', 9999999999, 'test.apk', '0.9', 'https://www.company.com', '', 'Copyright © Ludo 2020', 'contact us 24x7. we are here to help ', 3, '', 30, 0, 25, 0, 0, 'http://company.com/admin/', 'No', 'Game is Live', 'JOINGAME!@#', 'SKILL!@#$%', 'CB!@#$!@', 'remoteip', '120', 50, 50, 50, '2019-09-26 15:32:25', '2021-01-27 17:15:12');

-- --------------------------------------------------------

--
-- Table structure for table `mst_sms_body`
--

DROP TABLE IF EXISTS `mst_sms_body`;
CREATE TABLE `mst_sms_body` (
  `smsId` int(11) NOT NULL,
  `smsType` varchar(255) NOT NULL,
  `smsBody` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_sms_body`
--

INSERT INTO `mst_sms_body` (`smsId`, `smsType`, `smsBody`, `created`, `modified`) VALUES
(1, 'Otp-verification', '{otp} is your OTP (One Time Password) to verify your user account on Ludo', '0000-00-00 00:00:00', '2019-10-31 07:27:23'),
(2, 'Forgot_password', 'Hello {user_name}, Your new password is {password}', '2019-11-01 15:22:39', '2019-11-01 15:22:39'),
(3, 'kyc_type_message', 'Dear {user_name},Your {kyc_type} {message}', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'redeem_rejection_sms', 'Hello {user_name}, Your withdraw request is rejected reason is {message} ', '2019-12-17 11:14:11', '2019-12-17 11:14:11'),
(5, 'refund-reedem-amount', 'Hello {user_name},Your withdraw amount {amt} is refunded due to {reason}', '2020-01-15 12:52:17', '2020-01-15 12:52:17'),
(6, 'contactus_reply', 'Dear {user_name},Your Reply {message}', '2019-12-13 18:42:32', '2019-12-13 18:42:32'),
(7, 'supports_reply', 'Dear {user_name},Your Reply {message}', '2019-12-13 18:42:32', '2019-12-13 18:42:32'),
(8, 'Email_Verification', 'Dear {user_name}, Your email is verified successfully', '2020-02-24 12:46:10', '2020-02-24 12:46:10'),
(9, 'Add_money', 'Dear {user_name}, ludo has added Rs. {amount} bonus in your wallet.', '2020-02-25 10:55:01', '2020-02-25 10:55:01'),
(11, 'withdraw_amt_msg_bank', 'Dear {user_name}, Your withdrawal request of Rs {amt} successfully credited to your bank by ludoGames pvt ltd  (Ludoskill).', '2020-01-15 12:52:17', '2020-01-15 12:52:17'),
(12, 'withdraw_amt_msg_paytm', 'Dear {user_name}, Your paytm wallet credited Rs {amt} by ludo Games pvt lt', '2020-01-15 12:52:17', '2020-01-15 12:52:17');

-- --------------------------------------------------------

--
-- Table structure for table `mst_tournaments`
--

DROP TABLE IF EXISTS `mst_tournaments`;
CREATE TABLE `mst_tournaments` (
  `tournamentId` int(11) NOT NULL,
  `tournamentTitle` varchar(255) NOT NULL,
  `tournamentDescription` varchar(255) NOT NULL,
  `startDate` varchar(50) NOT NULL,
  `startTime` time NOT NULL,
  `entryFee` double NOT NULL,
  `winningPrice` int(11) NOT NULL,
  `playerLimitInRoom` int(11) NOT NULL,
  `noOfRoundInTournament` int(11) NOT NULL,
  `playerLimitInTournament` int(11) NOT NULL,
  `commision` int(11) NOT NULL,
  `currentRound` int(11) NOT NULL DEFAULT 1,
  `lastRound` int(11) NOT NULL,
  `lastRoundPlayerLeft` int(11) NOT NULL,
  `registerPlayerCount` int(11) NOT NULL,
  `startRoundTime` int(11) NOT NULL,
  `tokenMoveTime` int(11) NOT NULL,
  `rollDiceTime` int(11) NOT NULL,
  `status` enum('Active','Inactive','Next','Complete') NOT NULL DEFAULT 'Inactive',
  `gameMode` enum('Quick','Classic') NOT NULL DEFAULT 'Quick',
  `roundTimer` enum('Start','End') NOT NULL DEFAULT 'Start',
  `isUpdateWinPrice` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_tournaments`
--

INSERT INTO `mst_tournaments` (`tournamentId`, `tournamentTitle`, `tournamentDescription`, `startDate`, `startTime`, `entryFee`, `winningPrice`, `playerLimitInRoom`, `noOfRoundInTournament`, `playerLimitInTournament`, `commision`, `currentRound`, `lastRound`, `lastRoundPlayerLeft`, `registerPlayerCount`, `startRoundTime`, `tokenMoveTime`, `rollDiceTime`, `status`, `gameMode`, `roundTimer`, `isUpdateWinPrice`, `created`, `modified`) VALUES
(43, 'Test10', 'Teta', '2020-06-16', '19:41:00', 10, 36, 2, 5, 6, 10, 2, 0, 0, 4, 60, 30, 30, 'Next', 'Classic', 'Start', 'Yes', '2020-06-16 18:35:30', '2020-10-14 19:33:05'),
(50, 'Test 2', 'Testing tournament.', '2020-11-10', '15:47:00', 10, 26, 2, 3, 8, 15, 2, 0, 0, 3, 60, 30, 30, 'Next', 'Classic', 'Start', 'Yes', '2020-10-22 16:25:33', '2020-11-10 15:40:46'),
(59, 'testing2', 'Test2', '2020-10-27', '18:52:02', 5, 17, 2, 2, 8, 15, 2, 0, 0, 4, 60, 30, 30, 'Next', 'Classic', 'Start', 'Yes', '2020-10-27 17:34:29', '2020-10-27 17:52:02'),
(61, 'Tour27', 'Test', '2020-10-27', '22:36:32', 10, 36, 2, 4, 6, 10, 2, 0, 0, 4, 60, 30, 30, 'Next', 'Classic', 'Start', 'Yes', '2020-10-27 21:03:44', '2020-10-27 21:36:32'),
(63, 'tournamnet1', 'Hello', '2020-10-29', '12:26:17', 5, 17, 2, 2, 6, 15, 2, 0, 0, 4, 60, 30, 30, 'Next', 'Classic', 'Start', 'Yes', '2020-10-29 11:04:58', '2020-10-29 11:26:17'),
(65, 'tournament 2', 'Test', '2020-11-02', '15:33:36', 10, 51, 2, 3, 6, 15, 3, 0, 0, 6, 60, 30, 30, 'Next', 'Classic', 'Start', 'Yes', '2020-11-02 12:39:26', '2020-11-02 14:33:36'),
(69, 'Tournament 5', 'Testing', '2020-11-02', '19:25:24', 5, 21, 2, 2, 5, 15, 2, 0, 0, 5, 60, 30, 30, 'Next', 'Classic', 'Start', 'Yes', '2020-11-02 18:11:47', '2020-11-02 18:25:24'),
(70, 'Tournament 6', 'Test', '2020-11-03', '14:10:05', 5, 13, 2, 2, 3, 15, 2, 0, 0, 3, 60, 30, 30, 'Next', 'Classic', 'Start', 'Yes', '2020-11-03 13:02:38', '2020-11-03 13:10:05'),
(71, 'Tournament 7', 'Test', '2020-11-03', '14:39:04', 10, 34, 2, 2, 4, 15, 2, 0, 0, 4, 60, 30, 30, 'Next', 'Classic', 'Start', 'Yes', '2020-11-03 13:31:42', '2020-11-03 13:39:04'),
(73, 'Tournament 8', 'Test', '2020-11-03', '16:10:53', 5, 17, 2, 2, 4, 15, 2, 0, 0, 4, 60, 30, 30, 'Next', 'Classic', 'Start', 'Yes', '2020-11-03 14:41:26', '2020-11-03 15:10:53'),
(75, 'Tournament 10', 'Testing', '2020-11-04', '14:50:01', 5, 17, 2, 3, 5, 15, 2, 0, 0, 4, 60, 60, 30, 'Next', 'Classic', 'Start', 'Yes', '2020-11-04 13:05:30', '2020-11-04 13:50:01'),
(80, 'Test price', 'Test', '2020-11-10', '17:08:49', 10, 27, 2, 3, 4, 10, 2, 0, 0, 3, 60, 30, 30, 'Complete', 'Classic', 'End', 'Yes', '2020-11-10 15:41:59', '2020-11-10 17:13:52'),
(81, 'Tournament 11', 'Testing', '2020-11-26', '19:38:11', 5, 68, 2, 4, 20, 15, 4, 0, 0, 16, 30, 60, 30, 'Next', 'Quick', 'Start', 'Yes', '2020-11-26 15:21:38', '2020-11-26 18:38:11'),
(82, 'Tournament 12', 'Testing', '2020-11-26', '16:55:00', 5, 68, 2, 4, 16, 15, 1, 0, 0, 16, 30, 60, 30, 'Active', 'Classic', 'End', 'Yes', '2020-11-26 16:34:26', '2020-11-26 16:54:20'),
(83, 'Tournament 13', 'Testing', '2020-11-27', '14:04:09', 5, 68, 2, 4, 16, 15, 3, 0, 0, 16, 30, 60, 30, 'Complete', 'Classic', 'End', 'Yes', '2020-11-27 10:50:10', '2020-11-27 14:09:10');

-- --------------------------------------------------------

--
-- Table structure for table `mst_tournaments_old`
--

DROP TABLE IF EXISTS `mst_tournaments_old`;
CREATE TABLE `mst_tournaments_old` (
  `tournamentId` int(11) NOT NULL,
  `tournamentName` varchar(200) NOT NULL,
  `tournamentDate` date NOT NULL,
  `tournamentTime` time NOT NULL,
  `noOfPlayers` int(11) NOT NULL,
  `totalPlayers` int(11) NOT NULL,
  `entryFee` double NOT NULL,
  `winnerPriceRank1` double NOT NULL,
  `winnerPriceRank2` double NOT NULL,
  `winnerPriceRank3` double NOT NULL,
  `nextRoundMinute` int(11) NOT NULL,
  `adminPercent` int(11) NOT NULL,
  `tournamentStatus` enum('Active','Inactive') NOT NULL DEFAULT 'Inactive',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mst_tournament_logs`
--

DROP TABLE IF EXISTS `mst_tournament_logs`;
CREATE TABLE `mst_tournament_logs` (
  `tournamenLogtId` int(11) NOT NULL,
  `tournamentId` int(11) NOT NULL,
  `tournamentTitle` varchar(150) NOT NULL,
  `startDate` varchar(155) NOT NULL,
  `startTime` varchar(155) NOT NULL,
  `playerLimitInRoom` int(11) NOT NULL,
  `playerLimitInTournament` int(11) NOT NULL,
  `registerPlayerCount` int(11) NOT NULL,
  `currentRound` int(11) NOT NULL DEFAULT 1,
  `status` enum('Registration','Join','StartMatch','End','NextRound') NOT NULL DEFAULT 'Join',
  `gameMode` varchar(155) NOT NULL,
  `playerInGameCount` int(11) NOT NULL,
  `winPlayerCount` int(11) NOT NULL,
  `totalRoomInCurrentRound` int(11) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_tournament_logs`
--

INSERT INTO `mst_tournament_logs` (`tournamenLogtId`, `tournamentId`, `tournamentTitle`, `startDate`, `startTime`, `playerLimitInRoom`, `playerLimitInTournament`, `registerPlayerCount`, `currentRound`, `status`, `gameMode`, `playerInGameCount`, `winPlayerCount`, `totalRoomInCurrentRound`, `created`) VALUES
(1, 5, 'Checking', '2020-06-05', '16:45:00', 2, 0, 0, 1, 'End', 'Classic', 1, 0, 0, '2020-06-05 16:50:03'),
(2, 13, 'Three', '2020-06-09', '20:05:00', 2, 0, 0, 1, 'End', 'Classic', 2, 0, 0, '2020-06-09 20:10:18'),
(3, 14, 'Four', '2020-06-09', '21:19:03', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-06-09 20:19:03'),
(4, 14, 'Four', '2020-06-09', '20:20:00', 2, 0, 0, 2, 'End', 'Classic', 2, 0, 0, '2020-06-09 20:25:02'),
(5, 15, 'Five', '2020-06-10', '10:20:00', 2, 0, 0, 1, 'End', 'Classic', 2, 0, 0, '2020-06-10 10:32:40'),
(6, 17, 'Seven', '2020-06-10', '12:04:00', 2, 0, 0, 1, 'End', 'Classic', 2, 0, 0, '2020-06-10 12:09:04'),
(7, 22, 'Eight', '2020-06-10', '18:26:00', 2, 0, 0, 1, 'End', 'Classic', 2, 0, 0, '2020-06-10 18:31:03'),
(8, 24, 'Tested', '2020-06-10', '19:19:00', 2, 0, 0, 1, 'End', 'Classic', 2, 0, 0, '2020-06-10 19:24:03'),
(9, 25, 'Testing', '2020-06-10', '19:29:00', 2, 0, 0, 1, 'End', 'Classic', 2, 0, 0, '2020-06-10 19:34:02'),
(10, 27, 'SUNDAY bonanza', '2020-06-14', '17:15:24', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-06-14 16:15:24'),
(11, 29, 'test', '2020-06-15', '14:46:37', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-06-15 13:46:37'),
(12, 33, 'Test12', '2020-06-16', '14:17:01', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-06-16 13:17:01'),
(13, 33, 'Test12', '2020-06-16', '13:17:00', 2, 0, 0, 2, 'End', 'Classic', 2, 0, 0, '2020-06-16 13:22:02'),
(14, 34, 'Test1', '2020-06-16', '16:14:04', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-06-16 15:14:04'),
(15, 35, 'Test2', '2020-06-16', '16:36:01', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-06-16 15:36:01'),
(16, 36, 'Test3', '2020-06-16', '16:49:00', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-06-16 15:49:00'),
(17, 36, 'Test3', '2020-06-16', '15:51:00', 2, 0, 0, 2, 'End', 'Classic', 2, 0, 0, '2020-06-16 15:56:01'),
(18, 43, 'Test10', '2020-06-16', '19:41:05', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-06-16 18:41:05'),
(19, 45, 'Test21', '2020-06-17', '13:56:06', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-06-17 12:56:06'),
(20, 45, 'Test21', '2020-06-17', '12:56:00', 2, 0, 0, 2, 'End', 'Classic', 2, 0, 0, '2020-06-17 13:01:04'),
(21, 46, 'testskill', '2020-06-17', '17:37:05', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-06-17 16:37:05'),
(22, 46, 'testskill', '2020-06-17', '16:37:00', 2, 0, 0, 2, 'End', 'Classic', 2, 0, 0, '2020-06-17 16:42:04'),
(23, 47, 'test111', '2020-07-03', '19:20:05', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-07-03 18:20:05'),
(24, 47, 'test111', '2020-07-03', '18:21:00', 2, 0, 0, 2, 'End', 'Classic', 2, 0, 0, '2020-07-03 18:26:06'),
(25, 50, 'Test 2', '2020-10-22', '19:20:02', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-10-22 18:20:02'),
(26, 52, 'Tour12', '2020-10-24', '13:26:00', 2, 0, 0, 1, 'End', 'Quick', 2, 0, 0, '2020-10-24 13:31:01'),
(27, 53, 'Tour13', '2020-10-24', '13:36:00', 2, 0, 0, 1, 'End', 'Classic', 2, 0, 0, '2020-10-24 13:41:02'),
(28, 54, 'Tour1', '2020-10-26', '16:07:00', 2, 0, 0, 1, 'End', 'Classic', 2, 0, 0, '2020-10-26 16:12:01'),
(29, 55, 'Tour1', '2020-10-26', '16:23:00', 2, 0, 0, 1, 'End', 'Classic', 2, 0, 0, '2020-10-26 16:28:02'),
(30, 56, 'Tour26', '2020-10-26', '17:50:01', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-10-26 16:50:01'),
(31, 56, 'Tour26', '2020-10-26', '16:53:00', 2, 0, 0, 2, 'End', 'Classic', 2, 0, 0, '2020-10-26 16:58:01'),
(32, 57, 'Tour27', '2020-10-27', '12:26:00', 2, 0, 0, 1, 'End', 'Classic', 2, 0, 0, '2020-10-27 12:31:01'),
(33, 59, 'testing2', '2020-10-27', '18:52:02', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-10-27 17:52:02'),
(34, 60, 'Tour278', '2020-10-27', '20:45:00', 2, 0, 0, 1, 'End', 'Classic', 2, 0, 0, '2020-10-27 20:50:22'),
(35, 61, 'Tour27', '2020-10-27', '22:36:32', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-10-27 21:36:32'),
(36, 62, 'Tour28', '2020-10-28', '16:31:27', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-10-28 15:31:27'),
(37, 62, 'Tour28', '2020-10-28', '16:31:27', 2, 0, 0, 2, 'End', 'Classic', 2, 0, 0, '2020-10-28 16:36:29'),
(38, 63, 'tournamnet1', '2020-10-29', '12:26:17', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-10-29 11:26:17'),
(39, 64, 'Tour2', '2020-11-02', '12:06:00', 2, 0, 0, 1, 'End', 'Classic', 2, 0, 0, '2020-11-02 12:17:38'),
(40, 65, 'tournament 2', '2020-11-02', '14:28:32', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-11-02 13:28:32'),
(41, 67, 'Tour4', '2020-11-02', '13:49:00', 2, 0, 0, 1, 'End', 'Classic', 1, 0, 0, '2020-11-02 13:54:09'),
(42, 68, 'Tournament 4', '2020-11-02', '14:12:00', 3, 0, 0, 1, 'End', 'Classic', 3, 0, 0, '2020-11-02 14:19:18'),
(43, 65, 'tournament 2', '2020-11-02', '15:33:36', 2, 0, 0, 3, 'NextRound', 'Classic', 0, 0, 0, '2020-11-02 14:33:36'),
(44, 69, 'Tournament 5', '2020-11-02', '19:25:24', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-11-02 18:25:24'),
(45, 70, 'Tournament 6', '2020-11-03', '14:10:05', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-11-03 13:10:05'),
(46, 71, 'Tournament 7', '2020-11-03', '14:39:04', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-11-03 13:39:04'),
(47, 73, 'Tournament 8', '2020-11-03', '16:10:53', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-11-03 15:10:53'),
(48, 74, 'Tournament 9', '2020-11-03', '20:21:15', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-11-03 19:21:15'),
(49, 74, 'Tournament 9', '2020-11-03', '20:21:15', 2, 0, 0, 2, 'End', 'Classic', 2, 0, 0, '2020-11-03 20:26:21'),
(50, 75, 'Tournament 10', '2020-11-04', '14:50:01', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-11-04 13:50:01'),
(51, 76, 'Tournament 11', '2020-11-04', '15:30:15', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-11-04 14:30:15'),
(52, 76, 'Tournament 11', '2020-11-04', '15:30:15', 2, 0, 0, 2, 'End', 'Classic', 2, 0, 0, '2020-11-04 15:35:16'),
(53, 77, 'Tournament 12', '2020-11-04', '18:22:24', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-11-04 17:22:24'),
(54, 77, 'Tournament 12', '2020-11-04', '19:27:25', 2, 0, 0, 3, 'NextRound', 'Classic', 0, 0, 0, '2020-11-04 18:27:25'),
(55, 77, 'Tournament 12', '2020-11-04', '20:32:27', 2, 0, 0, 4, 'NextRound', 'Classic', 0, 0, 0, '2020-11-04 19:32:27'),
(56, 77, 'Tournament 12', '2020-11-04', '20:32:27', 2, 0, 0, 4, 'End', 'Classic', 2, 0, 0, '2020-11-04 20:37:28'),
(57, 78, 'Tournament 13', '2020-11-06', '13:13:53', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-11-06 12:13:53'),
(58, 78, 'Tournament 13', '2020-11-06', '13:13:53', 2, 0, 0, 2, 'End', 'Classic', 2, 0, 0, '2020-11-06 13:18:54'),
(59, 79, 'Tournament 14', '2020-11-06', '16:49:51', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-11-06 15:49:51'),
(60, 79, 'Tournament 14', '2020-11-06', '16:49:51', 2, 0, 0, 2, 'End', 'Classic', 2, 0, 0, '2020-11-06 16:54:52'),
(61, 80, 'Test price', '2020-11-10', '17:08:49', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-11-10 16:08:49'),
(62, 80, 'Test price', '2020-11-10', '17:08:49', 2, 0, 0, 2, 'End', 'Classic', 2, 0, 0, '2020-11-10 17:13:52'),
(63, 81, 'Tournament 11', '2020-11-26', '17:28:07', 2, 0, 0, 2, 'NextRound', 'Quick', 0, 0, 0, '2020-11-26 16:28:07'),
(64, 81, 'Tournament 11', '2020-11-26', '18:33:09', 2, 0, 0, 3, 'NextRound', 'Quick', 0, 0, 0, '2020-11-26 17:33:09'),
(65, 81, 'Tournament 11', '2020-11-26', '19:38:11', 2, 0, 0, 4, 'NextRound', 'Quick', 0, 0, 0, '2020-11-26 18:38:11'),
(66, 83, 'Tournament 13', '2020-11-27', '12:59:07', 2, 0, 0, 2, 'NextRound', 'Classic', 0, 0, 0, '2020-11-27 11:59:07'),
(67, 83, 'Tournament 13', '2020-11-27', '14:04:09', 2, 0, 0, 3, 'NextRound', 'Classic', 0, 0, 0, '2020-11-27 13:04:09'),
(68, 83, 'Tournament 13', '2020-11-27', '14:04:09', 2, 0, 0, 3, 'End', 'Classic', 1, 0, 0, '2020-11-27 14:09:10');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `notification` text NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `customer_id` varchar(100) NOT NULL,
  `orderId` varchar(255) NOT NULL,
  `amount` float NOT NULL,
  `paymentMode` varchar(255) NOT NULL,
  `isPayment` enum('Yes','No') NOT NULL DEFAULT 'No',
  `transaction_id` varchar(255) NOT NULL,
  `json_data` text NOT NULL,
  `coupanCode` varchar(255) NOT NULL,
  `discount` varchar(255) NOT NULL COMMENT 'discount amount',
  `discountAmount` double NOT NULL COMMENT 'amount minus discount is discountAmount',
  `isCoupan` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `isamountgiven` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `user_detail_id` int(11) NOT NULL,
  `orderId` varchar(255) NOT NULL,
  `paymentType` varchar(255) NOT NULL,
  `txnMode` varchar(255) NOT NULL,
  `type` enum('Deposit','Withdraw','Gratification') NOT NULL,
  `amount` double NOT NULL,
  `balance` double NOT NULL,
  `mainWallet` double NOT NULL,
  `winWallet` double NOT NULL,
  `status` enum('Approved','Pending','Rejected','Success','Failed','Process','BankExport') NOT NULL DEFAULT 'Pending',
  `paytmStatus` varchar(255) NOT NULL,
  `json_data` text NOT NULL,
  `paymentMode` varchar(255) NOT NULL DEFAULT 'PPI' COMMENT 'PPI=>Wallet',
  `bankName` varchar(255) NOT NULL DEFAULT 'WALLET',
  `statusMessage` varchar(255) NOT NULL,
  `transactionId` varchar(255) NOT NULL COMMENT 'value will get from payment gateway response',
  `checkSum` varchar(255) NOT NULL,
  `rejectedReason` varchar(255) NOT NULL,
  `isReadNotification` enum('Yes','No') NOT NULL DEFAULT 'No',
  `isAdminReedem` enum('Yes','No') NOT NULL DEFAULT 'No',
  `statusCode` varchar(255) DEFAULT NULL,
  `coupanCode` varchar(150) DEFAULT NULL,
  `isCoupan` enum('Yes','No') NOT NULL DEFAULT 'No',
  `discount` double NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `mobileNo` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_logs`
--

DROP TABLE IF EXISTS `payment_logs`;
CREATE TABLE `payment_logs` (
  `id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `user_detail_id` int(11) NOT NULL,
  `userName` varchar(255) NOT NULL,
  `userEmail` varchar(255) NOT NULL,
  `userPhone` bigint(20) NOT NULL,
  `orderId` varchar(255) NOT NULL,
  `paymentType` varchar(255) NOT NULL,
  `txnMode` varchar(255) NOT NULL,
  `amount` double NOT NULL,
  `balance` double NOT NULL,
  `mainWallet` double NOT NULL,
  `winWallet` double NOT NULL,
  `checkSum` varchar(255) NOT NULL,
  `paytmType` varchar(255) NOT NULL,
  `type` enum('Deposit','Withdraw') NOT NULL,
  `paytmStatus` varchar(255) NOT NULL,
  `statusCode` varchar(255) NOT NULL,
  `statusMessage` varchar(255) NOT NULL,
  `status` enum('Approved','Pending','Rejected','Process','Failed','Success','BankExport') NOT NULL DEFAULT 'Pending',
  `transactionId` varchar(255) NOT NULL COMMENT 'value will get from payment gateway response',
  `json_data` text NOT NULL,
  `rejectedReason` varchar(255) NOT NULL,
  `paymentMode` varchar(255) NOT NULL DEFAULT 'PPI' COMMENT 'PPI=>Wallet',
  `paymentBy` enum('Bank','Paytm') NOT NULL COMMENT 'Paytm',
  `bankName` varchar(255) NOT NULL DEFAULT 'WALLET',
  `isPayment` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created` datetime NOT NULL,
  `mobileNo` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_process`
--

DROP TABLE IF EXISTS `payment_process`;
CREATE TABLE `payment_process` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `setInOrders` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paytm_refunds`
--

DROP TABLE IF EXISTS `paytm_refunds`;
CREATE TABLE `paytm_refunds` (
  `id` int(11) NOT NULL,
  `user_detail_id` int(11) NOT NULL,
  `orderId` varchar(255) NOT NULL,
  `amount` double NOT NULL,
  `checkSum` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `statusCode` varchar(255) NOT NULL,
  `statusMessage` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paytm_refund_logs`
--

DROP TABLE IF EXISTS `paytm_refund_logs`;
CREATE TABLE `paytm_refund_logs` (
  `id` int(11) NOT NULL,
  `paytm_refund_id` int(11) NOT NULL,
  `user_detail_id` int(11) NOT NULL,
  `orderId` varchar(255) NOT NULL,
  `type` enum('byBank','byQuery') NOT NULL,
  `amount` double NOT NULL,
  `checkSum` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `statusCode` varchar(255) NOT NULL,
  `statusMessage` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

DROP TABLE IF EXISTS `players`;
CREATE TABLE `players` (
  `id` int(11) NOT NULL,
  `player` int(11) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `referal_user_logs`
--

DROP TABLE IF EXISTS `referal_user_logs`;
CREATE TABLE `referal_user_logs` (
  `referLogId` int(11) NOT NULL,
  `fromUserId` int(11) NOT NULL,
  `toUserId` int(11) NOT NULL,
  `referalAmount` double NOT NULL,
  `toUserName` varchar(50) NOT NULL,
  `tableId` int(11) NOT NULL,
  `referalAmountBy` varchar(255) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `referral_users`
--

DROP TABLE IF EXISTS `referral_users`;
CREATE TABLE `referral_users` (
  `id` int(11) NOT NULL,
  `fromReferralUserId` int(11) NOT NULL,
  `toReferralUserId` int(11) NOT NULL,
  `referralBonus` double NOT NULL,
  `isRegister` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reply_logs`
--

DROP TABLE IF EXISTS `reply_logs`;
CREATE TABLE `reply_logs` (
  `id` int(11) NOT NULL,
  `type` enum('Support','Contact') NOT NULL,
  `from_id` int(11) NOT NULL,
  `reply` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

DROP TABLE IF EXISTS `reports`;
CREATE TABLE `reports` (
  `reportId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `reportTitle` varchar(150) NOT NULL,
  `reportDescription` text NOT NULL,
  `reportScreenShot` varchar(255) NOT NULL,
  `reply` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `spin_rolls`
--

DROP TABLE IF EXISTS `spin_rolls`;
CREATE TABLE `spin_rolls` (
  `id` int(11) NOT NULL,
  `title` varchar(225) NOT NULL,
  `value` float NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `spin_rolls`
--

INSERT INTO `spin_rolls` (`id`, `title`, `value`, `status`, `created`, `modified`) VALUES
(1, 'Offer1', 1, 'Active', '2020-05-01 17:59:13', '2020-05-12 15:56:00'),
(4, 'offer2', 2, 'Active', '2020-05-12 12:17:05', '2020-05-12 15:55:32'),
(5, 'Offer3', 3, 'Active', '2020-05-12 15:56:24', '2020-05-12 15:56:24'),
(6, 'Offer4', 4, 'Active', '2020-05-12 15:56:43', '2020-05-12 15:56:43'),
(7, 'Offer5', 5, 'Active', '2020-05-12 15:57:02', '2020-05-12 15:57:02'),
(8, 'Offer6', 6, 'Active', '2020-05-12 15:57:16', '2020-05-12 15:57:16'),
(9, 'Offer7', 7, 'Active', '2020-05-12 15:57:30', '2020-05-12 15:57:30'),
(10, 'Offer8', 8, 'Active', '2020-05-12 15:57:45', '2020-05-12 15:57:45'),
(11, 'Offer9', 9, 'Active', '2020-05-12 15:58:02', '2020-05-12 15:58:02');

-- --------------------------------------------------------

--
-- Table structure for table `support_logs`
--

DROP TABLE IF EXISTS `support_logs`;
CREATE TABLE `support_logs` (
  `supportLogId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `type` enum('User','Admin') NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `isRead` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tournaments`
--

DROP TABLE IF EXISTS `tournaments`;
CREATE TABLE `tournaments` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `betAmt` double NOT NULL,
  `winningAmt` double NOT NULL,
  `noOfPlayers` bigint(20) NOT NULL,
  `round` int(11) NOT NULL,
  `startTime` time NOT NULL,
  `commision` double NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tournament_registrations`
--

DROP TABLE IF EXISTS `tournament_registrations`;
CREATE TABLE `tournament_registrations` (
  `tournamentRegtrationId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `tournamentId` int(11) NOT NULL,
  `userName` varchar(150) NOT NULL,
  `entryFee` int(11) NOT NULL,
  `isEnter` enum('Yes','No') NOT NULL DEFAULT 'No',
  `roundStatus` enum('Win','Loss','Out','Pending','Left','TournamentWiner') NOT NULL DEFAULT 'Pending',
  `round` int(11) NOT NULL,
  `winningPrice` int(11) NOT NULL DEFAULT 0,
  `isDelete` enum('Yes','No') NOT NULL DEFAULT 'No',
  `isWin` enum('Yes','No') NOT NULL DEFAULT 'No',
  `isJoin` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 means not joined 1 means joined',
  `formMainWallet` double NOT NULL,
  `formWinWallet` double NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tournament_registrations`
--

INSERT INTO `tournament_registrations` (`tournamentRegtrationId`, `userId`, `tournamentId`, `userName`, `entryFee`, `isEnter`, `roundStatus`, `round`, `winningPrice`, `isDelete`, `isWin`, `isJoin`, `formMainWallet`, `formWinWallet`, `created`, `modified`) VALUES
(96, 1, 43, 'Vishwa', 10, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 10, 0, '2020-06-16 18:35:52', '2020-06-16 18:35:52'),
(97, 46, 43, 'vishwa1', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 10, 0, '2020-06-16 18:35:53', '2020-06-16 18:35:53'),
(98, 27, 43, 'rajan', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 10, 0, '2020-06-16 18:35:55', '2020-06-16 18:35:55'),
(99, 48, 43, 'Ashish', 10, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 10, 0, '2020-06-16 18:36:28', '2020-06-16 18:36:28'),
(100, 25, 44, 'testplayer', 10, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 10, 0, '2020-06-17 12:14:17', '2020-06-17 12:14:17'),
(101, 46, 44, 'vishwa1', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 10, 0, '2020-06-17 12:14:50', '2020-06-17 12:14:50'),
(102, 1, 44, 'Vishwa', 10, 'No', 'Pending', 1, 0, 'No', 'No', 0, 10, 0, '2020-06-17 12:14:54', '2020-06-17 12:14:54'),
(103, 1, 45, 'Vishwa', 100, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 100, 0, '2020-06-17 12:30:13', '2020-06-17 12:30:13'),
(104, 46, 45, 'vishwa1', 100, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 100, 0, '2020-06-17 12:31:03', '2020-06-17 12:31:03'),
(105, 25, 45, 'testplayer', 100, 'Yes', 'Loss', 2, 0, 'No', 'No', 0, 100, 0, '2020-06-17 12:31:26', '2020-06-17 12:31:26'),
(106, 27, 45, 'rajan', 100, 'Yes', 'Win', 3, 360, 'No', 'Yes', 0, 100, 0, '2020-06-17 12:49:32', '2020-06-17 12:49:32'),
(107, 53, 46, 'test', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 10, 0, '2020-06-17 16:28:29', '2020-06-17 16:28:29'),
(108, 27, 46, 'rajan', 10, 'Yes', 'Win', 3, 36, 'No', 'Yes', 0, 10, 0, '2020-06-17 16:29:14', '2020-06-17 16:29:14'),
(109, 25, 46, 'testplayer', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 10, 0, '2020-06-17 16:31:14', '2020-06-17 16:31:14'),
(110, 1, 46, 'Vishwa', 10, 'Yes', 'Loss', 2, 0, 'No', 'No', 0, 10, 0, '2020-06-17 16:31:46', '2020-06-17 16:31:46'),
(111, 53, 47, 'test', 10, 'Yes', 'Loss', 2, 0, 'No', 'Yes', 0, 5, 5, '2020-07-03 18:14:34', '2020-07-03 18:14:34'),
(112, 25, 47, 'testplayer', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 10, 0, '2020-07-03 18:14:38', '2020-07-03 18:14:38'),
(113, 27, 47, 'rajan', 10, 'Yes', 'Loss', 2, 0, 'No', 'Yes', 0, 10, 0, '2020-07-03 18:15:03', '2020-07-03 18:15:03'),
(115, 73109, 49, 'smstest', 10, 'No', 'Pending', 1, 0, 'No', 'No', 0, 0, 10, '2020-10-22 12:47:38', '2020-10-22 12:47:38'),
(117, 73300, 50, 'testing', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 10, '2020-10-22 16:46:58', '2020-10-22 16:46:58'),
(119, 79186, 50, 'aishu', 10, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 10, 0, '2020-10-22 16:54:40', '2020-10-22 16:54:40'),
(123, 73089, 50, 'mahi', 10, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 10, 0, '2020-10-22 17:22:33', '2020-10-22 17:22:33'),
(124, 73300, 51, 'testing', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 10, '2020-10-23 11:57:34', '2020-10-23 11:57:34'),
(125, 76162, 51, 'aishu', 10, 'No', 'Pending', 1, 0, 'No', 'No', 0, 10, 0, '2020-10-23 12:07:48', '2020-10-23 12:07:48'),
(126, 79186, 51, 'aishu', 10, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 10, 0, '2020-10-23 12:07:58', '2020-10-23 12:07:58'),
(127, 73089, 51, 'mahi', 10, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 10, 0, '2020-10-23 12:13:21', '2020-10-23 12:13:21'),
(128, 81635, 51, 'Anjali G', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 10, 0, '2020-10-23 12:15:01', '2020-10-23 12:15:01'),
(129, 73109, 52, 'smstest', 10, 'Yes', 'Win', 2, 18, 'No', 'Yes', 0, 0, 10, '2020-10-24 13:14:29', '2020-10-24 13:14:29'),
(130, 2204, 52, 'Rajan', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 10, 0, '2020-10-24 13:25:06', '2020-10-24 13:25:06'),
(131, 2204, 53, 'Rajan', 10, 'Yes', 'Win', 2, 18, 'No', 'Yes', 0, 10, 0, '2020-10-24 13:35:19', '2020-10-24 13:35:19'),
(132, 73109, 53, 'smstest', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 10, '2020-10-24 13:35:33', '2020-10-24 13:35:33'),
(133, 76581, 54, 'testplayer', 10, 'Yes', 'Win', 2, 18, 'No', 'Yes', 0, 10, 0, '2020-10-26 16:06:35', '2020-10-26 16:06:35'),
(134, 73109, 54, 'smstest', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 10, '2020-10-26 16:06:39', '2020-10-26 16:06:39'),
(135, 73109, 55, 'smstest', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 10, '2020-10-26 16:20:16', '2020-10-26 16:20:16'),
(136, 76581, 55, 'testplayer', 10, 'Yes', 'Win', 2, 18, 'No', 'Yes', 0, 10, 0, '2020-10-26 16:22:15', '2020-10-26 16:22:15'),
(137, 2204, 56, 'Rajan', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 10, 0, '2020-10-26 16:43:43', '2020-10-26 16:43:43'),
(138, 76581, 56, 'testplayer', 10, 'Yes', 'Loss', 2, 0, 'No', 'Yes', 0, 10, 0, '2020-10-26 16:43:54', '2020-10-26 16:43:54'),
(139, 73109, 56, 'smstest', 10, 'Yes', 'Loss', 2, 0, 'No', 'Yes', 0, 0, 10, '2020-10-26 16:44:00', '2020-10-26 16:44:00'),
(140, 73109, 57, 'smstest', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 10, '2020-10-27 12:23:54', '2020-10-27 12:23:54'),
(141, 76581, 57, 'testplayer', 10, 'Yes', 'Win', 2, 18, 'No', 'Yes', 0, 10, 0, '2020-10-27 12:25:19', '2020-10-27 12:25:19'),
(142, 76162, 58, 'aishu', 10, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 10, 0, '2020-10-27 16:56:39', '2020-10-27 16:56:39'),
(143, 76092, 58, 'SK sanjay', 10, 'No', 'Pending', 1, 0, 'No', 'No', 0, 0, 10, '2020-10-27 17:04:05', '2020-10-27 17:04:05'),
(144, 73300, 58, 'testing', 10, 'No', 'Out', 1, 0, 'No', 'No', 0, 0, 10, '2020-10-27 17:07:02', '2020-10-27 17:07:02'),
(145, 79186, 58, 'aishu', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 10, 0, '2020-10-27 17:14:39', '2020-10-27 17:14:39'),
(146, 76162, 59, 'aishu1', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-10-27 17:37:20', '2020-10-27 17:37:20'),
(147, 79186, 59, 'aishu', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 5, 0, '2020-10-27 17:37:28', '2020-10-27 17:37:28'),
(148, 76092, 59, 'SK sanjay', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 5, '2020-10-27 17:37:49', '2020-10-27 17:37:49'),
(149, 73300, 59, 'testing', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 0, 5, '2020-10-27 17:38:55', '2020-10-27 17:38:55'),
(150, 76581, 60, 'testplayer', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 10, 0, '2020-10-27 20:43:47', '2020-10-27 20:43:47'),
(151, 73109, 60, 'smstest', 10, 'Yes', 'Win', 2, 18, 'No', 'Yes', 0, 0, 10, '2020-10-27 20:44:12', '2020-10-27 20:44:12'),
(152, 73109, 61, 'smstest', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 10, '2020-10-27 21:09:47', '2020-10-27 21:09:47'),
(153, 76162, 61, 'aishu1', 10, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 10, 0, '2020-10-27 21:13:46', '2020-10-27 21:13:46'),
(154, 79186, 61, 'aishu', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 10, 0, '2020-10-27 21:16:33', '2020-10-27 21:16:33'),
(155, 73300, 61, 'testing', 10, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 0, 10, '2020-10-27 21:18:43', '2020-10-27 21:18:43'),
(156, 73109, 62, 'smstest', 10, 'Yes', 'Win', 3, 36, 'No', 'Yes', 0, 0, 10, '2020-10-28 14:48:30', '2020-10-28 14:48:30'),
(157, 76162, 62, 'aishu1', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 10, 0, '2020-10-28 15:12:00', '2020-10-28 15:12:00'),
(158, 76092, 62, 'SK sanjay', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 10, '2020-10-28 15:13:32', '2020-10-28 15:13:32'),
(159, 79186, 62, 'aishu', 10, 'Yes', 'Loss', 2, 0, 'No', 'No', 0, 10, 0, '2020-10-28 15:14:34', '2020-10-28 15:14:34'),
(160, 76162, 63, 'aishu1', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-10-29 11:09:02', '2020-10-29 11:09:02'),
(161, 79186, 63, 'aishu', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 5, 0, '2020-10-29 11:09:04', '2020-10-29 11:09:04'),
(162, 76092, 63, 'SK sanjay', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 5, '2020-10-29 11:09:17', '2020-10-29 11:09:17'),
(163, 73300, 63, 'testing', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 0, 5, '2020-10-29 11:17:32', '2020-10-29 11:17:32'),
(164, 76581, 64, 'testplayer', 10, 'Yes', 'Win', 2, 18, 'No', 'Yes', 0, 10, 0, '2020-11-02 12:05:35', '2020-11-02 12:05:35'),
(165, 73109, 64, 'smstest', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 10, '2020-11-02 12:05:41', '2020-11-02 12:05:41'),
(166, 73300, 65, 'testing', 10, 'Yes', 'Loss', 2, 0, 'No', 'No', 0, 0, 10, '2020-11-02 12:40:29', '2020-11-02 12:40:29'),
(167, 79186, 65, 'aishu', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 10, 0, '2020-11-02 12:41:05', '2020-11-02 12:41:05'),
(168, 76162, 65, 'aishu1', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 10, 0, '2020-11-02 12:41:12', '2020-11-02 12:41:12'),
(169, 76092, 65, 'SK sanjay', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 10, '2020-11-02 12:41:24', '2020-11-02 12:41:24'),
(170, 73109, 65, 'smstest', 10, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 0, 10, '2020-11-02 12:47:40', '2020-11-02 12:47:40'),
(171, 73104, 65, 'Amol G', 10, 'Yes', 'Win', 3, 0, 'No', 'Yes', 0, 0, 10, '2020-11-02 13:06:24', '2020-11-02 13:06:24'),
(172, 76162, 66, 'aishu1', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-02 13:21:47', '2020-11-02 13:21:47'),
(173, 79186, 66, 'aishu', 5, 'No', 'Pending', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-02 13:22:15', '2020-11-02 13:22:15'),
(174, 73109, 66, 'smstest', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 0, 5, '2020-11-02 13:26:59', '2020-11-02 13:26:59'),
(175, 73109, 67, 'smstest', 10, 'Yes', 'Win', 2, 9, 'No', 'Yes', 0, 0, 10, '2020-11-02 13:48:00', '2020-11-02 13:48:00'),
(176, 73300, 68, 'testing', 5, 'Yes', 'Win', 2, 13, 'No', 'Yes', 0, 0, 5, '2020-11-02 14:10:39', '2020-11-02 14:10:39'),
(177, 76162, 68, 'aishu1', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-02 14:12:32', '2020-11-02 14:12:32'),
(178, 73109, 68, 'smstest', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 5, '2020-11-02 14:12:37', '2020-11-02 14:12:37'),
(179, 76162, 69, 'aishu1', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 5, 0, '2020-11-02 18:13:28', '2020-11-02 18:13:28'),
(180, 79186, 69, 'aishu', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-02 18:14:01', '2020-11-02 18:14:01'),
(181, 76092, 69, 'SK sanjay', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 0, 5, '2020-11-02 18:14:11', '2020-11-02 18:14:11'),
(182, 73300, 69, 'testing', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 0, 5, '2020-11-02 18:14:13', '2020-11-02 18:14:13'),
(183, 73104, 69, 'Amol G', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 5, '2020-11-02 18:15:35', '2020-11-02 18:15:35'),
(184, 79186, 70, 'aishu', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 0, 5, '2020-11-03 13:03:44', '2020-11-03 13:03:44'),
(185, 73109, 70, 'smstest', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 0, 5, '2020-11-03 13:04:40', '2020-11-03 13:04:40'),
(186, 76092, 70, 'SK sanjay', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 5, '2020-11-03 13:05:19', '2020-11-03 13:05:19'),
(187, 73300, 71, 'testing', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 10, '2020-11-03 13:32:29', '2020-11-03 13:32:29'),
(188, 79186, 71, 'aishu', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 10, '2020-11-03 13:33:30', '2020-11-03 13:33:30'),
(189, 73109, 71, 'smstest', 10, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 0, 10, '2020-11-03 13:33:46', '2020-11-03 13:33:46'),
(190, 76581, 71, 'testplayer', 10, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 10, 0, '2020-11-03 13:33:54', '2020-11-03 13:33:54'),
(191, 79186, 72, 'aishu', 5, 'No', 'Pending', 1, 0, 'No', 'No', 0, 0, 5, '2020-11-03 13:55:46', '2020-11-03 13:55:46'),
(192, 73300, 72, 'testing', 5, 'No', 'Out', 1, 0, 'No', 'No', 0, 0, 5, '2020-11-03 13:57:24', '2020-11-03 13:57:24'),
(193, 73109, 72, 'smstest', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 0, 5, '2020-11-03 14:31:19', '2020-11-03 14:31:19'),
(194, 76581, 72, 'testplayer', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-03 14:31:29', '2020-11-03 14:31:29'),
(195, 79186, 73, 'aishu', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 5, '2020-11-03 14:43:39', '2020-11-03 14:43:39'),
(196, 73300, 73, 'testing', 5, 'No', 'Out', 1, 0, 'No', 'No', 0, 0, 5, '2020-11-03 14:51:10', '2020-11-03 14:51:10'),
(197, 73109, 73, 'smstest', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 0, 5, '2020-11-03 14:53:57', '2020-11-03 14:53:57'),
(198, 76581, 73, 'testplayer', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 5, 0, '2020-11-03 14:54:54', '2020-11-03 14:54:54'),
(199, 76162, 74, 'aishu1', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-03 18:52:53', '2020-11-03 18:52:53'),
(200, 79186, 74, 'aishu', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 5, '2020-11-03 18:53:01', '2020-11-03 18:53:01'),
(201, 73300, 74, 'testing', 5, 'Yes', 'Loss', 2, 17, 'No', 'No', 0, 0, 5, '2020-11-03 18:57:48', '2020-11-03 18:57:48'),
(202, 76092, 74, 'SK sanjay', 5, 'Yes', 'Win', 3, 0, 'No', 'Yes', 0, 5, 0, '2020-11-03 19:03:04', '2020-11-03 19:03:04'),
(203, 76162, 75, 'aishu1', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-04 13:06:04', '2020-11-04 13:06:04'),
(204, 73300, 75, 'testing', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 0, 5, '2020-11-04 13:06:20', '2020-11-04 13:06:20'),
(205, 76092, 75, 'SK sanjay', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 5, 0, '2020-11-04 13:06:47', '2020-11-04 13:06:47'),
(206, 79186, 75, 'aishu', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 5, '2020-11-04 13:06:54', '2020-11-04 13:06:54'),
(207, 79186, 76, 'aishu', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 5, '2020-11-04 14:04:41', '2020-11-04 14:04:41'),
(208, 73300, 76, 'testing', 5, 'Yes', 'Win', 3, 17, 'No', 'Yes', 0, 0, 5, '2020-11-04 14:05:39', '2020-11-04 14:05:39'),
(209, 76162, 76, 'aishu1', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-04 14:06:10', '2020-11-04 14:06:10'),
(210, 76092, 76, 'SK sanjay', 5, 'Yes', 'Loss', 2, 0, 'No', 'No', 0, 5, 0, '2020-11-04 14:07:26', '2020-11-04 14:07:26'),
(212, 100752, 77, 'shubham jagtap', 5, 'Yes', 'Loss', 3, 0, 'No', 'No', 0, 5, 0, '2020-11-04 16:46:35', '2020-11-04 16:46:35'),
(213, 157, 77, 'Ramkrushna Kadam', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-04 16:46:40', '2020-11-04 16:46:40'),
(214, 100750, 77, 'piyush bhandari', 5, 'Yes', 'Win', 5, 52, 'No', 'Yes', 0, 5, 0, '2020-11-04 16:46:57', '2020-11-04 16:46:57'),
(215, 100801, 77, 'Swapnali Dabhade', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-04 16:46:58', '2020-11-04 16:46:58'),
(216, 100763, 77, 'shraddha Jadhav', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-04 16:46:58', '2020-11-04 16:46:58'),
(218, 100751, 77, 'Harshal', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-04 16:47:16', '2020-11-04 16:47:16'),
(220, 79186, 77, 'aishu', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 5, '2020-11-04 16:47:45', '2020-11-04 16:47:45'),
(221, 100800, 77, 'Sohail Mujawar', 5, 'Yes', 'Loss', 2, 0, 'No', 'No', 0, 5, 0, '2020-11-04 16:48:10', '2020-11-04 16:48:10'),
(223, 76092, 77, 'SK sanjay', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-04 16:49:06', '2020-11-04 16:49:06'),
(224, 76162, 77, 'aishu1', 5, 'Yes', 'Loss', 4, 0, 'No', 'No', 0, 5, 0, '2020-11-04 16:50:19', '2020-11-04 16:50:19'),
(225, 73300, 77, 'testing', 5, 'Yes', 'Loss', 2, 0, 'No', 'No', 0, 0, 5, '2020-11-04 16:50:48', '2020-11-04 16:50:48'),
(226, 70961, 77, 'ji', 5, 'Yes', 'Loss', 2, 0, 'No', 'Yes', 0, 0, 5, '2020-11-04 16:52:14', '2020-11-04 16:52:14'),
(227, 100758, 77, 'Mayuri', 5, 'Yes', 'Loss', 2, 0, 'No', 'Yes', 0, 5, 0, '2020-11-04 16:52:40', '2020-11-04 16:52:40'),
(228, 100788, 77, 'Mamta', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-04 16:57:23', '2020-11-04 16:57:23'),
(230, 81635, 77, 'Anjali G', 5, 'Yes', 'Loss', 2, 0, 'No', 'No', 0, 5, 0, '2020-11-04 16:59:37', '2020-11-04 16:59:37'),
(231, 79186, 78, 'aishu', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 5, '2020-11-06 11:55:22', '2020-11-06 11:55:22'),
(232, 73300, 78, 'testing', 5, 'Yes', 'Loss', 2, 17, 'No', 'No', 0, 0, 5, '2020-11-06 11:55:54', '2020-11-06 11:55:54'),
(234, 76092, 78, 'SK sanjay', 5, 'Yes', 'Win', 3, 0, 'No', 'Yes', 0, 5, 0, '2020-11-06 11:56:57', '2020-11-06 11:56:57'),
(235, 76162, 78, 'aishu1', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-06 11:57:33', '2020-11-06 11:57:33'),
(236, 76092, 79, 'SK sanjay', 5, 'Yes', 'Win', 3, 17, 'No', 'Yes', 0, 5, 0, '2020-11-06 14:43:03', '2020-11-06 14:43:03'),
(237, 73300, 79, 'testing', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 5, '2020-11-06 14:43:36', '2020-11-06 14:43:36'),
(238, 79186, 79, 'aishu', 5, 'Yes', 'Loss', 2, 0, 'No', 'No', 0, 0, 5, '2020-11-06 15:33:14', '2020-11-06 15:33:14'),
(239, 76162, 79, 'aishu1', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-06 15:33:59', '2020-11-06 15:33:59'),
(240, 76581, 80, 'testplayer', 10, 'Yes', 'Loss', 2, 0, 'No', 'Yes', 0, 10, 0, '2020-11-10 15:55:59', '2020-11-10 15:55:59'),
(241, 73109, 80, 'smstest', 10, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 10, '2020-11-10 15:56:06', '2020-11-10 15:56:06'),
(242, 22325, 80, 'vishwa', 10, 'Yes', 'Loss', 2, 0, 'No', 'Yes', 0, 0, 10, '2020-11-10 15:56:08', '2020-11-10 15:56:08'),
(243, 76162, 81, 'aishu1', 5, 'Yes', 'Win', 3, 0, 'No', 'Yes', 0, 5, 0, '2020-11-26 15:22:55', '2020-11-26 15:22:55'),
(245, 76092, 81, 'SK sanjay', 5, 'Yes', 'Win', 4, 0, 'No', 'Yes', 0, 0, 5, '2020-11-26 15:26:34', '2020-11-26 15:26:34'),
(246, 100750, 81, 'piyush bhandari', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-26 15:26:49', '2020-11-26 15:26:49'),
(247, 123709, 81, 'pragati', 5, 'Yes', 'Loss', 3, 0, 'No', 'Yes', 0, 5, 0, '2020-11-26 15:27:18', '2020-11-26 15:27:18'),
(248, 100788, 81, 'Mamta', 5, 'No', 'Out', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-26 15:28:53', '2020-11-26 15:28:53'),
(249, 123725, 81, 'shraddha ', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-26 15:28:58', '2020-11-26 15:28:58'),
(250, 73300, 81, 'testing', 5, 'Yes', 'Loss', 2, 0, 'No', 'No', 0, 0, 5, '2020-11-26 15:30:24', '2020-11-26 15:30:24'),
(252, 123728, 81, 'jay', 5, 'No', 'Out', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-26 15:30:58', '2020-11-26 15:30:58'),
(253, 100751, 81, 'Harshal', 5, 'Yes', 'Win', 3, 0, 'No', 'Yes', 0, 5, 0, '2020-11-26 15:31:29', '2020-11-26 15:31:29'),
(254, 123727, 81, 'rubina', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 5, 0, '2020-11-26 15:34:01', '2020-11-26 15:34:01'),
(256, 123729, 81, 'raj gaikwad', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 5, 0, '2020-11-26 15:35:07', '2020-11-26 15:35:07'),
(257, 100758, 81, 'Mayuri', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-26 15:35:25', '2020-11-26 15:35:25'),
(259, 70961, 81, 'ji', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 5, '2020-11-26 15:37:07', '2020-11-26 15:37:07'),
(260, 123732, 81, 'varun', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-26 15:41:49', '2020-11-26 15:41:49'),
(261, 100797, 81, 'Shabista', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-26 15:42:51', '2020-11-26 15:42:51'),
(263, 73104, 81, 'Amol G', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 5, '2020-11-26 15:57:09', '2020-11-26 15:57:09'),
(264, 76162, 82, 'aishu1', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-26 16:36:03', '2020-11-26 16:36:03'),
(265, 100758, 82, 'Mayuri', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-26 16:36:36', '2020-11-26 16:36:36'),
(266, 100751, 82, 'Harshal', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 5, 0, '2020-11-26 16:36:53', '2020-11-26 16:36:53'),
(267, 123731, 82, 'Rushi sathe ', 5, 'No', 'Pending', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-26 16:37:26', '2020-11-26 16:37:26'),
(268, 123732, 82, 'varun', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 5, 0, '2020-11-26 16:37:44', '2020-11-26 16:37:44'),
(269, 123725, 82, 'shraddha ', 5, 'No', 'Pending', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-26 16:38:22', '2020-11-26 16:38:22'),
(270, 123709, 82, 'pragati', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-26 16:38:29', '2020-11-26 16:38:29'),
(271, 73104, 82, 'Amol G', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 0, 5, '2020-11-26 16:38:54', '2020-11-26 16:38:54'),
(272, 73300, 82, 'testing', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 5, '2020-11-26 16:38:56', '2020-11-26 16:38:56'),
(273, 76092, 82, 'SK sanjay', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 0, 5, '2020-11-26 16:39:26', '2020-11-26 16:39:26'),
(275, 100800, 82, 'Sohail Mujawar', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 5, 0, '2020-11-26 16:41:42', '2020-11-26 16:41:42'),
(277, 100750, 82, 'piyush bhandari', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 5, 0, '2020-11-26 16:42:15', '2020-11-26 16:42:15'),
(278, 100788, 82, 'Mamta', 5, 'No', 'Out', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-26 16:42:39', '2020-11-26 16:42:39'),
(280, 123694, 82, 'ankita', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 5, 0, '2020-11-26 16:43:03', '2020-11-26 16:43:03'),
(281, 123728, 82, 'jay', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-26 16:50:06', '2020-11-26 16:50:06'),
(282, 70961, 82, 'ji', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 0, 5, '2020-11-26 16:54:20', '2020-11-26 16:54:20'),
(283, 76162, 83, 'aishu1', 5, 'Yes', 'Win', 2, 68, 'No', 'Yes', 0, 5, 0, '2020-11-27 10:50:55', '2020-11-27 10:50:55'),
(286, 100750, 83, 'piyush bhandari', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 5, 0, '2020-11-27 10:51:22', '2020-11-27 10:51:22'),
(287, 123694, 83, 'ankita', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-27 10:51:26', '2020-11-27 10:51:26'),
(288, 100752, 83, 'shubham jagtap', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-27 10:52:08', '2020-11-27 10:52:08'),
(289, 100758, 83, 'Mayuri', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 5, 0, '2020-11-27 10:52:17', '2020-11-27 10:52:17'),
(290, 70961, 83, 'ji', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 0, 5, '2020-11-27 10:54:37', '2020-11-27 10:54:37'),
(291, 100800, 83, 'Sohail Mujawar', 5, 'Yes', 'Loss', 2, 0, 'No', 'No', 0, 5, 0, '2020-11-27 11:01:28', '2020-11-27 11:01:28'),
(292, 100751, 83, 'Harshal', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-27 11:01:41', '2020-11-27 11:01:41'),
(293, 157, 83, 'R.K', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-27 11:02:04', '2020-11-27 11:02:04'),
(300, 123902, 83, 'snehal', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 5, 0, '2020-11-27 11:30:50', '2020-11-27 11:30:50'),
(301, 73300, 83, 'testing', 5, 'Yes', 'Win', 2, 0, 'No', 'Yes', 0, 5, 0, '2020-11-27 11:32:17', '2020-11-27 11:32:17'),
(302, 81635, 83, 'Anjali G', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-27 11:34:44', '2020-11-27 11:34:44'),
(303, 81638, 83, 'Osomose', 5, 'Yes', 'Win', 4, 0, 'No', 'Yes', 0, 5, 0, '2020-11-27 11:40:02', '2020-11-27 11:40:02'),
(307, 123727, 83, 'rubina', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-27 11:41:19', '2020-11-27 11:41:19'),
(308, 100763, 83, 'shraddha Jadhav', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-27 11:41:28', '2020-11-27 11:41:28'),
(309, 123728, 83, 'jay', 5, 'Yes', 'Loss', 1, 0, 'No', 'No', 0, 5, 0, '2020-11-27 11:41:37', '2020-11-27 11:41:37');

-- --------------------------------------------------------

--
-- Table structure for table `tournament_win_loss_logs`
--

DROP TABLE IF EXISTS `tournament_win_loss_logs`;
CREATE TABLE `tournament_win_loss_logs` (
  `tournamentWinLossLogId` int(11) NOT NULL,
  `tournamentId` int(11) NOT NULL,
  `tournamentTitle` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `startDate` varchar(100) NOT NULL,
  `startTime` varchar(100) NOT NULL,
  `userName` varchar(150) NOT NULL,
  `entryFee` int(11) NOT NULL,
  `round` int(11) NOT NULL,
  `roundStatus` enum('Win','Loss') NOT NULL,
  `playerLimitInRoom` int(11) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tournament_win_loss_logs`
--

INSERT INTO `tournament_win_loss_logs` (`tournamentWinLossLogId`, `tournamentId`, `tournamentTitle`, `userId`, `startDate`, `startTime`, `userName`, `entryFee`, `round`, `roundStatus`, `playerLimitInRoom`, `created`) VALUES
(42, 43, 'Test10', 46, '2020-06-16', '18:36:00', 'vishwa1', 10, 1, 'Loss', 2, '2020-06-16 18:38:40'),
(43, 43, 'Test10', 1, '2020-06-16', '18:36:00', 'Vishwa', 10, 1, 'Win', 2, '2020-06-16 18:38:40'),
(44, 43, 'Test10', 27, '2020-06-16', '18:36:00', 'rajan', 10, 1, 'Loss', 2, '2020-06-16 18:39:21'),
(45, 43, 'Test10', 48, '2020-06-16', '18:36:00', 'Ashish', 10, 1, 'Win', 2, '2020-06-16 18:39:21'),
(46, 44, 'Test123', 46, '2020-06-17', '12:15:00', 'vishwa1', 10, 1, 'Loss', 2, '2020-06-17 12:16:14'),
(47, 44, 'Test123', 25, '2020-06-17', '12:15:00', 'testplayer', 10, 1, 'Win', 2, '2020-06-17 12:16:14'),
(48, 45, 'Test21', 46, '2020-06-17', '12:51:00', 'vishwa1', 100, 1, 'Loss', 2, '2020-06-17 12:52:42'),
(49, 45, 'Test21', 25, '2020-06-17', '12:51:00', 'testplayer', 100, 1, 'Win', 2, '2020-06-17 12:52:43'),
(50, 45, 'Test21', 1, '2020-06-17', '12:51:00', 'Vishwa', 100, 1, 'Loss', 2, '2020-06-17 12:53:04'),
(51, 45, 'Test21', 27, '2020-06-17', '12:51:00', 'rajan', 100, 1, 'Win', 2, '2020-06-17 12:53:05'),
(52, 45, 'Test21', 25, '2020-06-17', '12:56:00', 'testplayer', 100, 2, 'Loss', 2, '2020-06-17 12:58:19'),
(53, 45, 'Test21', 27, '2020-06-17', '12:56:00', 'rajan', 100, 2, 'Win', 2, '2020-06-17 12:58:20'),
(54, 46, 'testskill', 25, '2020-06-17', '16:32:00', 'testplayer', 10, 1, 'Loss', 2, '2020-06-17 16:33:28'),
(55, 46, 'testskill', 27, '2020-06-17', '16:32:00', 'rajan', 10, 1, 'Win', 2, '2020-06-17 16:33:29'),
(56, 46, 'testskill', 53, '2020-06-17', '16:32:00', 'test', 10, 1, 'Loss', 2, '2020-06-17 16:35:53'),
(57, 46, 'testskill', 1, '2020-06-17', '16:32:00', 'Vishwa', 10, 1, 'Win', 2, '2020-06-17 16:35:53'),
(58, 46, 'testskill', 27, '2020-06-17', '16:37:00', 'rajan', 10, 2, 'Win', 2, '2020-06-17 16:43:11'),
(59, 46, 'testskill', 1, '2020-06-17', '16:37:00', 'Vishwa', 10, 2, 'Loss', 2, '2020-06-17 16:43:11'),
(60, 47, 'test111', 25, '2020-07-03', '18:15:00', 'testplayer', 10, 1, 'Loss', 2, '2020-07-03 18:16:28'),
(61, 47, 'test111', 53, '2020-07-03', '18:15:00', 'test', 10, 1, 'Win', 2, '2020-07-03 18:16:28'),
(62, 47, 'test111', 27, '2020-07-03', '18:15:00', 'rajan', 10, 1, 'Win', 2, '2020-07-03 18:19:55'),
(63, 47, 'test111', 53, '2020-07-03', '18:21:00', 'test', 10, 2, 'Loss', 2, '2020-07-03 18:23:07'),
(64, 47, 'test111', 27, '2020-07-03', '18:21:00', 'rajan', 10, 2, 'Loss', 2, '2020-07-03 18:23:42'),
(65, 50, 'Test 2', 73300, '2020-10-22', '18:15:00', 'testing', 10, 1, 'Loss', 2, '2020-10-22 18:18:25'),
(66, 50, 'Test 2', 73089, '2020-10-22', '18:15:00', 'mahi', 10, 1, 'Win', 2, '2020-10-22 18:18:26'),
(67, 50, 'Test 2', 79186, '2020-10-22', '18:15:00', 'aishu', 10, 1, 'Win', 2, '2020-10-22 18:19:52'),
(68, 51, 'weekend contest', 81635, '2020-10-23', '12:30:00', 'Anjali G', 10, 1, 'Loss', 2, '2020-10-23 12:32:12'),
(69, 51, 'weekend contest', 73089, '2020-10-23', '12:30:00', 'mahi', 10, 1, 'Win', 2, '2020-10-23 12:32:13'),
(70, 51, 'weekend contest', 73300, '2020-10-23', '12:30:00', 'testing', 10, 1, 'Loss', 2, '2020-10-23 12:32:19'),
(71, 51, 'weekend contest', 79186, '2020-10-23', '12:30:00', 'aishu', 10, 1, 'Win', 2, '2020-10-23 12:32:20'),
(72, 52, 'Tour12', 2204, '2020-10-24', '13:26:00', 'Rajan', 10, 1, 'Loss', 2, '2020-10-24 13:28:13'),
(73, 52, 'Tour12', 73109, '2020-10-24', '13:26:00', 'smstest', 10, 1, 'Win', 2, '2020-10-24 13:28:14'),
(74, 53, 'Tour13', 73109, '2020-10-24', '13:36:00', 'smstest', 10, 1, 'Loss', 2, '2020-10-24 13:38:15'),
(75, 53, 'Tour13', 2204, '2020-10-24', '13:36:00', 'Rajan', 10, 1, 'Win', 2, '2020-10-24 13:38:16'),
(76, 54, 'Tour1', 73109, '2020-10-26', '16:07:00', 'smstest', 10, 1, 'Loss', 2, '2020-10-26 16:09:27'),
(77, 54, 'Tour1', 76581, '2020-10-26', '16:07:00', 'testplayer', 10, 1, 'Win', 2, '2020-10-26 16:09:28'),
(78, 55, 'Tour1', 73109, '2020-10-26', '16:23:00', 'smstest', 10, 1, 'Loss', 2, '2020-10-26 16:27:11'),
(79, 55, 'Tour1', 76581, '2020-10-26', '16:23:00', 'testplayer', 10, 1, 'Win', 2, '2020-10-26 16:27:12'),
(80, 56, 'Tour26', 2204, '2020-10-26', '16:45:00', 'Rajan', 10, 1, 'Loss', 2, '2020-10-26 16:47:38'),
(81, 56, 'Tour26', 73109, '2020-10-26', '16:45:00', 'smstest', 10, 1, 'Win', 2, '2020-10-26 16:47:38'),
(82, 56, 'Tour26', 76581, '2020-10-26', '16:45:00', 'testplayer', 10, 1, 'Win', 2, '2020-10-26 16:49:53'),
(83, 56, 'Tour26', 73109, '2020-10-26', '16:53:00', 'smstest', 10, 2, 'Loss', 2, '2020-10-26 16:54:04'),
(84, 56, 'Tour26', 76581, '2020-10-26', '16:53:00', 'testplayer', 10, 2, 'Loss', 2, '2020-10-26 16:54:11'),
(85, 57, 'Tour27', 73109, '2020-10-27', '12:26:00', 'smstest', 10, 1, 'Loss', 2, '2020-10-27 12:30:58'),
(86, 57, 'Tour27', 76581, '2020-10-27', '12:26:00', 'testplayer', 10, 1, 'Win', 2, '2020-10-27 12:30:58'),
(87, 58, 'Testing', 79186, '2020-10-27', '17:20:00', 'aishu', 10, 1, 'Loss', 2, '2020-10-27 17:22:28'),
(88, 58, 'Testing', 76162, '2020-10-27', '17:20:00', 'aishu', 10, 1, 'Win', 2, '2020-10-27 17:22:29'),
(89, 59, 'testing2', 76092, '2020-10-27', '17:42:00', 'SK sanjay', 5, 1, 'Loss', 2, '2020-10-27 17:43:50'),
(90, 59, 'testing2', 73300, '2020-10-27', '17:42:00', 'testing', 5, 1, 'Win', 2, '2020-10-27 17:43:51'),
(91, 59, 'testing2', 79186, '2020-10-27', '17:42:00', 'aishu', 5, 1, 'Win', 2, '2020-10-27 17:51:59'),
(92, 59, 'testing2', 76162, '2020-10-27', '17:42:00', 'aishu1', 5, 1, 'Loss', 2, '2020-10-27 17:51:59'),
(93, 60, 'Tour278', 76581, '2020-10-27', '20:45:00', 'testplayer', 10, 1, 'Loss', 2, '2020-10-27 20:50:19'),
(94, 60, 'Tour278', 73109, '2020-10-27', '20:45:00', 'smstest', 10, 1, 'Win', 2, '2020-10-27 20:50:20'),
(95, 61, 'Tour27', 73109, '2020-10-27', '21:22:00', 'smstest', 10, 1, 'Loss', 2, '2020-10-27 21:30:22'),
(96, 61, 'Tour27', 76162, '2020-10-27', '21:22:00', 'aishu1', 10, 1, 'Win', 2, '2020-10-27 21:30:23'),
(97, 61, 'Tour27', 79186, '2020-10-27', '21:22:00', 'aishu', 10, 1, 'Loss', 2, '2020-10-27 21:36:29'),
(98, 61, 'Tour27', 73300, '2020-10-27', '21:22:00', 'testing', 10, 1, 'Win', 2, '2020-10-27 21:36:30'),
(99, 62, 'Tour28', 76092, '2020-10-28', '15:16:00', 'SK sanjay', 10, 1, 'Loss', 2, '2020-10-28 15:17:58'),
(100, 62, 'Tour28', 79186, '2020-10-28', '15:16:00', 'aishu', 10, 1, 'Win', 2, '2020-10-28 15:17:59'),
(101, 62, 'Tour28', 76162, '2020-10-28', '15:16:00', 'aishu1', 10, 1, 'Loss', 2, '2020-10-28 15:31:24'),
(102, 62, 'Tour28', 73109, '2020-10-28', '15:16:00', 'smstest', 10, 1, 'Win', 2, '2020-10-28 15:31:25'),
(103, 62, 'Tour28', 79186, '2020-10-28', '16:31:27', 'aishu', 10, 2, 'Loss', 2, '2020-10-28 16:35:54'),
(104, 62, 'Tour28', 73109, '2020-10-28', '16:31:27', 'smstest', 10, 2, 'Win', 2, '2020-10-28 16:35:55'),
(105, 63, 'tournamnet1', 76092, '2020-10-29', '11:18:00', 'SK sanjay', 5, 1, 'Loss', 2, '2020-10-29 11:21:35'),
(106, 63, 'tournamnet1', 73300, '2020-10-29', '11:18:00', 'testing', 5, 1, 'Win', 2, '2020-10-29 11:21:36'),
(107, 63, 'tournamnet1', 76162, '2020-10-29', '11:18:00', 'aishu1', 5, 1, 'Loss', 2, '2020-10-29 11:26:15'),
(108, 63, 'tournamnet1', 79186, '2020-10-29', '11:18:00', 'aishu', 5, 1, 'Win', 2, '2020-10-29 11:26:16'),
(109, 64, 'Tour2', 76581, '2020-11-02', '12:06:00', 'testplayer', 10, 1, 'Win', 2, '2020-11-02 12:17:34'),
(110, 64, 'Tour2', 73109, '2020-11-02', '12:06:00', 'smstest', 10, 1, 'Loss', 2, '2020-11-02 12:17:34'),
(111, 65, 'tournament 2', 79186, '2020-11-02', '13:08:00', 'aishu', 10, 1, 'Loss', 2, '2020-11-02 13:14:28'),
(112, 65, 'tournament 2', 73300, '2020-11-02', '13:08:00', 'testing', 10, 1, 'Win', 2, '2020-11-02 13:14:29'),
(113, 65, 'tournament 2', 76162, '2020-11-02', '13:08:00', 'aishu1', 10, 1, 'Loss', 2, '2020-11-02 13:15:54'),
(114, 65, 'tournament 2', 73109, '2020-11-02', '13:08:00', 'smstest', 10, 1, 'Win', 2, '2020-11-02 13:15:55'),
(115, 65, 'tournament 2', 73104, '2020-11-02', '13:08:00', 'Amol G', 10, 1, 'Win', 2, '2020-11-02 13:28:30'),
(116, 65, 'tournament 2', 76092, '2020-11-02', '13:08:00', 'SK sanjay', 10, 1, 'Loss', 2, '2020-11-02 13:28:30'),
(117, 66, 'Tournament 3', 73109, '2020-11-02', '13:27:00', 'smstest', 5, 1, 'Win', 2, '2020-11-02 13:39:47'),
(118, 66, 'Tournament 3', 76162, '2020-11-02', '13:27:00', 'aishu1', 5, 1, 'Loss', 2, '2020-11-02 13:39:47'),
(119, 67, 'Tour4', 73109, '2020-11-02', '13:49:00', 'smstest', 10, 1, 'Win', 2, '2020-11-02 13:53:58'),
(120, 68, 'Tournament 4', 73109, '2020-11-02', '14:12:00', 'smstest', 5, 1, 'Loss', 3, '2020-11-02 14:15:41'),
(121, 68, 'Tournament 4', 76162, '2020-11-02', '14:12:00', 'aishu1', 5, 1, 'Loss', 3, '2020-11-02 14:19:13'),
(122, 68, 'Tournament 4', 73300, '2020-11-02', '14:12:00', 'testing', 5, 1, 'Win', 3, '2020-11-02 14:19:15'),
(123, 65, 'tournament 2', 73300, '2020-11-02', '14:28:32', 'testing', 10, 2, 'Loss', 2, '2020-11-02 14:39:35'),
(124, 65, 'tournament 2', 73104, '2020-11-02', '14:28:32', 'Amol G', 10, 2, 'Win', 2, '2020-11-02 14:39:36'),
(125, 69, 'Tournament 5', 76162, '2020-11-02', '18:16:00', 'aishu1', 5, 1, 'Win', 2, '2020-11-02 18:20:55'),
(126, 69, 'Tournament 5', 73104, '2020-11-02', '18:16:00', 'Amol G', 5, 1, 'Loss', 2, '2020-11-02 18:22:26'),
(127, 69, 'Tournament 5', 73300, '2020-11-02', '18:16:00', 'testing', 5, 1, 'Win', 2, '2020-11-02 18:22:27'),
(128, 69, 'Tournament 5', 79186, '2020-11-02', '18:16:00', 'aishu', 5, 1, 'Loss', 2, '2020-11-02 18:25:21'),
(129, 69, 'Tournament 5', 76092, '2020-11-02', '18:16:00', 'SK sanjay', 5, 1, 'Win', 2, '2020-11-02 18:25:23'),
(130, 70, 'Tournament 6', 76092, '2020-11-03', '13:05:00', 'SK sanjay', 5, 1, 'Loss', 2, '2020-11-03 13:07:40'),
(131, 70, 'Tournament 6', 79186, '2020-11-03', '13:05:00', 'aishu', 5, 1, 'Win', 2, '2020-11-03 13:07:41'),
(132, 70, 'Tournament 6', 73109, '2020-11-03', '13:05:00', 'smstest', 5, 1, 'Win', 2, '2020-11-03 13:09:56'),
(133, 71, 'Tournament 7', 73300, '2020-11-03', '13:34:00', 'testing', 10, 1, 'Loss', 2, '2020-11-03 13:36:36'),
(134, 71, 'Tournament 7', 76581, '2020-11-03', '13:34:00', 'testplayer', 10, 1, 'Win', 2, '2020-11-03 13:36:37'),
(135, 71, 'Tournament 7', 79186, '2020-11-03', '13:34:00', 'aishu', 10, 1, 'Loss', 2, '2020-11-03 13:37:40'),
(136, 71, 'Tournament 7', 73109, '2020-11-03', '13:34:00', 'smstest', 10, 1, 'Win', 2, '2020-11-03 13:37:41'),
(137, 72, 'Tournament 8', 76581, '2020-11-03', '14:34:00', 'testplayer', 5, 1, 'Loss', 2, '2020-11-03 14:38:28'),
(138, 72, 'Tournament 8', 73109, '2020-11-03', '14:34:00', 'smstest', 5, 1, 'Win', 2, '2020-11-03 14:38:29'),
(139, 73, 'Tournament 8', 76581, '2020-11-03', '14:58:00', 'testplayer', 5, 1, 'Win', 2, '2020-11-03 15:02:55'),
(140, 73, 'Tournament 8', 79186, '2020-11-03', '14:58:00', 'aishu', 5, 1, 'Loss', 2, '2020-11-03 15:10:51'),
(141, 73, 'Tournament 8', 73109, '2020-11-03', '14:58:00', 'smstest', 5, 1, 'Win', 2, '2020-11-03 15:10:51'),
(142, 74, 'Tournament 9', 76092, '2020-11-03', '19:04:00', 'SK sanjay', 5, 1, 'Win', 2, '2020-11-03 19:19:02'),
(143, 74, 'Tournament 9', 76162, '2020-11-03', '19:04:00', 'aishu1', 5, 1, 'Loss', 2, '2020-11-03 19:19:02'),
(144, 74, 'Tournament 9', 73300, '2020-11-03', '19:04:00', 'testing', 5, 1, 'Win', 2, '2020-11-03 19:21:12'),
(145, 74, 'Tournament 9', 79186, '2020-11-03', '19:04:00', 'aishu', 5, 1, 'Loss', 2, '2020-11-03 19:21:12'),
(146, 74, 'Tournament 9', 73300, '2020-11-03', '20:21:15', 'testing', 5, 2, 'Loss', 2, '2020-11-03 20:33:11'),
(147, 74, 'Tournament 9', 76092, '2020-11-03', '20:21:15', 'SK sanjay', 5, 2, 'Win', 2, '2020-11-03 20:33:11'),
(148, 75, 'Tournament 10', 79186, '2020-11-04', '13:45:00', 'aishu', 5, 1, 'Loss', 2, '2020-11-04 13:48:17'),
(149, 75, 'Tournament 10', 73300, '2020-11-04', '13:45:00', 'testing', 5, 1, 'Win', 2, '2020-11-04 13:48:18'),
(150, 75, 'Tournament 10', 76162, '2020-11-04', '13:45:00', 'aishu1', 5, 1, 'Loss', 2, '2020-11-04 13:48:42'),
(151, 75, 'Tournament 10', 76092, '2020-11-04', '13:45:00', 'SK sanjay', 5, 1, 'Win', 2, '2020-11-04 13:48:43'),
(152, 76, 'Tournament 11', 76162, '2020-11-04', '14:13:00', 'aishu1', 5, 1, 'Loss', 2, '2020-11-04 14:17:18'),
(153, 76, 'Tournament 11', 76092, '2020-11-04', '14:13:00', 'SK sanjay', 5, 1, 'Win', 2, '2020-11-04 14:17:19'),
(154, 76, 'Tournament 11', 73300, '2020-11-04', '14:13:00', 'testing', 5, 1, 'Win', 2, '2020-11-04 14:30:14'),
(155, 76, 'Tournament 11', 79186, '2020-11-04', '14:13:00', 'aishu', 5, 1, 'Loss', 2, '2020-11-04 14:30:14'),
(156, 76, 'Tournament 11', 73300, '2020-11-04', '15:30:15', 'testing', 5, 2, 'Win', 2, '2020-11-04 15:53:14'),
(157, 76, 'Tournament 11', 76092, '2020-11-04', '15:30:15', 'SK sanjay', 5, 2, 'Loss', 2, '2020-11-04 15:53:14'),
(158, 77, 'Tournament 12', 81635, '2020-11-04', '16:56:00', 'Anjali G', 5, 1, 'Win', 2, '2020-11-04 17:00:52'),
(159, 77, 'Tournament 12', 100788, '2020-11-04', '16:56:00', 'Mamta', 5, 1, 'Loss', 2, '2020-11-04 17:01:11'),
(160, 77, 'Tournament 12', 100752, '2020-11-04', '16:56:00', 'shubham jagtap', 5, 1, 'Win', 2, '2020-11-04 17:01:12'),
(161, 77, 'Tournament 12', 70961, '2020-11-04', '16:56:00', 'ji', 5, 1, 'Win', 2, '2020-11-04 17:07:40'),
(162, 77, 'Tournament 12', 157, '2020-11-04', '16:56:00', 'Ramkrushna Kadam', 5, 1, 'Loss', 2, '2020-11-04 17:07:40'),
(163, 77, 'Tournament 12', 76092, '2020-11-04', '16:56:00', 'SK sanjay', 5, 1, 'Loss', 2, '2020-11-04 17:10:31'),
(164, 77, 'Tournament 12', 100758, '2020-11-04', '16:56:00', 'Mayuri', 5, 1, 'Win', 2, '2020-11-04 17:10:31'),
(165, 77, 'Tournament 12', 100751, '2020-11-04', '16:56:00', 'Harshal', 5, 1, 'Loss', 2, '2020-11-04 17:12:11'),
(166, 77, 'Tournament 12', 100800, '2020-11-04', '16:56:00', 'Sohail Mujawar', 5, 1, 'Win', 2, '2020-11-04 17:12:11'),
(167, 77, 'Tournament 12', 73300, '2020-11-04', '16:56:00', 'testing', 5, 1, 'Win', 2, '2020-11-04 17:12:29'),
(168, 77, 'Tournament 12', 79186, '2020-11-04', '16:56:00', 'aishu', 5, 1, 'Loss', 2, '2020-11-04 17:12:29'),
(169, 77, 'Tournament 12', 100763, '2020-11-04', '16:56:00', 'shraddha Jadhav', 5, 1, 'Loss', 2, '2020-11-04 17:13:31'),
(170, 77, 'Tournament 12', 100750, '2020-11-04', '16:56:00', 'piyush bhandari', 5, 1, 'Win', 2, '2020-11-04 17:13:31'),
(171, 77, 'Tournament 12', 100801, '2020-11-04', '16:56:00', 'Swapnali Dabhade', 5, 1, 'Loss', 2, '2020-11-04 17:22:23'),
(172, 77, 'Tournament 12', 76162, '2020-11-04', '16:56:00', 'aishu1', 5, 1, 'Win', 2, '2020-11-04 17:22:23'),
(173, 77, 'Tournament 12', 70961, '2020-11-04', '18:22:24', 'ji', 5, 2, 'Loss', 2, '2020-11-04 18:24:21'),
(174, 77, 'Tournament 12', 100758, '2020-11-04', '18:22:24', 'Mayuri', 5, 2, 'Loss', 2, '2020-11-04 18:25:00'),
(175, 77, 'Tournament 12', 100800, '2020-11-04', '18:22:24', 'Sohail Mujawar', 5, 2, 'Loss', 2, '2020-11-04 18:35:12'),
(176, 77, 'Tournament 12', 100752, '2020-11-04', '18:22:24', 'shubham jagtap', 5, 2, 'Win', 2, '2020-11-04 18:35:12'),
(177, 77, 'Tournament 12', 100750, '2020-11-04', '18:22:24', 'piyush bhandari', 5, 2, 'Win', 2, '2020-11-04 18:35:32'),
(178, 77, 'Tournament 12', 81635, '2020-11-04', '18:22:24', 'Anjali G', 5, 2, 'Loss', 2, '2020-11-04 18:35:32'),
(179, 77, 'Tournament 12', 76162, '2020-11-04', '18:22:24', 'aishu1', 5, 2, 'Win', 2, '2020-11-04 18:39:34'),
(180, 77, 'Tournament 12', 73300, '2020-11-04', '18:22:24', 'testing', 5, 2, 'Loss', 2, '2020-11-04 18:39:34'),
(181, 77, 'Tournament 12', 100752, '2020-11-04', '19:27:25', 'shubham jagtap', 5, 3, 'Loss', 2, '2020-11-04 19:30:48'),
(182, 77, 'Tournament 12', 76162, '2020-11-04', '19:27:25', 'aishu1', 5, 3, 'Win', 2, '2020-11-04 19:30:49'),
(183, 77, 'Tournament 12', 100750, '2020-11-04', '19:27:25', 'piyush bhandari', 5, 3, 'Win', 2, '2020-11-04 19:32:18'),
(184, 77, 'Tournament 12', 76162, '2020-11-04', '20:32:27', 'aishu1', 5, 4, 'Loss', 2, '2020-11-04 20:49:12'),
(185, 77, 'Tournament 12', 100750, '2020-11-04', '20:32:27', 'piyush bhandari', 5, 4, 'Win', 2, '2020-11-04 20:49:12'),
(186, 78, 'Tournament 13', 79186, '2020-11-06', '12:02:00', 'aishu', 5, 1, 'Loss', 2, '2020-11-06 12:04:30'),
(187, 78, 'Tournament 13', 76092, '2020-11-06', '12:02:00', 'SK sanjay', 5, 1, 'Win', 2, '2020-11-06 12:04:31'),
(188, 78, 'Tournament 13', 76162, '2020-11-06', '12:02:00', 'aishu1', 5, 1, 'Loss', 2, '2020-11-06 12:13:51'),
(189, 78, 'Tournament 13', 73300, '2020-11-06', '12:02:00', 'testing', 5, 1, 'Win', 2, '2020-11-06 12:13:51'),
(190, 78, 'Tournament 13', 73300, '2020-11-06', '13:13:53', 'testing', 5, 2, 'Loss', 2, '2020-11-06 13:28:14'),
(191, 78, 'Tournament 13', 76092, '2020-11-06', '13:13:53', 'SK sanjay', 5, 2, 'Win', 2, '2020-11-06 13:28:14'),
(192, 79, 'Tournament 14', 76092, '2020-11-06', '15:34:00', 'SK sanjay', 5, 1, 'Win', 2, '2020-11-06 15:47:24'),
(193, 79, 'Tournament 14', 76162, '2020-11-06', '15:34:00', 'aishu1', 5, 1, 'Loss', 2, '2020-11-06 15:47:24'),
(194, 79, 'Tournament 14', 79186, '2020-11-06', '15:34:00', 'aishu', 5, 1, 'Win', 2, '2020-11-06 15:49:51'),
(195, 79, 'Tournament 14', 73300, '2020-11-06', '15:34:00', 'testing', 5, 1, 'Loss', 2, '2020-11-06 15:49:51'),
(196, 79, 'Tournament 14', 76092, '2020-11-06', '16:49:51', 'SK sanjay', 5, 2, 'Win', 2, '2020-11-06 17:07:21'),
(197, 79, 'Tournament 14', 79186, '2020-11-06', '16:49:51', 'aishu', 5, 2, 'Loss', 2, '2020-11-06 17:07:21'),
(198, 80, 'Test price', 22325, '2020-11-10', '15:57:00', 'vishwa', 10, 1, 'Win', 2, '2020-11-10 16:01:51'),
(199, 80, 'Test price', 73109, '2020-11-10', '15:57:00', 'smstest', 10, 1, 'Loss', 2, '2020-11-10 16:08:47'),
(200, 80, 'Test price', 76581, '2020-11-10', '15:57:00', 'testplayer', 10, 1, 'Win', 2, '2020-11-10 16:08:48'),
(201, 80, 'Test price', 22325, '2020-11-10', '17:08:49', 'vishwa', 10, 2, 'Loss', 2, '2020-11-10 17:09:35'),
(202, 80, 'Test price', 76581, '2020-11-10', '17:08:49', 'testplayer', 10, 2, 'Loss', 2, '2020-11-10 17:10:00'),
(203, 81, 'Tournament 11', 123732, '2020-11-26', '16:18:00', 'varun', 5, 1, 'Loss', 2, '2020-11-26 16:21:11'),
(204, 81, 'Tournament 11', 123729, '2020-11-26', '16:18:00', 'raj gaikwad', 5, 1, 'Win', 2, '2020-11-26 16:21:11'),
(205, 81, 'Tournament 11', 73104, '2020-11-26', '16:18:00', 'Amol G', 5, 1, 'Loss', 2, '2020-11-26 16:21:52'),
(206, 81, 'Tournament 11', 76162, '2020-11-26', '16:18:00', 'aishu1', 5, 1, 'Win', 2, '2020-11-26 16:21:53'),
(207, 81, 'Tournament 11', 100797, '2020-11-26', '16:18:00', 'Shabista', 5, 1, 'Loss', 2, '2020-11-26 16:22:55'),
(208, 81, 'Tournament 11', 100751, '2020-11-26', '16:18:00', 'Harshal', 5, 1, 'Win', 2, '2020-11-26 16:22:56'),
(209, 81, 'Tournament 11', 123727, '2020-11-26', '16:18:00', 'rubina', 5, 1, 'Win', 2, '2020-11-26 16:24:01'),
(210, 81, 'Tournament 11', 100758, '2020-11-26', '16:18:00', 'Mayuri', 5, 1, 'Loss', 2, '2020-11-26 16:24:01'),
(211, 81, 'Tournament 11', 73300, '2020-11-26', '16:18:00', 'testing', 5, 1, 'Win', 2, '2020-11-26 16:24:31'),
(212, 81, 'Tournament 11', 70961, '2020-11-26', '16:18:00', 'ji', 5, 1, 'Loss', 2, '2020-11-26 16:24:31'),
(213, 81, 'Tournament 11', 100750, '2020-11-26', '16:18:00', 'piyush bhandari', 5, 1, 'Loss', 2, '2020-11-26 16:26:35'),
(214, 81, 'Tournament 11', 123709, '2020-11-26', '16:18:00', 'pragati', 5, 1, 'Win', 2, '2020-11-26 16:26:35'),
(215, 81, 'Tournament 11', 76092, '2020-11-26', '16:18:00', 'SK sanjay', 5, 1, 'Win', 2, '2020-11-26 16:28:03'),
(216, 81, 'Tournament 11', 123725, '2020-11-26', '16:18:00', 'shraddha ', 5, 1, 'Loss', 2, '2020-11-26 16:28:03'),
(217, 82, 'Tournament 12', 123728, '2020-11-26', '16:55:00', 'jay', 5, 1, 'Loss', 2, '2020-11-26 16:58:16'),
(218, 82, 'Tournament 12', 100751, '2020-11-26', '16:55:00', 'Harshal', 5, 1, 'Win', 2, '2020-11-26 16:58:17'),
(219, 82, 'Tournament 12', 76092, '2020-11-26', '16:55:00', 'SK sanjay', 5, 1, 'Loss', 2, '2020-11-26 16:58:52'),
(220, 82, 'Tournament 12', 100800, '2020-11-26', '16:55:00', 'Sohail Mujawar', 5, 1, 'Win', 2, '2020-11-26 16:58:53'),
(221, 82, 'Tournament 12', 70961, '2020-11-26', '16:55:00', 'ji', 5, 1, 'Win', 2, '2020-11-26 17:06:13'),
(222, 82, 'Tournament 12', 123732, '2020-11-26', '16:55:00', 'varun', 5, 1, 'Win', 2, '2020-11-26 17:07:07'),
(223, 82, 'Tournament 12', 100758, '2020-11-26', '16:55:00', 'Mayuri', 5, 1, 'Loss', 2, '2020-11-26 17:07:07'),
(224, 82, 'Tournament 12', 123709, '2020-11-26', '16:55:00', 'pragati', 5, 1, 'Loss', 2, '2020-11-26 17:09:39'),
(225, 82, 'Tournament 12', 100750, '2020-11-26', '16:55:00', 'piyush bhandari', 5, 1, 'Win', 2, '2020-11-26 17:09:39'),
(226, 82, 'Tournament 12', 76162, '2020-11-26', '16:55:00', 'aishu1', 5, 1, 'Loss', 2, '2020-11-26 17:09:48'),
(227, 82, 'Tournament 12', 73104, '2020-11-26', '16:55:00', 'Amol G', 5, 1, 'Win', 2, '2020-11-26 17:09:48'),
(228, 82, 'Tournament 12', 73300, '2020-11-26', '16:55:00', 'testing', 5, 1, 'Loss', 2, '2020-11-26 17:10:57'),
(229, 82, 'Tournament 12', 123694, '2020-11-26', '16:55:00', 'ankita', 5, 1, 'Win', 2, '2020-11-26 17:10:57'),
(230, 81, 'Tournament 11', 123709, '2020-11-26', '17:28:07', 'pragati', 5, 2, 'Win', 2, '2020-11-26 17:32:49'),
(231, 81, 'Tournament 11', 76092, '2020-11-26', '17:28:07', 'SK sanjay', 5, 2, 'Win', 2, '2020-11-26 17:32:56'),
(232, 81, 'Tournament 11', 76162, '2020-11-26', '17:28:07', 'aishu1', 5, 2, 'Win', 2, '2020-11-26 17:33:02'),
(233, 81, 'Tournament 11', 73300, '2020-11-26', '17:28:07', 'testing', 5, 2, 'Loss', 2, '2020-11-26 17:35:39'),
(234, 81, 'Tournament 11', 100751, '2020-11-26', '17:28:07', 'Harshal', 5, 2, 'Win', 2, '2020-11-26 17:35:39'),
(235, 81, 'Tournament 11', 123709, '2020-11-26', '18:33:09', 'pragati', 5, 3, 'Loss', 2, '2020-11-26 18:36:17'),
(236, 81, 'Tournament 11', 76092, '2020-11-26', '18:33:09', 'SK sanjay', 5, 3, 'Win', 2, '2020-11-26 18:38:00'),
(237, 83, 'Tournament 13', 81635, '2020-11-27', '11:43:00', 'Anjali G', 5, 1, 'Loss', 2, '2020-11-27 11:45:22'),
(238, 83, 'Tournament 13', 73300, '2020-11-27', '11:43:00', 'testing', 5, 1, 'Win', 2, '2020-11-27 11:45:23'),
(239, 83, 'Tournament 13', 100751, '2020-11-27', '11:43:00', 'Harshal', 5, 1, 'Loss', 2, '2020-11-27 11:47:27'),
(240, 83, 'Tournament 13', 100750, '2020-11-27', '11:43:00', 'piyush bhandari', 5, 1, 'Win', 2, '2020-11-27 11:47:28'),
(241, 83, 'Tournament 13', 123728, '2020-11-27', '11:43:00', 'jay', 5, 1, 'Loss', 2, '2020-11-27 11:49:51'),
(242, 83, 'Tournament 13', 70961, '2020-11-27', '11:43:00', 'ji', 5, 1, 'Win', 2, '2020-11-27 11:49:52'),
(243, 83, 'Tournament 13', 76162, '2020-11-27', '11:43:00', 'aishu1', 5, 1, 'Win', 2, '2020-11-27 11:57:26'),
(244, 83, 'Tournament 13', 100752, '2020-11-27', '11:43:00', 'shubham jagtap', 5, 1, 'Loss', 2, '2020-11-27 11:57:26'),
(245, 83, 'Tournament 13', 157, '2020-11-27', '11:43:00', 'R.K', 5, 1, 'Loss', 2, '2020-11-27 11:57:51'),
(246, 83, 'Tournament 13', 100758, '2020-11-27', '11:43:00', 'Mayuri', 5, 1, 'Win', 2, '2020-11-27 11:57:51'),
(247, 83, 'Tournament 13', 81638, '2020-11-27', '11:43:00', 'Osomose', 5, 1, 'Win', 2, '2020-11-27 11:58:01'),
(248, 83, 'Tournament 13', 123727, '2020-11-27', '11:43:00', 'rubina', 5, 1, 'Loss', 2, '2020-11-27 11:58:01'),
(249, 83, 'Tournament 13', 123694, '2020-11-27', '11:43:00', 'ankita', 5, 1, 'Loss', 2, '2020-11-27 11:58:09'),
(250, 83, 'Tournament 13', 123902, '2020-11-27', '11:43:00', 'snehal', 5, 1, 'Win', 2, '2020-11-27 11:58:09'),
(251, 83, 'Tournament 13', 100763, '2020-11-27', '11:43:00', 'shraddha Jadhav', 5, 1, 'Loss', 2, '2020-11-27 11:59:05'),
(252, 83, 'Tournament 13', 100800, '2020-11-27', '11:43:00', 'Sohail Mujawar', 5, 1, 'Win', 2, '2020-11-27 11:59:05'),
(253, 83, 'Tournament 13', 100800, '2020-11-27', '12:59:07', 'Sohail Mujawar', 5, 2, 'Loss', 2, '2020-11-27 13:11:12'),
(254, 83, 'Tournament 13', 81638, '2020-11-27', '12:59:07', 'Osomose', 5, 2, 'Win', 2, '2020-11-27 13:11:12'),
(255, 83, 'Tournament 13', 81638, '2020-11-27', '14:04:09', 'Osomose', 5, 3, 'Win', 2, '2020-11-27 14:09:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_account`
--

DROP TABLE IF EXISTS `user_account`;
CREATE TABLE `user_account` (
  `id` int(11) NOT NULL,
  `user_detail_id` int(11) NOT NULL,
  `orderId` varchar(255) NOT NULL,
  `paymentType` varchar(255) NOT NULL,
  `txnMode` varchar(255) NOT NULL,
  `type` enum('Deposit','Withdraw','Gratification') NOT NULL,
  `amount` double NOT NULL,
  `balance` double NOT NULL,
  `mainWallet` double NOT NULL,
  `winWallet` double NOT NULL,
  `status` enum('Approved','Pending','Rejected','Success','Failed','Process','BankExport') NOT NULL DEFAULT 'Pending',
  `paytmStatus` varchar(255) NOT NULL,
  `statusMessage` varchar(255) NOT NULL,
  `transactionId` varchar(255) NOT NULL COMMENT 'value will get from payment gateway response',
  `checkSum` varchar(255) NOT NULL,
  `rejectedReason` varchar(255) NOT NULL,
  `isReadNotification` enum('Yes','No') NOT NULL DEFAULT 'No',
  `isAdminReedem` enum('Yes','No') NOT NULL DEFAULT 'No',
  `statusCode` varchar(255) DEFAULT NULL,
  `coupanCode` varchar(150) DEFAULT NULL,
  `isCoupan` enum('Yes','No') NOT NULL DEFAULT 'No',
  `discount` double NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `mobileNo` bigint(20) NOT NULL,
  `pm` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_account_logs`
--

DROP TABLE IF EXISTS `user_account_logs`;
CREATE TABLE `user_account_logs` (
  `id` int(11) NOT NULL,
  `user_account_id` int(11) NOT NULL,
  `user_detail_id` int(11) NOT NULL,
  `orderId` varchar(255) NOT NULL,
  `paymentType` varchar(255) NOT NULL,
  `txnMode` varchar(255) NOT NULL,
  `amount` double NOT NULL,
  `balance` double NOT NULL,
  `mainWallet` double NOT NULL,
  `winWallet` double NOT NULL,
  `checkSum` varchar(255) NOT NULL,
  `paytmType` varchar(255) NOT NULL,
  `type` enum('Deposit','Withdraw') NOT NULL,
  `paytmStatus` varchar(255) NOT NULL,
  `statusCode` varchar(255) NOT NULL,
  `statusMessage` varchar(255) NOT NULL,
  `status` enum('Approved','Pending','Rejected','Process','Failed','Success','BankExport') NOT NULL DEFAULT 'Pending',
  `coupanCode` varchar(150) NOT NULL,
  `isCoupan` enum('Yes','No') NOT NULL DEFAULT 'No',
  `discount` double NOT NULL,
  `transactionId` varchar(255) NOT NULL COMMENT 'value will get from payment gateway response',
  `rejectedReason` varchar(255) NOT NULL,
  `tmp` longtext NOT NULL,
  `created` datetime NOT NULL,
  `mobileNo` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

DROP TABLE IF EXISTS `user_details`;
CREATE TABLE `user_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `playerId` varchar(255) NOT NULL,
  `playerType` enum('Real','Bot') NOT NULL DEFAULT 'Real',
  `registrationType` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `socialId` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `country_name` varchar(255) NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `profile_img` varchar(255) NOT NULL,
  `adharUserName` varchar(255) NOT NULL,
  `adharFron_img` varchar(255) NOT NULL,
  `adharBack_img` varchar(255) NOT NULL,
  `panUserName` varchar(255) NOT NULL,
  `pan_img` varchar(255) NOT NULL,
  `adharCard_no` varchar(255) NOT NULL,
  `panCard_no` varchar(255) NOT NULL,
  `kyc_status` enum('Pending','Verified','Rejected') NOT NULL DEFAULT 'Pending',
  `kycDate` date NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `otp` int(11) NOT NULL,
  `otp_verify` enum('Yes','No') NOT NULL DEFAULT 'No',
  `blockuser` enum('Yes','No') NOT NULL DEFAULT 'No',
  `signup_date` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `referal_code` varchar(255) NOT NULL,
  `referred_by` varchar(255) NOT NULL,
  `referredByUserId` int(11) NOT NULL,
  `referredAmt` double NOT NULL,
  `userLevel` int(11) NOT NULL,
  `bankRejectionReason` varchar(255) NOT NULL,
  `aadharRejectionReason` varchar(255) NOT NULL,
  `panRejectionReason` varchar(255) NOT NULL,
  `is_emailVerified` enum('Yes','No') NOT NULL DEFAULT 'No',
  `is_mobileVerified` enum('Yes','No') NOT NULL DEFAULT 'No',
  `is_aadharVerified` enum('Pending','Verified','Rejected') NOT NULL DEFAULT 'Pending',
  `is_panVerified` enum('Pending','Verified','Rejected') NOT NULL DEFAULT 'Pending',
  `is_bankVerified` enum('Pending','Verified','Rejected') NOT NULL DEFAULT 'Pending',
  `playerProgress` varchar(255) NOT NULL,
  `coins` double NOT NULL,
  `totalScore` double NOT NULL,
  `totalWin` int(11) NOT NULL,
  `balance` double NOT NULL,
  `totalLoss` int(11) NOT NULL,
  `mainWallet` double NOT NULL,
  `winWallet` double NOT NULL,
  `totalMatches` double NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `deviceName` varchar(255) NOT NULL,
  `deviceModel` varchar(255) NOT NULL,
  `deviceOs` varchar(255) NOT NULL,
  `deviceRam` varchar(255) NOT NULL,
  `lastSpinDate` datetime NOT NULL,
  `deviceProcessor` varchar(255) NOT NULL,
  `firstReferalUpdate` enum('Yes','No') NOT NULL DEFAULT 'No',
  `secondReferalUpdate` enum('Yes','No') NOT NULL DEFAULT 'No',
  `thirdReferalUpdate` enum('Yes','No') NOT NULL DEFAULT 'No',
  `totalCoinSpent` double NOT NULL,
  `version` double NOT NULL,
  `bonusUpdate` tinyint(4) NOT NULL DEFAULT 0,
  `ip` varchar(100) NOT NULL,
  `kyc_request_date` datetime DEFAULT NULL,
  `bank_account_no` varchar(255) DEFAULT NULL,
  `bank_account_holder_name` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_city` varchar(255) DEFAULT NULL,
  `bank_branch` varchar(255) DEFAULT NULL,
  `bank_ifsc` varchar(255) DEFAULT NULL,
  `block` tinyint(4) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `values`
--

DROP TABLE IF EXISTS `values`;
CREATE TABLE `values` (
  `id` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `web_banners`
--

DROP TABLE IF EXISTS `web_banners`;
CREATE TABLE `web_banners` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `banner_type` enum('Image','Video') NOT NULL,
  `order_no` int(100) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_account_log`
--
ALTER TABLE `admin_account_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_login`
--
ALTER TABLE `admin_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_menus`
--
ALTER TABLE `admin_menus`
  ADD PRIMARY KEY (`menuId`);

--
-- Indexes for table `admin_menu_mapping`
--
ALTER TABLE `admin_menu_mapping`
  ADD PRIMARY KEY (`menuMappingId`),
  ADD KEY `adminId` (`adminId`),
  ADD KEY `menuId` (`menuId`),
  ADD KEY `subMenuId` (`subMenuId`);

--
-- Indexes for table `bank_details`
--
ALTER TABLE `bank_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_detail_id` (`user_detail_id`),
  ADD KEY `is_bankVerified` (`is_bankVerified`);

--
-- Indexes for table `bonus`
--
ALTER TABLE `bonus`
  ADD PRIMARY KEY (`bonusId`);

--
-- Indexes for table `bonus_logs`
--
ALTER TABLE `bonus_logs`
  ADD PRIMARY KEY (`bonusLogId`);

--
-- Indexes for table `cms_pages`
--
ALTER TABLE `cms_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coins_deduct_history`
--
ALTER TABLE `coins_deduct_history`
  ADD PRIMARY KEY (`coinsDeductHistoryId`),
  ADD KEY `userId` (`userId`),
  ADD KEY `tableId` (`tableId`),
  ADD KEY `isWin` (`isWin`),
  ADD KEY `created` (`created`),
  ADD KEY `gameType` (`gameType`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon_codes`
--
ALTER TABLE `coupon_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon_user_log`
--
ALTER TABLE `coupon_user_log`
  ADD PRIMARY KEY (`couponLogId`);

--
-- Indexes for table `custom_dice`
--
ALTER TABLE `custom_dice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daywisetimings`
--
ALTER TABLE `daywisetimings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deposit`
--
ALTER TABLE `deposit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `game_features`
--
ALTER TABLE `game_features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invite_and_earn`
--
ALTER TABLE `invite_and_earn`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kyc_logs`
--
ALTER TABLE `kyc_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_detail_id` (`user_detail_id`),
  ADD KEY `is_bankVerified` (`is_bankVerified`),
  ADD KEY `is_aadharVerified` (`is_aadharVerified`),
  ADD KEY `is_panVerified` (`is_panVerified`),
  ADD KEY `kyc_status` (`kyc_status`);

--
-- Indexes for table `ludo_join_rooms`
--
ALTER TABLE `ludo_join_rooms`
  ADD PRIMARY KEY (`joinRoomId`);

--
-- Indexes for table `ludo_join_room_users`
--
ALTER TABLE `ludo_join_room_users`
  ADD PRIMARY KEY (`joinRoomUserId`);

--
-- Indexes for table `ludo_join_tour_rooms`
--
ALTER TABLE `ludo_join_tour_rooms`
  ADD PRIMARY KEY (`joinTourRoomId`),
  ADD KEY `joinRoomId` (`joinTourRoomId`),
  ADD KEY `roomId` (`tournamentId`),
  ADD KEY `noOfPlayers` (`noOfPlayers`),
  ADD KEY `activePlayer` (`activePlayer`),
  ADD KEY `betValue` (`betValue`),
  ADD KEY `gameStatus` (`gameStatus`),
  ADD KEY `gameMode` (`gameMode`),
  ADD KEY `isPrivate` (`isPrivate`),
  ADD KEY `isFree` (`isFree`),
  ADD KEY `created` (`created`),
  ADD KEY `isTournament` (`isTournament`),
  ADD KEY `isDelete` (`isDelete`);

--
-- Indexes for table `ludo_join_tour_room_users`
--
ALTER TABLE `ludo_join_tour_room_users`
  ADD PRIMARY KEY (`joinTourRoomUserId`),
  ADD KEY `joinRoomId` (`joinTourRoomId`),
  ADD KEY `userId` (`userId`),
  ADD KEY `userName` (`userName`),
  ADD KEY `isWin` (`isWin`),
  ADD KEY `tokenColor` (`tokenColor`),
  ADD KEY `playerType` (`playerType`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `ludo_mst_rooms`
--
ALTER TABLE `ludo_mst_rooms`
  ADD PRIMARY KEY (`roomId`);

--
-- Indexes for table `ludo_winners`
--
ALTER TABLE `ludo_winners`
  ADD PRIMARY KEY (`winnerId`);

--
-- Indexes for table `main_environment`
--
ALTER TABLE `main_environment`
  ADD PRIMARY KEY (`mainEnvironmentId`),
  ADD KEY `value` (`value`);

--
-- Indexes for table `mst_bonus`
--
ALTER TABLE `mst_bonus`
  ADD PRIMARY KEY (`bonusId`);

--
-- Indexes for table `mst_settings`
--
ALTER TABLE `mst_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `version` (`version`);

--
-- Indexes for table `mst_sms_body`
--
ALTER TABLE `mst_sms_body`
  ADD PRIMARY KEY (`smsId`);

--
-- Indexes for table `mst_tournaments`
--
ALTER TABLE `mst_tournaments`
  ADD PRIMARY KEY (`tournamentId`);

--
-- Indexes for table `mst_tournaments_old`
--
ALTER TABLE `mst_tournaments_old`
  ADD PRIMARY KEY (`tournamentId`);

--
-- Indexes for table `mst_tournament_logs`
--
ALTER TABLE `mst_tournament_logs`
  ADD PRIMARY KEY (`tournamenLogtId`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_detail_id` (`user_detail_id`),
  ADD KEY `status` (`status`),
  ADD KEY `created` (`created`),
  ADD KEY `paymentType` (`paymentType`),
  ADD KEY `type` (`type`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `payment_logs`
--
ALTER TABLE `payment_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_detail_id` (`user_detail_id`);

--
-- Indexes for table `payment_process`
--
ALTER TABLE `payment_process`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paytm_refunds`
--
ALTER TABLE `paytm_refunds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paytm_refund_logs`
--
ALTER TABLE `paytm_refund_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referal_user_logs`
--
ALTER TABLE `referal_user_logs`
  ADD PRIMARY KEY (`referLogId`),
  ADD KEY `fromUserId` (`fromUserId`),
  ADD KEY `referalAmountBy` (`referalAmountBy`),
  ADD KEY `toUserId` (`toUserId`),
  ADD KEY `tableId` (`tableId`);

--
-- Indexes for table `referral_users`
--
ALTER TABLE `referral_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reply_logs`
--
ALTER TABLE `reply_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`reportId`);

--
-- Indexes for table `spin_rolls`
--
ALTER TABLE `spin_rolls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `support_logs`
--
ALTER TABLE `support_logs`
  ADD PRIMARY KEY (`supportLogId`);

--
-- Indexes for table `tournaments`
--
ALTER TABLE `tournaments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tournament_registrations`
--
ALTER TABLE `tournament_registrations`
  ADD PRIMARY KEY (`tournamentRegtrationId`),
  ADD KEY `userId` (`userId`),
  ADD KEY `tournamentId` (`tournamentId`),
  ADD KEY `userName` (`userName`),
  ADD KEY `tournamentRegtrationId` (`tournamentRegtrationId`),
  ADD KEY `entryFee` (`entryFee`),
  ADD KEY `isEnter` (`isEnter`),
  ADD KEY `isWin` (`roundStatus`),
  ADD KEY `round` (`round`),
  ADD KEY `isDelete` (`isDelete`),
  ADD KEY `created` (`created`),
  ADD KEY `modified` (`modified`);

--
-- Indexes for table `tournament_win_loss_logs`
--
ALTER TABLE `tournament_win_loss_logs`
  ADD PRIMARY KEY (`tournamentWinLossLogId`);

--
-- Indexes for table `user_account`
--
ALTER TABLE `user_account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_detail_id` (`user_detail_id`),
  ADD KEY `status` (`status`),
  ADD KEY `created` (`created`),
  ADD KEY `paymentType` (`paymentType`),
  ADD KEY `type` (`type`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `user_account_logs`
--
ALTER TABLE `user_account_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_detail_id` (`user_detail_id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `id` (`id`),
  ADD KEY `user_name` (`user_name`),
  ADD KEY `email_id` (`email_id`),
  ADD KEY `mobile` (`mobile`),
  ADD KEY `referal_code` (`referal_code`),
  ADD KEY `is_mobileVerified` (`is_mobileVerified`),
  ADD KEY `kyc_status` (`kyc_status`),
  ADD KEY `is_aadharVerified` (`is_aadharVerified`),
  ADD KEY `is_panVerified` (`is_panVerified`),
  ADD KEY `device_id` (`device_id`),
  ADD KEY `is_emailVerified` (`is_emailVerified`),
  ADD KEY `otp` (`otp`),
  ADD KEY `otp_verify` (`otp_verify`),
  ADD KEY `playerType` (`playerType`),
  ADD KEY `playerId` (`playerId`),
  ADD KEY `registrationType` (`registrationType`),
  ADD KEY `signup_date` (`signup_date`),
  ADD KEY `password` (`password`),
  ADD KEY `adharCard_no` (`adharCard_no`),
  ADD KEY `panCard_no` (`panCard_no`),
  ADD KEY `version` (`version`),
  ADD KEY `bonusUpdate` (`bonusUpdate`),
  ADD KEY `mainWallet` (`mainWallet`),
  ADD KEY `kyc_request_date` (`kyc_request_date`),
  ADD KEY `block` (`block`),
  ADD KEY `socialId` (`socialId`);

--
-- Indexes for table `values`
--
ALTER TABLE `values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_banners`
--
ALTER TABLE `web_banners`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_account_log`
--
ALTER TABLE `admin_account_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_login`
--
ALTER TABLE `admin_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `admin_menus`
--
ALTER TABLE `admin_menus`
  MODIFY `menuId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `admin_menu_mapping`
--
ALTER TABLE `admin_menu_mapping`
  MODIFY `menuMappingId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `bank_details`
--
ALTER TABLE `bank_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bonus`
--
ALTER TABLE `bonus`
  MODIFY `bonusId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `bonus_logs`
--
ALTER TABLE `bonus_logs`
  MODIFY `bonusLogId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_pages`
--
ALTER TABLE `cms_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coins_deduct_history`
--
ALTER TABLE `coins_deduct_history`
  MODIFY `coinsDeductHistoryId` double NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupon_codes`
--
ALTER TABLE `coupon_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- AUTO_INCREMENT for table `coupon_user_log`
--
ALTER TABLE `coupon_user_log`
  MODIFY `couponLogId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `custom_dice`
--
ALTER TABLE `custom_dice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `daywisetimings`
--
ALTER TABLE `daywisetimings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `deposit`
--
ALTER TABLE `deposit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `game_features`
--
ALTER TABLE `game_features`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invite_and_earn`
--
ALTER TABLE `invite_and_earn`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kyc_logs`
--
ALTER TABLE `kyc_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ludo_join_rooms`
--
ALTER TABLE `ludo_join_rooms`
  MODIFY `joinRoomId` double NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ludo_join_room_users`
--
ALTER TABLE `ludo_join_room_users`
  MODIFY `joinRoomUserId` double NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ludo_join_tour_rooms`
--
ALTER TABLE `ludo_join_tour_rooms`
  MODIFY `joinTourRoomId` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=173;

--
-- AUTO_INCREMENT for table `ludo_join_tour_room_users`
--
ALTER TABLE `ludo_join_tour_room_users`
  MODIFY `joinTourRoomUserId` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=320;

--
-- AUTO_INCREMENT for table `ludo_mst_rooms`
--
ALTER TABLE `ludo_mst_rooms`
  MODIFY `roomId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `ludo_winners`
--
ALTER TABLE `ludo_winners`
  MODIFY `winnerId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_environment`
--
ALTER TABLE `main_environment`
  MODIFY `mainEnvironmentId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mst_bonus`
--
ALTER TABLE `mst_bonus`
  MODIFY `bonusId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mst_settings`
--
ALTER TABLE `mst_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mst_sms_body`
--
ALTER TABLE `mst_sms_body`
  MODIFY `smsId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `mst_tournaments`
--
ALTER TABLE `mst_tournaments`
  MODIFY `tournamentId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `mst_tournaments_old`
--
ALTER TABLE `mst_tournaments_old`
  MODIFY `tournamentId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mst_tournament_logs`
--
ALTER TABLE `mst_tournament_logs`
  MODIFY `tournamenLogtId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_logs`
--
ALTER TABLE `payment_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_process`
--
ALTER TABLE `payment_process`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paytm_refunds`
--
ALTER TABLE `paytm_refunds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paytm_refund_logs`
--
ALTER TABLE `paytm_refund_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `players`
--
ALTER TABLE `players`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `referal_user_logs`
--
ALTER TABLE `referal_user_logs`
  MODIFY `referLogId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `referral_users`
--
ALTER TABLE `referral_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reply_logs`
--
ALTER TABLE `reply_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `reportId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `spin_rolls`
--
ALTER TABLE `spin_rolls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `support_logs`
--
ALTER TABLE `support_logs`
  MODIFY `supportLogId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tournaments`
--
ALTER TABLE `tournaments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tournament_registrations`
--
ALTER TABLE `tournament_registrations`
  MODIFY `tournamentRegtrationId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=310;

--
-- AUTO_INCREMENT for table `tournament_win_loss_logs`
--
ALTER TABLE `tournament_win_loss_logs`
  MODIFY `tournamentWinLossLogId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=256;

--
-- AUTO_INCREMENT for table `user_account`
--
ALTER TABLE `user_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_account_logs`
--
ALTER TABLE `user_account_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `values`
--
ALTER TABLE `values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `web_banners`
--
ALTER TABLE `web_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
