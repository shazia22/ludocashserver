<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Kyc extends CI_Controller
{
    var $kycImagePath = "../adminpanel/uploads/kycImgs/";
    function __construct()
    {
        parent::__construct();
        $this->load->helper('custom_helper');
    }


    public function addKyc()
    {
        headers();
        $this->_request =  file_get_contents("php://input");
        $str_post = $this->_request;
        $jsonDecodeData =json_decode($this->_request, true);
        $userId                 = $jsonDecodeData['userId'] ? $jsonDecodeData['userId'] : '';
        $mobile                 = $jsonDecodeData['mobile'] ? $jsonDecodeData['mobile'] : '';
        $aadharNo               = $jsonDecodeData['aadharNo'] ? $jsonDecodeData['aadharNo'] : '';
        $aadharUserName         = $jsonDecodeData['aadharUserName'] ? $jsonDecodeData['aadharUserName'] : '';
        $panUserName            = $jsonDecodeData['panUserName'] ? $jsonDecodeData['panUserName'] : '';
        $panNo                  = $jsonDecodeData['panNo'] ? $jsonDecodeData['panNo'] : '';
        $aadharFrontImg         = $jsonDecodeData['aadharFrontImg'] ? $jsonDecodeData['aadharFrontImg'] : '';
        $aadharBackImg          = $jsonDecodeData['aadharBackImg'] ? $jsonDecodeData['aadharBackImg'] : '';
        $panImg                 = $jsonDecodeData['panImg'] ? $jsonDecodeData['panImg'] : '';
        $accHolderName          = $jsonDecodeData['accHolderName'] ? $jsonDecodeData['accHolderName'] : '';
        $accno                  = $jsonDecodeData['accno'] ? $jsonDecodeData['accno'] : '';
        $ifsc                   = $jsonDecodeData['ifsc'] ? $jsonDecodeData['ifsc'] : '';
        $bankName               = $jsonDecodeData['bankName'] ? $jsonDecodeData['bankName'] : '';
        $bankCity               = $jsonDecodeData['bankCity'] ? $jsonDecodeData['bankCity'] : '';
        /* if(!empty($userId) && !empty($mobile) && !empty($aadharNo) && !empty($panNo) && !empty($mobile) && !empty($aadharUserName) && !empty($panUserName) && !empty($aadharFrontImg) && !empty($aadharBackImg) && !empty($panImg) && !empty($accHolderName) && !empty($accno) && !empty($ifsc) && !empty($bankName) && !empty($bankCity)){*/
            if((!empty($aadharUserName) &&!empty($aadharNo) && !empty($aadharFrontImg) && !empty($aadharBackImg)) || (!empty($panUserName) && !empty($panNo) && !empty($panImg)) || (!empty($accHolderName) && !empty($accno) && !empty($ifsc) && !empty($bankName) && !empty($bankCity))){
                $duplicationA = $this->Crud_model->GetData('kyc_logs','count(adharCard_no) as adharCard_no',"adharCard_no='".$aadharNo."' AND is_approve='Yes'", '', '', '', '1');

                $duplicationP = $this->Crud_model->GetData('kyc_logs','count(panCard_no) as panCard_no',"panCard_no='".$panNo."' AND is_approve='Yes'", '', '', '', '1');
                //$userKycMobileData = $this->Crud_model->GetData('kyc_logs','count(mobile) as mobile',"user_detail_id='".$userId."' AND mobile<>'".$mobile."'", '', '', '', '1');
                $userKycData = $this->Crud_model->GetData('kyc_logs','',"user_detail_id='".$userId."'", '', 'id desc', '', '1');
                $getUserData = $this->Crud_model->GetData('user_details','',"id='".$userId."'", '', '', '', '1');
            // print_r($getUserData->mobile."-".$mobile);
                if(!empty($duplicationA->adharCard_no)){
                    $response = array('status' => FALSE, 'success' => "7", 'message' => "Adhar card number already exist.");
                }else if(!empty($duplicationP->panCard_no)){   
                    $response = array('status' => FALSE, 'success' => "8", 'message' => "Pan card number already exist."); 
                }else if(!empty($userKycMobileData->mobile)){   
                    $response = array('status' => FALSE, 'success' => "9", 'message' => "Mobile number already exist."); 
                // }else if(empty($getUserData->mobile)){   
                //     $response = array('status' => FALSE, 'success' => "10",'message' => "Please enter valid mobile number."); 
                }else if($getUserData->is_mobileVerified !='Yes'){   
                    $response = array('status' => FALSE, 'success' => "4", 'message' => "Mobile number not verified."); 
                }else if($getUserData->mobile !=$mobile){   
                    $response = array('status' => FALSE, 'success' => "5", 'message' => "Mobile number not match."); 
                }else{


                    $ext = "png";

                // Aadhar fron Image
                    $aadharfront_img='';
                    if(!empty($aadharFrontImg)){
                        $aadharFImg = base64_decode($aadharFrontImg);
                        $profileImage=time()."image";
                        $aadharImg = 'AdharF_p_'.md5(uniqid(rand(), true));
                        $aadharfront_img = $aadharImg.'.'.$ext;
                        file_put_contents(FCPATH.$this->kycImagePath."aadhar/".$aadharfront_img,$aadharFImg,TRUE);
                        if(!empty($getUserData->adharFron_img) && file_exists(FCPATH.$this->kycImagePath.'aadhar/'.$getUserData->adharFron_img)){

                            unlink(FCPATH.$this->kycImagePath.'aadhar/'.$getUserData->adharFron_img);
                        }
                    }

                // Aadhar back Image
                    $aadharback_img='';

                    if(!empty($aadharBackImg)){
                        $aadharFImg = base64_decode($aadharBackImg);
                        $aadharbackImg = 'AdharF_'.md5(uniqid(rand(), true));
                        $aadharback_img = $aadharbackImg.'.'.$ext;
                        file_put_contents(FCPATH.$this->kycImagePath."aadhar/".$aadharback_img,$aadharFImg,TRUE);
                        if(!empty($getUserData->adharBack_img) && file_exists(FCPATH.$this->kycImagePath.'aadhar/'.$getUserData->adharBack_img)){
                            unlink(FCPATH.$this->kycImagePath.'aadhar/'.$getUserData->adharBack_img);
                        }
                    }

                    $panCardImg='';

                    if(!empty($panImg)){
                        $pan_img = base64_decode($panImg);
                        $panImg = 'AdharF_'.md5(uniqid(rand(), true));
                        $panImage = $panImg.'.'.$ext;
                        $panCardImg = $panImage;
                        file_put_contents(FCPATH.$this->kycImagePath."pan/".$panCardImg,$pan_img,TRUE);
                        if(!empty($getUserData->pan_img) && file_exists(FCPATH.$this->kycImagePath.'pan/'.$getUserData->pan_img)){
                            unlink(FCPATH.$this->kycImagePath.'pan/'.$getUserData->pan_img);
                        }
                    }
                    $userKycData = $this->Crud_model->GetData('kyc_logs','',"user_detail_id='".$userId."'", '', 'id DESC', '', '1');
                    $getUserData = $this->Crud_model->GetData('user_details','',"id='".$userId."'", '', '', '', '1');

                    $data= array(
                        'is_bankVerified'=>'Pending',
                        'kycDate'=>date("Y-m-d"),
                        'kyc_request_date'=>date("Y-m-d H:i:s"),
                        'kyc_status'=>'Pending',
                    );

                    $KycDataLog=array(
                        'user_detail_id'=>$userId,
                        'mobile'=>$mobile,
                        'email'=>$getUserData->email_id,
                        'kyc_status'=>'Pending',
                        'is_approve'=>'No',
                        'tmp'=>$str_post,
                        'created'=>date('Y-m-d H:i:s'),
                        'modified'=>date('Y-m-d H:i:s'),
                    );
                    // print_r($aadharNo);
                    // print_r($userKycData);exit;
                    if(!empty($aadharNo)){

                        $data['is_aadharVerified']  =   "Pending";
                        $data['adharUserName']      =   $aadharUserName;
                        $data['adharCard_no']       =   $aadharNo;
                        $data['adharFron_img']      =   $aadharfront_img;
                        $data['adharBack_img']      =   $aadharback_img;
                        $KycDataLog['is_aadharVerified']  =   "Pending";
                        $KycDataLog['adharFron_img']      =   $aadharfront_img;
                        $KycDataLog['adharBack_img']      =   $aadharback_img;
                        $KycDataLog['adharUserName']      =   $aadharUserName;
                        $KycDataLog['adharCard_no']       =   $aadharNo;

                    }else{
                        if(!empty($userKycData)){
                            $data['adharUserName']      =   $userKycData->adharUserName;
                            $data['adharCard_no']       =   $userKycData->adharCard_no;
                            $data['adharFron_img']      =   $userKycData->adharFron_img;
                            $data['adharBack_img']      =   $userKycData->adharBack_img;
                            $data['is_aadharVerified']      =   $userKycData->is_aadharVerified;
                            $KycDataLog['is_aadharVerified']      =   $userKycData->is_aadharVerified;
                            $KycDataLog['adharFron_img']      =   $userKycData->adharFron_img;
                            $KycDataLog['adharBack_img']      =   $userKycData->adharBack_img;
                            $KycDataLog['adharUserName']      =   $userKycData->adharUserName;
                            $KycDataLog['adharCard_no']       =   $userKycData->adharCard_no;
                        }
                    }
                    if(!empty($panNo)){

                        $data['is_panVerified']  =   "Pending";
                        $data['panUserName']      =   $panUserName;
                        $data['pan_img']      =   $panCardImg;
                        $data['panCard_no']      =   $panNo;
                        $KycDataLog['is_panVerified']  =   "Pending";
                        $KycDataLog['pan_img']      =   $panCardImg;
                        $KycDataLog['panUserName']      =   $panUserName;
                        $KycDataLog['panCard_no']      =   $panNo;

                    }else{
                        if(!empty($userKycData)){
                            $data['panUserName']      =   $userKycData->panUserName;
                            $data['pan_img']      =   $userKycData->pan_img;
                            $data['is_panVerified']      =   $userKycData->is_panVerified;
                            $data['panCard_no']      =   $userKycData->panCard_no;
                            $KycDataLog['is_panVerified']      =   $userKycData->is_panVerified;
                            $KycDataLog['pan_img']      =   $userKycData->pan_img;
                            $KycDataLog['panUserName']      =   $userKycData->panUserName;
                            $KycDataLog['panCard_no']      =   $userKycData->panCard_no;
                        }
                    }

                    if(!empty($accno)){

                        $data['is_bankVerified']  =   "Pending";
                        $data['bank_account_holder_name']  =   $accHolderName;
                        $data['bank_city']  =   $bankCity;
                        $data['bank_name']  =   $bankName;
                        $data['bank_account_no']  =   $accno;
                        $data['bank_ifsc']  =   $ifsc;
                        $data['ifsc']  =   $ifsc;
                        $KycDataLog['is_bankVerified']  =   "Pending";
                        $KycDataLog['acc_holderName']  =   $accHolderName;
                        $KycDataLog['bank_name']  =   $bankName;
                        $KycDataLog['bank_city']  =   $bankCity;
                        $KycDataLog['accno']  =   $accno;
                        $KycDataLog['ifsc']  =   $ifsc;

                    }
                    $this->Crud_model->SaveData('user_details',$data,"id='".$userId."'");


                    $this->Crud_model->SaveData('kyc_logs',$KycDataLog);
                    $response = array('status' => TRUE, 'success' => "1", 'message' => "kyc added succesfully.");
                }
            }else{
                $response = array('status' => FALSE, 'success' => "0", 'message' => "All fields are required");
            }
            echo json_encode($response);
        }


        function getKyc() {
            headers();
            $this->_request =  file_get_contents("php://input");
            $jsonDecodeData =json_decode($this->_request, true);

            $userId         = $jsonDecodeData['userId'];

            if(!empty($userId)){
                $condition="id='".$userId."'";
            // $getKycData = $this->Crud_model->getKyc('user_details ud',$condition);
                $getUserData = $this->Crud_model->GetData('user_details',"",$condition,"","","","1");
                $getKycData = $this->Crud_model->GetData('kyc_logs',"","user_detail_id='".$userId."'","","id desc","","1");
                if(!empty($getUserData)){

                    if(!empty($getKycData->adharFron_img)){
                        $adharFron_img = base_url().'../adminpanel/uploads/kycImgs/aadhar/'.''.$getKycData->adharFron_img;
                    }else{
                        $adharFron_img='';
                    }

                    if(!empty($getKycData->adharBack_img)){
                        $adharBack_img = base_url().'../adminpanel/uploads/kycImgs/aadhar/'.''.$getKycData->adharBack_img;
                    }else{
                        $adharBack_img='';
                    }

                    if(!empty($getKycData->pan_img)){
                     $pan_img = base_url().'../adminpanel/uploads/kycImgs/pan/'.''.$getKycData->pan_img;
                 }else{
                    $pan_img='';
                }

                if(!empty($getUserData->is_aadharVerified=="Rejected") && !empty($getUserData->aadharRejectionReason)){
                    $aadharRejectionReason= $getUserData->aadharRejectionReason;
                    $is_aadharVerified= $getUserData->is_aadharVerified;
                }elseif(!empty($getUserData->is_aadharVerified=="Pending") && !empty($getUserData->aadharRejectionReason)){
                    $aadharRejectionReason= $getUserData->aadharRejectionReason;
                    $is_aadharVerified= $getUserData->is_aadharVerified;
                }else{
                       $aadharRejectionReason= $getUserData->aadharRejectionReason;  
                   
                    $is_aadharVerified= $getUserData->is_aadharVerified;
                }


                if(!empty($getUserData->is_panVerified=="Rejected") && !empty($getUserData->panRejectionReason)){
                    $panRejectionReason= $getUserData->panRejectionReason;
                    $is_panVerified= $getUserData->is_panVerified;
                }elseif(!empty($getUserData->is_panVerified=="Pending") && !empty($getUserData->panRejectionReason)){
                    $panRejectionReason= $getUserData->panRejectionReason;
                    $is_panVerified= $getUserData->is_panVerified;
                }else{
                     if(!empty($getUserData->aadharRejectionReason)){
                       $aadharRejectionReason= $getUserData->aadharRejectionReason;  
                    }else{
                        $aadharRejectionReason= '';
                    }
                    $panRejectionReason= $getUserData->panRejectionReason;
                    $is_panVerified= $getUserData->is_panVerified;
                }



                if(!empty($getUserData->is_bankVerified=="Rejected") && !empty($getUserData->bankRejectionReason)){
                    $bankRejectionReason= $getUserData->bankRejectionReason;
                    $is_bankVerified= $getUserData->is_bankVerified;
                }elseif(!empty($getUserData->is_bankVerified=="Pending") && !empty($getUserData->bankRejectionReason)){
                    $bankRejectionReason= $getUserData->bankRejectionReason;
                    $is_bankVerified= $getUserData->is_bankVerified;
                }else{
                    $bankRejectionReason= $getUserData->bankRejectionReason;
                    $is_bankVerified= $getUserData->is_bankVerified;
                }
                if(!empty($getKycData->adharUserName)){
                    $adharUserName = $getKycData->adharUserName;
                }else{
                    $adharUserName = '';
                }
                if(!empty($getKycData->adharCard_no)){
                    $adharCard_no = $getKycData->adharCard_no;
                }else{
                    $adharCard_no = '';
                }
                if(!empty($getKycData->panUserName)){
                    $panUserName = $getKycData->panUserName;
                }else{
                    $panUserName = '';
                }
                if(!empty($getKycData->panCard_no)){
                    $panCard_no = $getKycData->panCard_no;
                }else{
                    $panCard_no = '';
                }
                if(!empty($getKycData->acc_holderName)){
                    $acc_holderName = $getKycData->acc_holderName;
                }else{
                    $acc_holderName = '';
                }
                $data = array(
                    'id' =>$getUserData->id , 
                    'user_detail_id' =>$getUserData->id , 
                    'user_id' =>$getUserData->id , 
                    'adharFron_img' =>$adharFron_img , 
                    'adharBack_img' =>$adharBack_img , 
                    'pan_img' =>$pan_img , 
                    'aadharRejectionReason' =>$aadharRejectionReason , 
                    'is_aadharVerified' =>$is_aadharVerified , 
                    'panRejectionReason' =>$panRejectionReason , 
                    'is_panVerified' =>$is_panVerified , 
                    'bankRejectionReason' =>$bankRejectionReason , 
                    'is_bankVerified' =>$getUserData->is_bankVerified , 
                    'adharUserName' =>$adharUserName, 
                    'adharCard_no' =>$adharCard_no , 
                    'panUserName' =>$panUserName , 
                    'panCard_no' =>$panCard_no , 
                    'acc_holderName' =>$acc_holderName , 
                    'bank_name' =>$getUserData->bank_name , 
                    'bank_city' =>$getUserData->bank_city , 
                    'bank_branch' =>$getUserData->bank_branch , 
                    'accno' =>$getUserData->bank_account_no , 
                    'ifsc' =>$getUserData->bank_ifsc , 
                    'kyc_status' =>$getUserData->kyc_status , 
                    'name' =>$getUserData->name , 
                    'email_id' =>$getUserData->email_id , 
                    'is_mobileVerified' =>$getUserData->is_mobileVerified , 
                );

           /* unset($getKycData->registrationType);
            unset($getKycData->socialId);
            unset($getKycData->user_name);
            unset($getKycData->country_name);
            unset($getKycData->profile_img);
            unset($getKycData->password);
            unset($getKycData->otp);
            unset($getKycData->otp_verify);
            unset($getKycData->blockuser);
            unset($getKycData->last_login);
            unset($getKycData->signup_date);
            unset($getKycData->referred_by);
            unset($getKycData->balance);
            unset($getKycData->userLevel);
            unset($getKycData->device_id);
            unset($getKycData->playerProgress);
            unset($getKycData->mobile);
            unset($getKycData->referal_code);
            unset($getKycData->status);
            unset($getKycData->is_emailVerified);*/

            $response = array('status' => TRUE, 'success' => "1",'data'=>$data, 'message' => "Success");
                    }else{
                        $response = array('status' => FALSE, 'success' => "2", 'message' => "No Record Found");
                    }
                }else{
                    $response = array('status' => FALSE, 'success' => "0", 'message' => "please enter UserId");
                }
               echo json_encode($response);
            }


}
?>