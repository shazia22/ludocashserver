module.exports = {
  apps : [{
    name: "skill-socket",
    script: "./index.js",
    exp_backoff_restart_delay: 100,
    restart_delay: 2000,
    watch: true,
    watch_delay: 1000,
    error_file: "err.log",
    out_file: "out.log",
    env: {
      NODE_ENV: "development",
      TZ: "Asia/Kolkata"
    },
    env_production: {
      NODE_ENV: "production",
      TZ: "Asia/Kolkata"
    }
  }]
};
